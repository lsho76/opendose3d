# Short description

<!-- comments in Markdown language -->

This module provides the possibility to perform the dosimetry of a molecular radiotherapy (MRT) treatment cycle with several time point CT and SPECT or PET acquisitions. It contains several utilities and a workflow from start to end of dosimetry process.

<!-- The names of each Input need to always contain an acquisition time in its e.g. Volume 1HR. -->


<!-- Markdown indentation of nested lists requires 4 spaces  -->
<!-- https://stackoverflow.com/questions/37575916/how-to-markdown-nested-list-items-in-bitbucket  -->
1. Parameters panel allows to enter common variables for the study, such as the SPECT sensitivity and the isotope.
2. Preprocessing panel allows to prepare the essential objects for the calculation.
    * Rename files button allows to create the required variables, group the files by time point and rename all files using a convention.
    * Resample CTs button allows to perform automatic CT volumes resampling using the procedures already available in slicer (using Lanczos interpolation)
    * Rescale button converts the CT Volumes into density using a well established procedure and also converts the SPECT volumes to Activity map volumes.
3. Registration panel allows to spatially register all volumes taking one of them as reference. It uses the already available registration methods in slicer (rigid + affine).
    * The user is expected to review the results in the transformation module, correct manually all broad deviations and repeat the registration.
    * Each registration is applied to all volumes of a given time point.
    * Repeating the execution after manually adjusting broad deviations refine the registration.
4. Absorbed Dose Rate Calculation panel allows to select among different procedures (local energy deposition, homogenous FFT convolution, heterogeneous convolution and Monte Carlo simulation).
    * The Activity threshold bar is designed to remove Poisson noise influence in the calculations. The recommended value of 5% shall be accurate in most cases.
5. Segmentation panel allows to import user defined segmentations to provide segment statistics.
    * The Propagate Segmentation button is completely optional to handle volume variations per time point. If the original segmentation is propagated then the module will use the respective segmentation that will be placed in each time point folder. 
    * The user can refine the segmentations in each time point using the Segment Editor module.
6. Segment Tables and Plots panel: if a segmentation is provided then it is possible to obtain segment statistics either from the activity maps or from the absorbed dose rate volumes.
    * Segment statistic tables are created in each time point for either activity map volumes or absorbed dose rate volumes.
    * The respective time plots are also calculated; at this point a common table is created for the following integration in time.
7. Time Integration panel is the final output of the module. A table either based in activity map or absorbed dose rate statistics per segment is created by integration in time.
    <!-- 1. The incorporation mode allows to select the initial treatment from injection to first acquisition (linear, constant, exponential). --> 
    * The Incorporation Mode allows to select how to modelize the beginning ot the treatment, from injection to first acquisition (linear, constant, exponential).
    * The Integration Algorithm allows to select the algorithm that will be used for the time integration from the first acquisiton to the last acquisition (trapezoid, mono-exponential, bi-exponential, tri-exponential, x-exponential, auto-fit).
8. Utilities panel allows to perform optional tasks.
    * Clean Scene erases all non-conform Volumes and also erases all intermediate results.
<!--2. Fix Hermes DICOM is a tool to adjust Hermes software output images to be used by the plugin. This is automatically done by the plugin, therefor this is for reference only. -->

DISCLAIMER: This plugin is for academic purposes only and it is not recommended for use in clinical environment.

# MODULE DESCRIPTION

This module provides the possibility to perform the dosimetry of a molecular radiotherapy (MRT) treatment cycle with several time point CT and SPECT or PET acquisitions. It contains several utilities and a workflow from start to end of dosimetry process. (Figure 2)
<!-- Figure 2 to be updated, with a screenshot having opendose3d instead of opendose4d -->

<!--It is important to remember that user is responsible for input data: each input volume must contain the acquisition time in its name e.g. Volume 1HR. -->

The module expects several time points to work. It performs segment dosimetry, meaning as segment anything from tumors to organs defined by the segmentation provided by the user.

# INSTALLATION

**Prerequisites**: 

- Latest version and release of 3D Slicer (4.11) 
- "*SlicerElastix*" and "*Segment EditorExtraEffects*" 3D Slicer extensions (installable from *View* -> *Extension manager* -> *Install Extensions* or directly from ![extensions](images/install_extensions.png) in Slicer Home)

The *OpenDose Dosimetry 3D* module shall be installed automatically from Slicer extension repository (the same way described above for *SlicerElastix* and *Segment EditorExtraEffects*). <!--NB not yet available --> Nevertheless, it can also be downloaded or cloned  from [https://gitlab.com/opendose/opendose3d](https://gitlab.com/opendose/opendose3d). 

In the case of download, the insallation proceeds as follows:

1. Download the zip file (Figure 0) and extract it in a personal directory folder. It will contain, among other things, the folder OpenDose3D with the OpenDose3D.py file
2. Open Slicer and click ![customize](images/customize.png), or alternatively *Edit* -> *Application Settings*. 
3. In *Modules* -> *Additional module paths*, add the path to the folder containing the module files (Figure 1).
4. Restart Slicer to apply the changes.

 ![download](images/download.png)
Figure 0 Download from GitLab repository


In the case of cloning from GitLab, the insallation proceeds as follows:
 
1. In GitLab copy URL from "Clone with HTTPS" (Figure 0.1)
2. Open terminal in a personal directory folder and execute the line: <pre><code>git clone https[]()://gitlab.com/opendose/opendose3d.git </code></pre>
(i.e. "git clone" plus the copied URL).
This will download a folder called *opendose3d*, with  all the necessary files for the module 
3. Open Slicer and click ![customize](images/customize.png), or alternatively *Edit* -> *Application Settings*. 
4. In *Modules* -> *Additional module paths*, add the path to the folder containing the module files (Figure 1).
5. Restart Slicer.

![download](images/clone.png)
Figure 0.1 Clone from GitLab repository

![modules](images/modules.png)
Figure 1 Slicer Settings

Once Slicer is restarted, the Dosimetry3D module will be available from *All Modules* → *Radiotherapy* → *OpenDose Dosimetry 3D* (figure 1.1), together with the module *OpenDose Calibration* (usable for calculating and storing scanner sensitivities)

![D3Dmodule](images/dosimetry3d.png)
Figure 1.1 Opendose Dosimetry 3D module

# WORKFLOW

1. The module is written in workflow mode, meaning that the user has to complete some steps to be able to access further ones
    a. The input images (CTs and SPECT/PET scans) are renamed using a convention and grouped by time point.
    * The CTs are rescaled to SPECT/PET resolution.
    * The CTs volumes are converted to density using a well established methodology [CITATION Wil00 \l 1033] and the SPECT/PET volumes are converted to activity maps using the provided sensitivity factor and the respective DICOM header information.
    * All time points are spatially registered to selected volume; the transformations are unique for each time point. After this the user is expected to review the results and correct manually any broad misalignment using the Transformation module available in Slicer. Finally, this step is repeated until it gives satisfactory results, usually the second iteration is enough.
    * In this point the user must select between doing integration of activity maps (point 2) or doing integration of absorbed dose rates (Point 3).
2. Integration of activity maps:
    a. A segmentation is required in this point; the user must supply a valid segmentation on a reference volume (see point 1.d).
    * Optionally the segmentation can be propagated to each time point; in this case the user must check each individual segmentation and refine them. It is not allowed to remove, add or rename already established segments, only modify its spatial definition.
    * At this point Segment statistics are calculated for each segment at each time point.
    * A common table together with activity vs time plots are created.
    * Finally, for each segment, activity is integrated in time to produce cumulative activity, which is deposited locally to calculate the absorbed dose.
3. Integration of absorbed dose rate:
    a. The absorbed dose rate is calculated at voxel level; the user must select the desired method among the following ones, in increasing order of accuracy and required time:
        * Local Energy Deposition (LED)
        * Convolution FFT homogeneous
        * Convolution heterogeneous
        * Monte Carlo
    * The user must select desired activity threshold; higher threshold makes faster algorithm at the cost of accuracy. The recommended 5% value is appropriate for the general case. Some procedures take some time depending on hardware, but usually not exceeding some minutes per time point in the heterogeneous algorithm.
    * A segmentation is required in this point, the user must supply a valid segmentation on reference volume (see point 1.d).
    * Optionally the segmentation can be propagated to each time point; in this case the user must check each individual segmentation and refine them. It is not allowed to remove, add or rename already established segments, only modify its spatial definition.
    * At this point Segment statistics are calculated for each segment at each time point.
    * A common table together with absorbed dose rate vs time plots are created.
    * Finally, for each segment, absorbed dose rate is integrated in time to produce absorbed dose.

 ![full module](images/full-module.png)
Figure 2 Dosimetry 3D module.

# LIMITATIONS

1. The module requires some basic knowledge of Slicer modules from the user, specifically DICOM import, data management, general registration, segment editor and transformations. (User pre-requisite)
2. The automatic registration may fail if volumes are too far away spatially, the user must correct manually any broad misalignment. (User driven)
3. Even when registration aligns every organ in all time points, the behaviour of each may vary, especially they can change in shape, volume and/or mass. To handle these changes, the user can propagate the original segmentation and modify each segment definition. (User driven)
4. The python executable included in Slicer does not handle multiprocessing yet, therefore multiprocessing acceleration is not yet possible. (package not yet present)

# HOW TO:

##"Prelude": loading the input images

First of all, the input images must be imported into Slicer and loaded in the Slicer scene; the images must be in DICOM format.

1. Go to *File* → *DICOM* (or use the button ![dcm](images/dcm.png) )
2. mark the *Advanced* modality (Figure 3.0 bottom right)
3. unmark *MultiVolumeImporterPlugin* (Figure 3.0 left), in order to avoid some possible bugs
4. click *Examine* and then *Load*

 ![importdicom](images/prelude.png)
Figure 3.0 Importing and loading DICOM images

Now the DICOM files are loaded in the Slicer scene. In order for the module to work properly, all the images must be included in the same *study* folder of the same *subject* folder. Check if this condition is fulfilled looking at *Data* -> *Subject hierarchy* -> *Node*; otherwise move manually all the images inside a single *subject* and *study*.

In the following the various panels of the Dosimetry 3D module will be described, according to the workflow order. 


## PARAMETERS

This panel allows to enter common variables for the study: the isotope, the SPECT/PET scan sensitivity (called "camera factor") and the Injected Activity (Figure 3)

 ![parameters](images/parameters2.png)
Figure 3 Parameters Panel.

*Select clinical center* and *Select calibration date* can be used to employ a camera factor deduced through *OpenDose calibration module*. **Attention: under development**

Current available isotopes are Y-90, Lu-177, I-131, F-18 and Tb-161. More isotopes can be added at request.

The field *Camera factor* permits to manually set the camera factor for a selected isotope. Select carefully the correct units for the camera factor, bearing in mind that it is used as the factor that, multiplied for the specific values of the SPECT/PET images one is using (with their specific units), has to give the corresponding activity values in MBq. 
To calculate your camera factor follow the following guide: [https://youtu.be/xDn\_ySn-BSY](https://youtu.be/xDn_ySn-BSY) and optionally make use of the IAEA-NMQC plugin for FIJI at [https://humanhealth.iaea.org/HHW/MedicalPhysics/NuclearMedicine/QualityAssurance/NMQC-Plugins/index.html](https://humanhealth.iaea.org/HHW/MedicalPhysics/NuclearMedicine/QualityAssurance/NMQC-Plugins/index.html)

The module gets from DICOM metadata the value of *Injected Activity*, when it is available; check carefully the units of activity of the employed SPECT/PET and eventually edit manually the injected activity value in the present panel, since the module interprets by default the injected activity metadata values as being in MBq, even if they are in different units.

Note: *Camera factor* and *Injected Activity* will be actually applied at *Rescale* step, that is explained below in PRE-PROCESSING. 

## PRE-PROCESING

This panel allows the preparation of the essential objects for the calculation. (Figure 4).

 ![preprocessing](images/preprocessing.png)
Figure 4 Pre-processing panel.

The panel functionality is automatic and the activation of the succesive buttons is sequential.

1. *Rename files* button allows to create the required variables, group the files by time point into different folders and rename all files using a convention.
The variables are deduced from DICOM metadata and from manually entered input parameters. The time points are in terms of hours, expressed in integer number approximating the real number found in DICOM metadata. Note that the time point indicated in the renamed file names and in the folders is an integer approximated, but in the calculations the module uses the real true time point, taken from DICOM image metadata. The time points can also be manually changed (read below).

    If the rename procedure fails (it may happen for complicated names of the volumes), a warning message will appear, inviting you to rename manually the files names before the actual *Rename file* procedure, using simpler names according to the following convention: for the CTs use names like "CT 0HR", "CT 1HR", etc., for the SPECTs use "SPECT 0HR", "SPECT 1HR", etc. 
After *Rename files* procedure, the created variables will be visible in *Data* -> *MRML node information*. If necessary for some specific puropuses or to avoid errors, their values in the *Attribute values* column can be edited manually. One Attribute that it is good to check is *OpenDose3D.TimeStamp*, that sets the time point of the selected image, and therefore also the folder in which it is put. For example when CT and PET/SPECT *TimeStamp* difference is too large, or when one of them gives negative CT *TimeStamp*, that may produce error. In these cases change manually the Attribute value under consideration and do *Rename files* again; in this way the desired time points and their relative folders will be set. It is suggested to delete the unnecessary empty folders with wrong names remaining after the previous passages. 

2. *Resample CTs* button allows to perform automatic CT volumes resampling to SPECT/PET resolution, using the procedures already available in Slicer (using Lanczos interpolation). It creates new CT volumes named with the convention J#:CTRS CT #HR (J stands for the day number, taking 0 as the day of the first scan), for example: J1:CTRS CT 0HR. 

3. *Rescale* button converts the CT volumes into density volumes using a well established procedure [CITATION Wil00 \l 1033]. The density volumes are named with the conventions: J#:DENS CT #HR, for the ones obtained from original CTs, and J#:DERS CT #HR, for the ones obtained from resampled CTs.
*Rescale* button also converts the SPECT volumes into Activity map volumes (naming them  J#:ACTM SPECT #HR), employing for this purpose  *Injected Activity*, *Isotope* and *Camera factor* parameters.

This separation of functionalities allows the user to resume his works if it is interrupted. Once the objects are created the only important functionality to restart the processing is within the rename files button, which creates required variables; the remaining files are preserved.

The conversion of CT to density requires the CT to be expressed in absolute HU (Hounsfield Units); this means the CT must be corrected to the proper range. This process is done automatically based in DICOM header information. The conversion of SPECT to Activity Map requires the SPECT to be expressed in real counts, therefore an automatic scalation is performed using DICOM header information.

## REGISTRATION

This panel allows to spatially register all volumes taking one of them as reference. It uses the already available registration methods in slicer (rigid + affine). (Figure 5)

 ![registration](images/registration.png)
Figure 5 Registration panel

All time points volumes are registered to the reference volume. One transform node is created for each time point. To complete the functionality the user must be familiar with the General Registration (BRAINS) and the Transforms modules, which are already available in Slicer.

Automatic registration will fail in most cases because of initial broad misalignment. For this reason, after creating all transforms, the user must align manually all volumes and repeat the automatic registration once more. This process is repeated until satisfactory results are achieved. Usually the second automatic registration will be enough if the manual alignment process is performed correctly, see below.

The registration is performed using the densities volumes already calculated, to prevent the zero and negative importances in Soft Tissue and lung/fat respectively, that is characteristic in CT Hounsfield values.

### **Manual Alignment of volumes using the Transforms module from Slicer:**

After the initial automatic registration one transformation node is created in each time point. To check the proper registration and misalignment the user can select the Transforms module already available in Slicer (Figure 6).

For better results use the Volumes module also to change color table and brightness/contrast levels. Always use the background image as the reference in grayscale and the foreground image as the moving volume in red or green scales. Be careful in selecting the same time point in both the foreground image and the transform node, (Figure 7)

In this point just play with the translation and rotation bars to get a close enough registration (Figure 8). This step does not need to be perfect as it will be refined later in the second automatic registration step. The closer the volumes in all time points to reference, the better results after in automatic registration.

 ![module transformations](images/module-transformation.png)
Figure 6 Selecting the Transforms module, viewing broad misalignment between two points

 ![transformations](images/transformation.png)
Figure 7 Setting up the transforms module

![transformation settings](images/transformation-settings.png)
Figure 8 Using the Transforms module to correct broad misalignment

## ABSORBED DOSE RATE CALCULATION

This panel allows to generate absorbed dose rate volumes, calculated at voxel level in each time point. (Figure 9)

![adrm](images/ADRM.png)
Figure 9 Absorbed Dose Rate calculation panel

The activity threshold is meant to eliminate or at least reduce the Poisson noise that the activity images contain, and a threshold on their values is the simplest method. Controlling the threshold level, you can also speed up the convolution. The 5% proposed level shall be good compromise between speed, accuracy and noise removal in the general case.

The algorithms already available are:

1. Local Energy Deposition (LED)
2. Convolution FFT homogeneous
3. Convolution heterogeneous 
4. Monte Carlo simulation

The LED algorithm makes use of the MIRD database [CITATION Eckerman08 \l 1033] to generate total emitted energy for selected isotope.

All convolution algorithms require a dose kernel to work. These kernels were generated using dose point kernels obtained with GATE [CITATION Sarrut14 \l 1033] which are integrated in a voxel wise Monte Carlo Integration [CITATION Cor06 \l 1033] that has been proven to get very accurate results [CITATION Pac15 \l 1033]. Special care was taken to make the convolution methods really fast using numpy vectorization capabilities. 
    
   For *Convolution FFT homogeneous* and *Convolution heterogeneous* the field *kernel limit* sets the spatial extent to which the kernels  will be truncated. The larger the kernel limit, the more accurate and less fast the calculation will be. Pay attention to the fact that the value 0.0 is interpreted as "infinity" by the module (i.e. no kernel limit), and that maximum accuracy may be very high to be supported by a common notebook for *Convolution heterogeneous*. To have a reference for *Convolution heterogeneous*, a kernel limit of 30.0 mm should be a good compromise between accuracy and speed if combined with a non-zero activity threshold.

Monte Carlo (MC) algorithm produces macros for the GATE toolkit, ready to be run for performing MC simulations (for each time point) using the resampled CT as phantom, the activity map as source, and all the other quantities and informations entered in the module workflow to set the correct settings for the simulation. A dedicated Gate workstation is needed either by having Gate installed into it or by using Docker container (recommended). To install Docker in your system follow this link [https://docs.docker.com/get-docker](https://docs.docker.com/get-docker). Then the macros can be run as `docker run -i --rm -v $PWD:/APP bishopwolf/gate:9.0 mac/executor.mac` in a console where PWD is the base folder where the macros were created.

## SEGMENTATION

This panel allows to select the segmentation node referred to the reference volume and optionally propagate it to all time points (Figure 10). Segmentations can be imported if already available in a format supported by Slicer (for example DICOM or RTSTRUCT), or can be created directly on Slicer by the user taking advantege of the Segment Editor module. 

![segmentation](images/segmentation.png)

Figure 10 Segmentation panel

Although propagation is not required, it is highly advisable in cases where organs change in shape or volume and specially when tumors move from one time point to the other leaving them outside the defined segment. The latter situation is more common since the registration is based in anatomical images while tumors are usually only visible in SPECT/PET images. Some organs, such as the spleen can have massive changes during the treatment cycle, and the propagation/correction can also handle this case.

If propagation is made then the user should use the Segment Editor to refine the segment definition in each time point.

### **Use of Segment editor module**

With the use of Segment Editor module already available in Slicer the original segmentation referred to the Reference Volume can be created. (Figure 11) Also if propagation is performed then each time point can be refined too with the use of this module.

Several tutorials on how to create a segmentation properly in Slicer are already available and we will only refer to some of them:

- 3D Slicer Tutorial: How to Segment a Lumbar Vertebrae [https://www.youtube.com/watch?v=GGgP89uTOLo](https://www.youtube.com/watch?v=GGgP89uTOLo)
- Segmentation &amp; 3D Model Construction using 3D slicer [https://www.youtube.com/watch?v=6GMfCZ1u7ds](https://www.youtube.com/watch?v=6GMfCZ1u7ds)
- 3D Slicer CT Modelling/Segmentation Tutorial (muted) [https://www.youtube.com/watch?v=og3t1i9gSG0](https://www.youtube.com/watch?v=og3t1i9gSG0)
- Segmentation of lung and nodule in CT using 3D Slicer [https://www.youtube.com/watch?v=koHKAJWGNhU](https://www.youtube.com/watch?v=koHKAJWGNhU)

<!--  For optimal performance it is recommended also the following extensions -->

<!--- Segment Editor Extra Effects -->

<!--![Segment Editor](images/Segment-Editor.png) -->
 
<!--Figure 11 Segment Editor module of Slicer -->



## SEGMENT TABLES AND PLOTS

This is an intermediate calculation panel that is necessary to separate the two main workflows: Integration of activity maps (see WORKFLOW 2.) and Integration of absorbed dose rate (see WORKFLOW 3.). It basically generates the segment statistics tables in each time point (for example number of voxels, mean values, max and min values, standard deviations etc.), then perform a grouping of desired variables in a common table and generates plots to verify the correctness of the algorithm. (Figure 12)

![tables](images/TablesPlots.png)
Figure 12 Segment tables and plots panel

On the left side of Figure 12 you can see the activity map workflow buttons: *Create ACTM Tables* and *Create ACTM Plots*. Total activity per segment and segment mass are calculated. Then a table of activity and mass per segment in time is generated. A plot of activity in time is generated for visual quality control (Figure 13).

![Plots](images/Plots.png)
Figure 13 Chart of segment activity in time.

On the right side of Figure 12 are visible the absorbed dose rate workflow buttons: *Create DOSE Tables* and *Creato DOSE Plots* . Mean absorbed dose per segment and segment mass are calculated. Then a table of mean absorbed dose and mass per segment is generated. A plot of mean absorbed dose rate in time is generated for visual quality control (Figure 14).

![Fits](images/Fits.png)
Figure 14 Chart of segment mean absorbed dose rate in time

## TIME INTEGRATION

This panel is designed for time integration, that produces the final results, the absorbed dose per segment. The procedure depends on the workflow selected (Figure 15): in the case of activity map workflow, one will do *Integrate Activity*, while in the case of dose rate map workflow, one will do *Integrate Absorbed Dose Rate*. The integration is divided in three intervals: incorporation, data and tail. 

* The *Incorporation Mode* allows to select how to modelize the beginning ot the treatment, from injection time point to first acquisition time point. The available models are:

   - Linear: taking linear interpolation between zero and first time point (zero activity at start)
   - Constant: taking the first time point activity as constant from zero to the first time point
   - Exponential: taking the isotope decay into account.

* The *Integration Algorithm* allows to select the algorithm that will be used for the time integration from the first acquisiton time point to the last acquisition time point. The available methods for the integration in this data region are:

   - Trapezoidal
   - Mono exponential fit
   - Bi-exponential fit 
   - Tri-exponential fit
   - X-exponential fit
   - Auto-fit

* The tail region, from the last acquisition time point to infinity, is integrated assuming a mono exponential decay after the last data point.
If fits are used in the *Integration Algorithm* then the effective half-life is calculated and applied to integrate the tail. Instead in the case of trapezoidal integration a *Tail Mode* panel permits to integrate the tail using the physical half-life of the radionuclide, or an effective half-life taken from studies on populations (at the moment that is available in the module only for I-131).


![integration](images/integration.png)
Figure 15 Time integration panel

The *Integrate Activity* button (left side of Figure 15) makes the activity per segment integrated over time. This yields the cumulative activity per segment, which is then treated with local energy deposition using the selected isotope emission spectra (Figure 16).

![TableACTM](images/TableACTM.png)
Figure 16 Result table from the activity integration workflow

The *Integrate Absorbed Dose Rate* button (right side of Figure 15) makes the mean absorbed dose integrated over time. This yields the absorbed dose per segment. (Figure 17)

![TableADRM](images/TableADRM.png)
Figure 17 Result table from the mean absorbed dose integration workflow.


# BIBLIOGRAPHY

Cornejo Diaz N, C. P. (2006). Cálculo de valores S para 188Re en una geometría voxel. _Revista de Física Médica, 7_, 101-6.

Keith F Eckerman, A. E. (2008). _MIRD: Radionuclide Data and Decay Schemes, 2nd Edition._ The Society of Nuclear Medicine.

Pacilio, M., Amato, E., Lanconelli, N., Basile, C., Torres, L. A., Botta, F., . . . Vergara Gil, A. (2015). Differences in 3D dose distributions due to calculation method of voxel S-values and the influence of image blurring in SPECT. _Physics in Medicine &amp; Biology, 60_, 1945 –1964.

Sarrut D, B. M. (2014). A review of the use and potential of the GATE Monte Carlo code for radiation therapy and dosimetry applications. _Med. Phys., 41_(6).

Wilfried Schneider, T. B. (2000). Correlation between CT numbers and tissue parameters needed for Monte Carlo simulations of clinical dose distributions. _Phys. Med. Biol._(45), 459-478.


