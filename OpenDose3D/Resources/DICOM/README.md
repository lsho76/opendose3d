# Acknowledgment - IAEA CRP

The dicom files used for testing this software were generously published by **IAEA CRP E2.30.05 on “Dosimetry in Radiopharmaceutical therapy for personalized patient treatment”**!

For more information on their activities, please head to their official [website](https://www.iaea.org/services/coordinated-research-activities).