from io import StringIO
import json
from pathlib import Path
import xmlschema
import xmlschema.validators as validators

from Logic.errors import ConventionError
from Logic.utils import getScriptPath
from Logic.xmlutils import xmlutils


class XSDtoXMLGenerator:
    def __init__(self, xsd, elem, enable_choice=False):
        if not isinstance(xsd, Path):
            xsd = Path(xsd)
        if not xsd.exists():
            raise IOError("xsd file not found")

        self.xsdPath = str(xsd)
        self.xsd = xmlschema.XMLSchema(self.xsdPath, loglevel=30)
        self.elem = elem
        self.enable_choice = enable_choice
        self.root = True
        self.values = {}
        self.file = StringIO()
        self.getDefaultValues()

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.file.close()

    # Prints a string into the file object
    def printToFile(self, string):
        print(string, file=self.file)

    # Sample data is hardcoded in a json file
    def getDefaultValues(self):
        script_path = getScriptPath()

        jsonVals = script_path / "Resources" / "JSON" / "defaultXMLvalues.json"
        with jsonVals.open('r') as f:
            self.values = json.load(f)

    # shorten the namespace
    def shortNamespace(self, ns):
        for k, v in self.xsd.namespaces.items():
            if k == '':
                continue
            if v == ns:
                return k
        return ''

    # if name is using long namespace,
    # lets replace it with the short one
    def useShortNamespace(self, name):
        if name[0] == '{':
            x = name.find('}')
            ns = name[1:x]
            return self.shortNamespace(ns) + ":" + name[x + 1:]
        return name

    # remove the namespace in name

    def removeNamespace(self, name):
        if name[0] == '{':
            x = name.find('}')
            return name[x + 1:-2]
        x = name.find(':')
        return name[x + 1:-2]

    # header of xml doc
    def printHeader(self):
        self.printToFile("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>")

    # put all defined namespaces as a string

    def namespaceMapString(self):
        ns_all = ''
        for k, v in self.xsd.namespaces.items():
            if k == '':
                continue
            else:
                ns_all += 'xmlns:' + k + '=\"' + v + '\"' + ' '
        return ns_all

    # start a tag with name

    def startTag(self, name):
        x = '<' + name
        if self.root:
            self.root = False
            x += ' ' + self.namespaceMapString()
        x += '>'
        return x

    # end a tag with name

    def endTag(self, name):
        return '</' + name + '>'

    # make a sample data for primitive types

    def generateDefaultValue(self, name):
        name = self.removeNamespace(name)
        if name in self.values:
            return self.values[name]
        return 'ERROR !'

    # print a group

    def groupToXML(self, g):
        model = str(g.model)
        model = self.removeNamespace(model)
        nextg = g._group
        y = len(nextg)
        if y == 0:
            return

        for ng in nextg:
            if isinstance(ng, validators.XsdElement):
                self.node2xml(ng)
            elif isinstance(ng, validators.XsdAnyElement):
                self.node2xml(ng)
            else:
                self.groupToXML(ng)

            if self.enable_choice and model == 'choice':
                break

    # print a node
    def node2xml(self, node):

        if isinstance(node, validators.XsdAnyElement):
            return

        if isinstance(node.type, validators.XsdComplexType):
            n = self.useShortNamespace(node.name)
            if node.type.is_simple():
                tp = str(node.type.content_type)
                self.printToFile(self.startTag(
                    n) + self.generateDefaultValue(tp) + self.endTag(n))
            else:
                self.printToFile(self.startTag(n))
                self.groupToXML(node.type.content_type)
                self.printToFile(self.endTag(n))
        elif isinstance(node.type, validators.XsdAtomicBuiltin):
            n = self.useShortNamespace(node.name)
            tp = str(node.type)
            self.printToFile(self.startTag(
                n) + self.generateDefaultValue(tp) + self.endTag(n))
        elif isinstance(node.type, validators.XsdSimpleType):
            n = self.useShortNamespace(node.name)
            if isinstance(node.type, validators.XsdList):
                tp = str(node.type.item_type)
                self.printToFile(self.startTag(
                    n) + self.generateDefaultValue(tp) + self.endTag(n))
            elif isinstance(node.type, validators.XsdUnion):
                tp = str(node.type.member_types[0].base_type)
                self.printToFile(self.startTag(
                    n) + self.generateDefaultValue(tp) + self.endTag(n))
            elif isinstance(node.type, validators.XsdAtomicRestriction):
                for elem in node.type.facets.values():
                    tp = str(elem.enumeration[0])
                    break

                self.printToFile(self.startTag(n) + tp + self.endTag(n))
            else:
                tp = str(node.type.base_type)
                self.printToFile(self.startTag(
                    n) + self.generateDefaultValue(tp) + self.endTag(n))
        else:
            raise ConventionError('ERROR: unknown type: ' + node.type)

    # setup and print everything

    def run(self, print_header=True):
        if print_header:
            self.printHeader()
        self.node2xml(self.xsd.elements[self.elem])
        self.file.seek(0)

        xmlobject = xmlutils()
        xmlobject.setXSD(self.xsdPath)
        xmlobject.setXMLfromIO(self.file)
        return xmlobject
