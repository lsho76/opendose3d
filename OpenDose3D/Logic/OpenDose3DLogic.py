from datetime import datetime
from pathlib import Path
import tempfile
import pickle
import numpy as np
import qt
import vtk
from scipy import signal

import slicer
from slicer.ScriptedLoadableModule import ScriptedLoadableModuleLogic

from Logic.attributes import Attributes
from Logic.CLIScripts.Convolution import Convolution
from Logic.errors import Error, ConventionError
from Logic.fit_values import FitValues
from Logic.logging import getLogger
from Logic.constants import Constants
from Logic.vtkmrmlutils import * 
from Logic.utils import getCurrentTime, extractMode, isNumber, checkTesting
from Logic.Process import ProcessesLogic, ConvolutionProcess
from Logic.nodes import Node
from Logic.utils import getSystemCoresInfo, getScriptPath


class OpenDose3DLogic(ScriptedLoadableModuleLogic):
    """
    This class implements all the actual computation done by the module.
    The interface should be such, that other python code can import this class
    and make use of the functionality without requiring an instance of
    the Widget. Uses ScriptedLoadableModuleLogic base class, available at:
    https://github.com/Slicer/Slicer/blob/master/Base/Python/slicer/ScriptedLoadableModule.py
    """

    def __init__(self, parent=None, isotopeText='Lu-177', compressed=True):
        super().__init__(parent=parent)
        self.logger = getLogger('OpenDose3D.Logic')

        # Variables
        self.parent = parent
        self.testing = checkTesting()
        self.data = {}
        self.tableheader = {}
        self.CpuCores, self.CpuProcessors, self.system = getSystemCoresInfo()
        self.logger.info(
            f"Detected {self.CpuProcessors} processors with {self.CpuCores} physical cores")
        self.injectedActivity = -1
        self.injectionTime = 0

        # Setup Variables
        self.PETnodeColor = slicer.util.getFirstNodeByName('PET-Heat')
        self.GreynodeColor = slicer.util.getFirstNodeByName('Grey')
        self.script_path = getScriptPath()
        self.logger.info(f'Loading data for isotope {isotopeText}')
        self.Isotope = Constants().isotopes[isotopeText]
        self.Isotope['name'] = isotopeText
        self.logyaxis = False
        self.densityCorrection = True
        self.referenceFolderID = 0
        self.compressed = compressed


    def standardise(self, injectedActivity=-1, injectionTime=0):
        '''
        method to prepare raw data to be grouped by subject hierarchy and image name
        NOTE your data must have names according to the convention of either "[J][0-9]" or "\\d+h|h\\d+"
        TODO we might require to have searchSTR as input to allow flexibility
        '''
        try:
            _, studyID = self.standardiseHierarchy()
        except ConventionError as err:
            self.showError(err.message)
            return

        try:
            self.standardiseNodes(studyID, float(
                injectedActivity), injectionTime)
        except ConventionError as err:
            self.showError(err.message)
            return

    def standardiseHierarchy(self):
        """
        Setup a default hierarchy or incorporate existing folder structure
        """
        shNode = getSubjectHierarchyNode()
        sceneItemID = shNode.GetSceneItemID()

        # define subject
        subjectIDs = getSubjectIDs()
        if len(subjectIDs) == 0:
            subjectID = shNode.CreateSubjectItem(sceneItemID, 'Subject')
        elif len(subjectIDs) == 1:
            subjectID = subjectIDs[0]
        else:
            self.logger.error(
                "Failed to standardise hierarchy, multiple subjects exist")
            raise ConventionError(
                f"Scene contains {len(subjectIDs)} subjects. Please remove all but one subject.")

        # get study
        studyIDs = getStudyIDs()
        if len(studyIDs) == 0:
            studyID = shNode.CreateStudyItem(subjectID, 'Study')
        elif len(studyIDs) >= 1:
            studyID = studyIDs[0]
        else:
            self.logger.error(
                "Failed to standardise hierarchy, multiple studies exist")
            raise ConventionError(
                f"Subject contains {len(studyIDs)} studies. Please remove all but one study.")

        # get results folder
        resultIDs = getResultIDs()
        if len(resultIDs) == 0:
            _ = createResultsFolder(studyID)
        elif len(resultIDs) == 1:
            ResultFolder = resultIDs[0]
        else:
            self.logger.error(
                "Failed to standardise hierarchy, multiple results folder exist")
            raise ConventionError(
                f"Study contains {len(resultIDs)} results folder. Please remove all but one results folder.")

        return subjectID, studyID

    def standardiseNodes(self, studyID, injectedActivity, injectionTime):
        """
        Standardise node naming and move nodes to correct folder
        """
        volumeNodeIDs = getScalarVolumeNodes()

        # setup progress bar
        if not self.testing:
            ProgressDialog = slicer.util.createProgressDialog(
                parent=None, value=0, maximum=len(volumeNodeIDs))
        if not volumeNodeIDs:
            if not self.testing:
                ProgressDialog.close()
            self.showError('Please provide image data for renaming.')
            self.logger.error('Failed to rename files, no image data found')

        # search for initial acquisition time
        try:
            timeStamp, hours0, self.injectedActivity, self.injectionTime = Node.getInitialTime(
                injectedActivity, injectionTime)
        except Error as e:
            if not self.testing:
                ProgressDialog.close()
            raise ConventionError(e.message)

        if self.injectedActivity < 0:
            self.injectedActivity = injectedActivity

        # If user supply valid injection Time (<1 week from first acquisition) then force to use it
        if injectionTime:
            try:
                fromNode = datetime.strptime(self.injectionTime, Constants().timeFormat)
                fromUser = datetime.strptime(injectionTime, Constants().timeFormat)            
                difference = fromNode - fromUser
                hours = round(difference.total_seconds() / Constants().units['hour'])
                if abs(hours) < 24*8:  # 8 days to allow 1 day of grace
                    self.injectionTime = injectionTime
            except ValueError:
                self.logger.warning(f"User input {injectionTime} is not a valid date time, using header time")

        # process nodes (erase invalid ones)
        for index, volumeNodeID in enumerate(volumeNodeIDs):
            try:
                node = Node.new(volumeNodeID, self.Isotope['name'], self.injectedActivity)
            except:
                node = getItemDataNode(volumeNodeID)
                self.logger.error(
                    f'Erasing non conforming node {node.GetName()}...')
                slicer.mrmlScene.RemoveNode(node)
                continue

            if not('monte' in node.name.lower()) and not(node.getTimeStamp()):
                node.setTimeStamp(self.injectionTime)

            if not self.testing:
                ProgressDialog.labelText = f"Processing {node.name}..."
                ProgressDialog.setValue(index)
                if ProgressDialog.wasCanceled:
                    break
            slicer.app.processEvents()

            hours = round(float(node.getTimeStamp()),2)
            if hours is None or hours < 0:
                hours = 0

            if hours > 12:
                # Avoid duplicating folders for the same day, except the first day
                hours = round(hours/24)*24

            try:
                node.rename(hours)
            except:
                if not self.testing:
                    ProgressDialog.close()
                raise

            setAndObserveColorNode(node.nodeID)

            # create folder (named by hours) if not already exists inside study
            folderName = f'{hours}HR'
            folderID = getFolderChildByName(studyID, folderName)
            if not folderID:
                folderID = createFolder(studyID, folderName)

            setItemFolder(node.nodeID, folderID)

        if not self.testing:
            ProgressDialog.close()

        self.referenceFolderID = self.getReferenceFolder()
        if self.referenceFolderID == 0:
            self.createWaterCT()
            self.referenceFolderID = self.getReferenceFolder()
            if self.referenceFolderID == 0:
                raise ConventionError(
                    'No CT study available, impossible to proceed')

        referencenodes = Node.getFolderChildren(self.referenceFolderID)
        if 'CTCT' in referencenodes:
            minCTID = referencenodes['CTCT'].data.GetID()
        elif 'CTRS' in referencenodes:
            minCTID = referencenodes['CTRS'].data.GetID()
        else:
            raise ConventionError(
                'No CT study available, impossible to proceed')

        if 'ACSC' in referencenodes:
            minSPECTID = referencenodes['ACSC'].data.GetID()
            self.spacing = referencenodes['ACSC'].getSpacing()
        else:
            raise ConventionError(
                'No SPECT study available, impossible to proceed')

        setSlicerViews(minCTID, minSPECTID)

        self.fixManualTransformations()
        self.checkAttributes()

    def fixManualTransformations(self):
        # FIX manual transformations
        referencenodes = Node.getFolderChildren(self.referenceFolderID)
        folders = getFolderIDs()
        for index, folderID in enumerate(folders):
            nodes = Node.getFolderChildren(folderID)
            if 'TRNF' in nodes:
                node = nodes['TRNF'].data
                if not hasItemDataNodeAttribute(nodes['TRNF'].nodeID, Attributes().modality):
                    if 'CTCT' not in nodes:
                        node.SetAttribute(
                            Attributes().registrationVolume, str(nodes['ACSC'].nodeID))
                        node.SetAttribute(Attributes().registrationReference, str(
                            referencenodes['ACSC'].nodeID))
                    else:
                        node.SetAttribute(
                            Attributes().registrationVolume, str(nodes['ACSC'].nodeID))
                        node.SetAttribute(
                            Attributes().registrationReference, str(nodes['CTCT'].nodeID))
                    node.SetAttribute(
                        Attributes().registrationSampling, str(0.02))
                    node.SetAttribute(
                        Attributes().registrationMode, 'useGeometryAlign')
                    node.SetAttribute(Attributes().registrationRigid, 'True')
                    node.SetAttribute(Attributes().registrationAffine, 'False')
                    node.SetAttribute(
                        Attributes().registrationAlgorithm, 'Manual')
                    node.SetAttribute(
                        Attributes().studyCreation, getCurrentTime())
                    node.SetAttribute(Attributes().modality, 'REG')

    def checkAttributes(self):
        '''
        Runs sanity checks for the each node in each folder against a reference node
        and sets default attributes if they're missing.
        '''
        folders = getFolderIDs()
        referenceFolderID = self.referenceFolderID

        try:
            referenceNodes = Node.getFolderChildren(referenceFolderID)
            referenceNode = referenceNodes['ACSC']
        except:
            self.logger.error(
                f'Could not find a reference node in reference folder with id: {referenceFolderID}')
            return False

        if not self.testing:
            ProgressDialog = slicer.util.createProgressDialog(
                parent=None, value=0, maximum=len(folders))

        for index, folderID in enumerate(folders):
            folderName = getItemName(folderID)
            if not self.testing:
                ProgressDialog.labelText = f"Performing Sanity check on folder {folderName} ..."
                ProgressDialog.value = index
                ProgressDialog.show()
                if ProgressDialog.wasCanceled:
                    break
                slicer.app.processEvents()

            # exclude the reference folder from checks
            if folderID != referenceFolderID:
                try:
                    nodes = Node.getFolderChildren(folderID)
                    node = nodes['ACSC']
                except:
                    self.logger.error(
                        f'No node to compare to reference node in folder with id {folderID} found')
                    if not self.testing:
                        ProgressDialog.close()
                    return False

                if not node.compareAttributes(referenceNode):
                    self.showError(
                        'The attributes of the given nodes differ. Please check the integrity of the provided data.')
                    if not self.testing:
                        ProgressDialog.close()
                    return False

        if not self.testing:
            ProgressDialog.close()
        self.logger.info("Sanity check successful!!")
        return True

    def showError(self, error):
        '''
        displays an error as a feedback to the user
        '''
        # TODO create a new class for all this frontend stuff
        errorPopup = qt.QErrorMessage()
        errorPopup.showMessage(error)
        errorPopup.exec_()

    def importMonteCarlo(self, directory):
        '''
        imports Gate monte carlo doses
        '''
        # Check directory matches loaded Patient
        subdirectories = [f for f in directory.iterdir() if f.is_dir()]
        subdirectoriesStr = [str(d).split("/")[-1] for d in subdirectories]
        shNode = getSubjectHierarchyNode()
        folders = getFolderIDs()
        folderkeys = {}
        success = True
        for index, folderID in enumerate(folders):
            folderName = getItemName(folderID)
            folderkeys[folderName] = folderID
            success = success and (folderName in subdirectoriesStr)
            if success:  # Erase old data if exists
                nodes = Node.getFolderChildren(folderID)
                for key, node in nodes.items():
                    if key == 'ADRM':
                        for ikey, inode in node.items():
                            if 'monte' in ikey.lower():
                                slicer.mrmlScene.RemoveNode(inode.data)

        if not success:
            raise IOError('loaded folder does not match patient data')

        # Load image and run data
        for subdirectory in subdirectories:
            executorFile = subdirectory / "mac" / "executor.mac"
            dname = str(subdirectory).split('/')[-1]
            hour = float(dname[:-2])
            day = int(hour/24)
            newName = f"J{day}:ADRM MonteCarlo {dname}"
            with executorFile.open('r') as f:
                lines = f.readlines()
                for line in lines:
                    if "setTotalNumberOfPrimaries" in line:
                        NumberOfPrimaries = int(
                            float(line.strip().split()[-1]))
            simulationFile = subdirectory / "output" / "SimulationStatus.txt"
            with simulationFile.open('r') as f:
                lines = f.readlines()
                for line in lines:
                    if "NumberOfEvents" in line:
                        NumberOfEvents = int(float(line.strip().split()[-1]))
            doseFile = subdirectory / "output" / f"doseMonteCarlo{dname}-Dose.mhd"
            volumeNode = slicer.util.loadVolume(str(doseFile))
            volumeNodeID = getItemID(volumeNode)
            arr_data = slicer.util.arrayFromVolume(volumeNode).astype(
                float) * (NumberOfPrimaries/NumberOfEvents)
            slicer.util.updateVolumeFromArray(volumeNode, arr_data)
            # set attribute
            shNode.SetItemName(volumeNodeID, newName)
            setItemFolder(volumeNodeID, folderkeys[dname])

        # Standardise and fix positioning
        #self.standardise()
        for index, folderID in enumerate(folders):
            nodes = Node.getFolderChildren(folderID)

            for key, node in nodes.items():
                if key == 'ADRM':
                    for ikey, inode in node.items():
                        if 'monte' in ikey.lower():
                            # Apply folder transformation
                            if 'TRNF' in nodes:
                                trnfNodeID = nodes['TRNF'].data.GetID()
                                inode.data.SetAndObserveTransformNodeID(
                                    trnfNodeID)
                            # Gate outputs in Gy/s, convert to mGy/h
                            vox_arr = inode.getArrayData() / \
                                Constants().units["mGy_h"]
                            # Fix overflows: >1000 mGy/h is nonsense and it happens because PVE in air
                            vox_arr = np.where(vox_arr > 1000, 0, vox_arr)
                            vox_arr = np.flip(vox_arr, axis=(1,2)) # flip Y, Z axis
                            inode.setArrayData(vox_arr)

                            # get ACSC node for data
                            origin = nodes['ACSC'].getOrigin()
                            spacing = nodes['ACSC'].getSpacing()
                            dim = nodes['ACSC'].getDimensions()
                            ijkToRas = nodes['ACSC'].getIJKtoRASMatrix()

                            # Modify ijkToRas matrix
                            for i in range(3):
                                if ijkToRas.GetElement(i, i) < 0: # We need to flip the direction
                                    ijkToRas.SetElement(i, i, -ijkToRas.GetElement(i, i))
                                    origin[i] -= spacing[i]*(dim[i] - 1)

                            # Modify position and orientation
                            inode.setIJKtoRASMatrix(ijkToRas)
                            inode.setOrigin(origin)
                            inode.setSpacing(spacing)

                            # Convert to RAS orientation
                            inode.reorient("RAS")

                            # TODO: Fill missing attributes
                            inode.data.SetAttribute(
                                Attributes().modality, 'RTDOSE')
                            inode.data.SetAttribute(
                                Attributes().timeStamp, nodes['ACSC'].getTimeStamp())
                            inode.data.SetAttribute(
                                Attributes().isotope, str(self.Isotope['name']))
                            if 'ACTM' in nodes:
                                inode.data.SetAttribute(
                                    Attributes().doseACTMVolume, str(nodes['ACTM'].nodeID))
                            else:
                                inode.data.SetAttribute(
                                    Attributes().doseACTMVolume, str(nodes['ACSC'].nodeID))
                            inode.data.SetAttribute(
                                Attributes().doseAlgorithm, 'Monte Carlo')
                            inode.data.SetAttribute(
                                Attributes().studyCreation, getCurrentTime())

                            setAndObserveColorNode(inode.nodeID)  # refresh window

    def clean(self):
        ''' Cleans the setup from extra files and reinitializes all important data
        '''
        self.data = {}
        self.tableheader = {}
        nodes = slicer.util.getNodesByClass('vtkMRMLScalarVolumeNode')
        for node in nodes:
            name = node.GetName()
            if not any(ext in name for ext in ('ACSC', 'CTCT', ': CT')):
                slicer.mrmlScene.RemoveNode(node)
        nodes = slicer.util.getNodesByClass('vtkMRMLTransformNode')
        for node in nodes:
            slicer.mrmlScene.RemoveNode(node)
        nodes = slicer.util.getNodesByClass('vtkMRMLTableNode')
        for node in nodes:
            slicer.mrmlScene.RemoveNode(node)
        nodes = slicer.util.getNodesByClass('vtkMRMLSegmentationNode')
        for node in nodes:
            if 'SEGM' in node.GetName():  # Only remove propagated segmentations
                slicer.mrmlScene.RemoveNode(node)
        nodes = slicer.util.getNodesByClass('vtkMRMLPlotSeriesNode')
        for node in nodes:
            slicer.mrmlScene.RemoveNode(node)
        nodes = slicer.util.getNodesByClass('vtkMRMLPlotChartNode')
        for node in nodes:
            slicer.mrmlScene.RemoveNode(node)

    def getReferenceFolder(self):
        folders = getFolderIDs()
        minFolderID = 0
        hmin = 1000
        for folderID in folders:
            nodes = Node.getFolderChildren(folderID)

            if 'CTCT' in nodes and 'ACSC' in nodes:  # we need a folder with both CT and SPECT
                node = nodes['CTCT']
            else:
                continue

            _, hours = node.extractTime()

            if hours <= hmin:
                hmin = hours
                minFolderID = folderID

        if minFolderID == 0:  # There are no CT, try with ACSC
            for folderID in folders:
                nodes = Node.getFolderChildren(folderID)

                if 'ACSC' in nodes:
                    node = nodes['ACSC']
                else:
                    continue

                _, hours = node.extractTime()

                if hours <= hmin:
                    hmin = hours
                    minFolderID = folderID

        return minFolderID

    def createWaterCT(self):
        folders = getFolderIDs()
        for folderID in folders:
            nodes = Node.getFolderChildren(folderID)
            if not 'CTCT' in nodes or not 'CTRS' in nodes:
                newName = nodes['ACSC'].name.replace('ACSC', 'CTRS')
                CTRSNode = cloneNode(nodes['ACSC'].data, newName)
                CTRSNode.SetAttribute(Attributes().modality, 'CTRS')
                ctrsItemID = getItemID(CTRSNode)
                CTRSNode = Node.new(ctrsItemID)

                arrayCTRS = CTRSNode.getArrayData()
                new_arr = np.zeros_like(arrayCTRS)
                CTRSNode.setArrayData(new_arr)

    def resampleCT(self):
        '''
        resample all CT nodes to SPECT resolution, keep original data
        '''
        folders = getFolderIDs()
        if not self.testing:
            ProgressDialog = slicer.util.createProgressDialog(
                parent=None, value=0, maximum=len(folders))
        for index, folderID in enumerate(folders):
            folderName = getItemName(folderID)
            nodes = Node.getFolderChildren(folderID)
            if not self.testing:
                ProgressDialog.labelText = f"Processing {folderName}..."
                ProgressDialog.value = index
                ProgressDialog.show()
                if ProgressDialog.wasCanceled:
                    break
                slicer.app.processEvents()

            # SPECT
            nodeSPECT = nodes['ACSC']
            newName = nodeSPECT.name.replace('ACSC', 'CTRS')

            # CT
            if 'CTCT' in nodes:
                nodeCT = nodes['CTCT']
            else:
                minNodes = Node.getFolderChildren(self.referenceFolderID)
                nodeCT = minNodes['CTCT']

            if 'CTRS' in nodes:
                slicer.mrmlScene.RemoveNode(nodes['CTRS'].data)

            # Output Rescaled CT using SPECT sampling
            minCT = float(min(nodeCT.getArrayData().ravel()))
            CTRSNode = slicer.mrmlScene.AddNewNodeByClass(
                'vtkMRMLScalarVolumeNode')
            CTRSItemID = getItemID(CTRSNode)

            parameters = {'inputVolume': nodeCT.data, 'referenceVolume': nodeSPECT.data, 'outputVolume': CTRSNode,
                          'interpolationMode': 'Lanczos', 'defaultValue': minCT}

            slicer.cli.run(slicer.modules.brainsresample,
                           None, parameters, wait_for_completion=True, update_display=False)

            CTRSNode.SetAttribute(Attributes().patientID,
                                  nodeSPECT.getPatientID())
            CTRSNode.SetAttribute(
                Attributes().seriesInstanceUID, nodeCT.getSeriesInstanceUID())
            CTRSNode.SetAttribute(
                Attributes().studyInstanceUID, nodeCT.getStudyInstanceUID())
            CTRSNode.SetAttribute(Attributes().instanceUIDs,
                                  nodeSPECT.getInstanceUIDs())
            CTRSNode.SetAttribute(Attributes().acquisition,
                                  nodeSPECT.getAcquisition())
            CTRSNode.SetAttribute(
                Attributes().acquisitionDuration, nodeSPECT.getAcquisitionDuration())
            CTRSNode.SetAttribute(Attributes().patientName,
                                  nodeSPECT.getPatientName())
            CTRSNode.SetAttribute(Attributes().modality, 'CTRS')
            CTRSNode.SetAttribute(Attributes().timeStamp,
                                  nodeSPECT.getTimeStamp())
            CTRSNode.SetAttribute(Attributes().resamplingMode, 'Lanczos')
            CTRSNode.SetAttribute(
                Attributes().resamplingVolume, str(nodeCT.nodeID))
            CTRSNode.SetAttribute(
                Attributes().resamplingReference, str(nodeSPECT.nodeID))
            CTRSNode.SetAttribute(
                Attributes().studyCreation, getCurrentTime())
            CTRSNode.Modified()

            nodes['CTRS'] = Node.new(CTRSItemID)
            nodes['CTRS'].setVisualization()
            nodes['CTRS'].setNameParent(newName, folderID)

            self.logger.info(f"Folder {folderName} processed")

        if not self.testing:
            ProgressDialog.close()

    def scaleValues(self, calibration, injectedActivity, injectionTime):
        ''' method to perform image scaling by a factor
            passed as value of dictionnary
            TODO: rephrase
        '''
        folders = getFolderIDs()
        cSensitivity = calibration['SPECTSensitivity']['Value']
        # from counts/Bq to Bq/counts
        if "counts/" in calibration['SPECTSensitivity']['Units']:
            cSensitivity = 1/cSensitivity
        if not 'MBq' in calibration['SPECTSensitivity']['Units']:  # to MBq/counts
            cSensitivity /= 1e6
        correctFactor = 'Bqs' in calibration['SPECTSensitivity']['Units']
        if not correctFactor:
            cSensitivity *= calibration['SPECTSensitivity']['Time']
        T_h = self.Isotope['T_h'] * Constants().units['hour']

        ders = slicer.util.getNodes('*CTRS*')
        if len(ders) == 0:
            self.logger.error(
                'Resampled CT images not present, please create them before creating transforms')
            return
        if not self.testing:
            ProgressDialog = slicer.util.createProgressDialog(
                parent=None, value=0, maximum=len(folders)*3)

        for index, folderID in enumerate(folders):
            # Initialize folder
            folderName = getItemName(folderID)
            nodes = Node.getFolderChildren(folderID)
            if not self.testing:
                ProgressDialog.labelText = f"Processing {folderName}...Activity Map"
                ProgressDialog.value = 3*index
                ProgressDialog.show()
                if ProgressDialog.wasCanceled:
                    break
                slicer.app.processEvents()

            # Scale SPECT to Activity Map
            ACSCNode = nodes['ACSC']
            newName = ACSCNode.name.replace('ACSC', 'ACTM')
            try:
                duration = float(ACSCNode.getAcquisitionDuration())
            except:
                self.logger.error(
                    "Acquisition duration not set in DICOM")
                self.showError("Acquisition duration not set in DICOM\n" +
                               "Please fill the relevant data in the Node information"
                               )
                if not self.testing:
                    ProgressDialog.close()
                return

            try:
                localInjectedActivity = float(ACSCNode.getInjectedActivity())
            finally:
                if float(injectedActivity) != localInjectedActivity:
                    ACSCNode.data.SetAttribute(
                        Attributes().injectedActivity, str(injectedActivity))
                localInjectedActivity = float(injectedActivity)

            try:
                localInjectionTime = ACSCNode.getInjectionTime()
            finally:
                if injectionTime and injectionTime != localInjectionTime:
                    ACSCNode.data.SetAttribute(
                        Attributes().injectionTime, str(injectionTime))
                localInjectionTime = injectionTime

            if 'ACTM' in nodes:
                slicer.mrmlScene.RemoveNode(nodes['ACTM'].data)

            ACTMNode = cloneNode(ACSCNode.data, newName)
            actmItemID = getItemID(ACTMNode)
            ACTMNode.SetAttribute(Attributes().sensitivity, str(
                calibration['SPECTSensitivity']['Value']))
            ACTMNode.SetAttribute(Attributes().sensitivityUnits, str(
                calibration['SPECTSensitivity']['Units']))
            ACTMNode.SetAttribute(
                Attributes().acquisitionDuration, str(duration))
            ACTMNode.SetAttribute(Attributes().calibrationDuration, str(
                calibration['SPECTSensitivity']['Time']))
            ACTMNode.SetAttribute(
                Attributes().isotope, str(self.Isotope['name']))
            ACTMNode.SetAttribute(
                Attributes().rescalingVolume, str(ACSCNode.nodeID))
            ACTMNode.SetAttribute(Attributes().modality, 'RWV')
            ACTMNode.SetAttribute(
                Attributes().studyCreation, getCurrentTime())
            ACTMNode.SetAttribute(
                Attributes().injectedActivity, str(localInjectedActivity))

            vox_arr = ACSCNode.getArrayData()

            x = np.log(2) * duration / T_h
            decayCorrection = (1 - np.exp(-x)) / x

            # to MBq, we consider MBq everywhere
            if ACSCNode.getModality()=='PT':  # PET images are expressed in Bq/ml
                voxelVolume = np.prod(ACSCNode.getSpacing()) * Constants().units['mm3'] / Constants().units['cm3']
                vox_arr = voxelVolume * decayCorrection * vox_arr / 1e6
            elif ACSCNode.getModality()=='NM': # SPECT images are expressed in MBq
                lsensitivity = cSensitivity / duration
                vox_arr = lsensitivity * vox_arr #TODO: Apply decay correction too?
            else: # error
                raise IOError('activity node invalid, no modality present')
            
            vox_arr.astype(int)

            nodes['ACTM'] = Node.new(actmItemID)
            nodes['ACTM'].setArrayData(vox_arr)

            # Scale rescaled CT to density map
            if not self.testing:
                ProgressDialog.labelText = f"Processing {folderName}...CT rescaled density Map"
                ProgressDialog.value = 3*index + 1
                if ProgressDialog.wasCanceled:
                    break
                slicer.app.processEvents()
            if 'CTRS' in nodes:
                ctrsnode = nodes['CTRS']
            else:
                continue

            newName = ctrsnode.name.replace('CTRS', 'DERS')
            if 'DERS' in nodes:
                slicer.mrmlScene.RemoveNode(nodes['DERS'].data)

            DERSNode = cloneNode(ctrsnode.data, newName)
            dersItemID = getItemID(DERSNode)
            DERSNode.SetAttribute(Attributes().modality, 'RWV')
            DERSNode.SetAttribute(
                Attributes().calibrationCT, str(calibration))
            DERSNode.SetAttribute(
                Attributes().rescalingVolume, str(ctrsnode.nodeID))
            DERSNode.SetAttribute(
                Attributes().studyCreation, getCurrentTime())

            vox_arr = ctrsnode.getArrayData()
            # use same scale as original CT
            vox_arr = np.where(vox_arr < 0,
                               calibration['CTCalibration']['a0'] *
                               vox_arr +
                               calibration['CTCalibration']['b0'],
                               calibration['CTCalibration']['a1'] *
                               vox_arr + calibration['CTCalibration']['b1']
                               )
            vox_arr = np.where(
                vox_arr < 0, 0, vox_arr * Constants().units['g_cm3'])

            nodes['DERS'] = Node.new(dersItemID)
            nodes['DERS'].setArrayData(vox_arr)

            # Scale also CT to Density map
            if not self.testing:
                ProgressDialog.labelText = f"Processing {folderName}...CT density Map"
                ProgressDialog.value = 3*index + 2
                if ProgressDialog.wasCanceled:
                    break
                slicer.app.processEvents()

            if 'CTCT' in nodes:
                CTCTNode = nodes['CTCT']
            else:
                continue

            newName = CTCTNode.name.replace('CTCT', 'DENS')
            if 'DENS' in nodes:
                slicer.mrmlScene.RemoveNode(nodes['DENS'].data)

            DENSNode = cloneNode(CTCTNode.data, newName)
            densItemID = getItemID(DENSNode)
            DENSNode.SetAttribute(Attributes().modality, 'RWV')
            DENSNode.SetAttribute(Attributes().calibrationCT, str(
                calibration['CTCalibration']))
            DENSNode.SetAttribute(
                Attributes().rescalingVolume, str(CTCTNode.nodeID))
            DENSNode.SetAttribute(
                Attributes().studyCreation, getCurrentTime())

            vox_arr = CTCTNode.getArrayData()
            vox_arr = np.where(vox_arr < 0,
                               calibration['CTCalibration']['a0'] *
                               vox_arr +
                               calibration['CTCalibration']['b0'],
                               calibration['CTCalibration']['a1'] *
                               vox_arr + calibration['CTCalibration']['b1']
                               )
            # to kg_m3, we need kg and common unit for distance
            vox_arr = np.where(
                vox_arr < 0, 0, vox_arr * Constants().units['g_cm3'])

            nodes['DENS'] = Node.new(densItemID)
            nodes['DENS'].setArrayData(vox_arr)

            self.logger.info(f"Folder {folderName} processed")

        if not self.testing:
            ProgressDialog.close()

    def createTransforms(self, reference):
        ''' Creates the transformations in all time points
            Performs initial COM registration
            Performs Elastix Automatic registration
            If there is a CT in the folder perform CT-CT elastic registration
            otherwise it performs a SPECT-SPECT rigid registration
            TODO: Affine registration is not working properly but it is the default when no elastix is found
        '''
        try:
            import Elastix
            USE_ELASTIX = True
        except:
            USE_ELASTIX = False

        if checkTesting():  # TODO: Elastix is failing in tests
            USE_ELASTIX = False

        if USE_ELASTIX:
            elastixLogic = Elastix.ElastixLogic()
            parameterFilenamesElastix = elastixLogic.getRegistrationPresets(
            )[0][Elastix.RegistrationPresets_ParameterFilenames]
            parameterFilenamesRigid = elastixLogic.getRegistrationPresets(
            )[1][Elastix.RegistrationPresets_ParameterFilenames]

        folders = getFolderIDs()
        referenceName = reference.GetName()
        folderIDR = getFolderID(referenceName)
        self.referenceFolderID = folderIDR
        nodes = Node.getFolderChildren(folderIDR)
        if 'ACTM' not in nodes:
            self.logger.error(
                'Scaling not done, please perform the step before creating transforms')
            return

        densRnode = nodes['DENS']
        actmRnode = nodes['ACTM']
        originRef = densRnode.getOrigin()
        spacingRef = densRnode.getSpacing()
        positionRef = np.array(
            [originRef[0]*spacingRef[0],
            originRef[1]*spacingRef[1],
            originRef[2]*spacingRef[2]])
        if not self.testing:
            ProgressDialog = slicer.util.createProgressDialog(
                parent=None, value=0, maximum=len(folders))

        for index, folderID in enumerate(folders):
            folderName = getItemName(folderID)
            if folderID == folderIDR:  # skip reference folder
                continue

            nodes = Node.getFolderChildren(folderID)
            DENSMode = 'DENS' in nodes and nodes['DENS']            

            if DENSMode:
                DENSNode = nodes['DENS']
            ACTMNode = nodes['ACTM']
            if DENSMode:
                setSlicerViews(
                    densRnode.data.GetID(), DENSNode.data.GetID())
            else:
                setSlicerViews(
                    actmRnode.data.GetID(), ACTMNode.data.GetID())
            if not self.testing:
                if DENSMode:
                    ProgressDialog.labelText = f"Registering {DENSNode.name} to {densRnode.name}..."
                else:
                    ProgressDialog.labelText = f"Registering {ACTMNode.name} to {actmRnode.name}..."
                ProgressDialog.value = index
                if ProgressDialog.wasCanceled:
                    break
                slicer.app.processEvents()

            # Create transformation
            if DENSMode:
                transformName = DENSNode.name.replace(
                    'DENS', 'TRNF').replace('CT', 'LinearTransform')
            else:
                transformName = ACTMNode.name.replace(
                    'ACTM', 'TRNF').replace('SPECT', 'LinearTransform')
            if 'TRNF' not in nodes:
                transformNode = slicer.mrmlScene.AddNewNodeByClass(
                    'vtkMRMLTransformNode')
                transformNode.SetName(transformName)
                transformID = getItemID(transformNode)
                # here it is the SH ID
                setNodeFolder(transformNode, folderID)

                # Get origin difference and apply as initial transform
                if DENSMode:
                    originNode = DENSNode.getOrigin()
                    spacingNode = DENSNode.getSpacing()
                else:
                    originNode = ACTMNode.getOrigin()
                    spacingNode = ACTMNode.getSpacing()
                positionNode = np.array([originNode[0]*spacingNode[0],originNode[1]*spacingNode[1],originNode[2]*spacingNode[2]])
                TranslationVector = positionRef-positionNode
                vtransform = vtk.vtkTransform()
                vtransform.Translate(TranslationVector)
                transformNode.SetMatrixTransformToParent(
                    vtransform.GetMatrix())
                useAffine = False
            else:
                transformNode = nodes['TRNF'].data
                useAffine = True
            # The procedures below require MRML ID
            transformID = transformNode.GetID()

            # Apply transformation to all volumes inside the folder
            for key, node in nodes.items():
                if key == 'TABL' or key == 'SEGM':
                    continue
                if key == 'ADRM':
                    for inode in node.values():
                        inode.data.SetAndObserveTransformNodeID(
                            transformID)
                else:
                    node.data.SetAndObserveTransformNodeID(transformID)

            # Perform the registration
            if not self.testing:
                ProgressDialog.labelText = f"Performing registration in {folderName}..."
                slicer.app.processEvents()

            if USE_ELASTIX:  # Try first elastix
                self.logger.info("Using Elastix")
                transformNode.SetName(transformName.replace(
                    'LinearTransform', 'ElastixTransform'))
                registrationMode = 'Elastix'
                useAffine = False
                useRigid = False
                algorithm = 'Automatic registration'
                if DENSMode:
                    elastixLogic.registerVolumes(
                        densRnode.data, DENSNode.data,
                        parameterFilenames=parameterFilenamesElastix,
                        outputTransformNode=transformNode
                    )
                else:
                    elastixLogic.registerVolumes(
                        actmRnode.data, ACTMNode.data,
                        parameterFilenames=parameterFilenamesRigid,
                        outputTransformNode=transformNode
                    )

            else:  # elastix does not worked, try brainsfit
                self.logger.info("Using Brains linear")
                transformNode.SetName(transformName.replace(
                    'ElastixTransform', 'LinearTransform'))
                registrationMode = 'useGeometryAlign'
                algorithm = 'Semi-automatic registration'
                useRigid = True
                parameters = {
                    'fixedVolume': densRnode.data,
                    'movingVolume': DENSNode.data,
                    'samplingPercentage': 0.02,
                    'linearTransform': transformID,
                    'initialTransform': transformID,
                    'initializeTransformMode': registrationMode,
                    'useRigid': useRigid,
                    'useAffine': useAffine,
                    'maximumStepLength': 0.5}

                slicer.cli.run(slicer.modules.brainsfit, None,
                               parameters, wait_for_completion=True, update_display=False)

            if DENSMode:
                transformNode.SetAttribute(
                    Attributes().registrationVolume, str(densRnode.nodeID))
                transformNode.SetAttribute(
                    Attributes().registrationReference, str(DENSNode.nodeID))
            else:
                transformNode.SetAttribute(
                    Attributes().registrationVolume, str(actmRnode.nodeID))
                transformNode.SetAttribute(
                    Attributes().registrationReference, str(ACTMNode.nodeID))
            transformNode.SetAttribute(
                Attributes().registrationSampling, str(0.02))
            transformNode.SetAttribute(
                Attributes().registrationMode, registrationMode)
            transformNode.SetAttribute(
                Attributes().registrationRigid, str(useRigid))
            transformNode.SetAttribute(
                Attributes().registrationAffine, str(useAffine))
            transformNode.SetAttribute(
                Attributes().registrationAlgorithm, algorithm)
            transformNode.SetAttribute(
                Attributes().studyCreation, getCurrentTime())
            transformNode.SetAttribute(Attributes().modality, 'REG')
            slicer.app.processEvents()

        if not self.testing:
            ProgressDialog.close()

    def o_LED(self, folderID, density, threshold):
        ''' Method to perform LED in one time point
        '''
        nodes = Node.getFolderChildren(folderID)
        if 'ACTM' not in nodes:
            self.logger.error(Constants().strings['Scaling not Done'])
            return

        actNode = nodes['ACTM']

        if 'DERS' not in nodes:
            minNodes = Node.getFolderChildren(self.minFolderID)

            if 'DERS' not in minNodes:
                self.logger.error(Constants().strings['Scaling not Done'])
                return

            DENSNode = minNodes['DERS']
        else:
            DENSNode = nodes['DERS']

        self.logger.info(f"Reading node {actNode.name}")
        act_arr = actNode.getArrayData()
        # This threshold is to eliminate statistical noise
        th = max(act_arr.ravel()) * threshold
        # set threshold to avoid noise
        new_arr = np.where(act_arr > th, act_arr,
                           0)  # In this case we need Bq

        voxelVolume = np.prod(actNode.getSpacing()) * Constants().units['mm3']

        self.logger.info(f"Reading node {DENSNode.name}")
        mass_arr = DENSNode.getArrayData() * voxelVolume  # get the mass per voxel (kg)
        mass_vox = density * voxelVolume

        if np.array_equal(np.shape(act_arr), np.shape(mass_arr)):
            # The following is in mGy/h
            if self.densityCorrection:
                new_arr = np.divide(new_arr, mass_arr, out=np.zeros_like(
                    new_arr), where=mass_arr > 0)   # avoid division by 0
            else:
                new_arr = new_arr / mass_vox
            new_arr = new_arr * Constants().units['MBq'] * \
                self.Isotope['energy'] / Constants().units['mGy_h']
            # create the dose volume
            lmode = 'LocalEnergyDepositionLED'
            newName = actNode.name.replace('ACTM', 'ADRM').replace(
                'SPECT', lmode)
            if 'ADRM' in nodes and lmode in nodes['ADRM']:
                slicer.mrmlScene.RemoveNode(nodes['ADRM'][lmode].data)

            clonedNode = cloneNode(actNode.data, newName)
            clonedItemID = getItemID(clonedNode)
            clonedNode.SetAttribute(Attributes().modality, 'RTDOSE')
            clonedNode.SetAttribute(
                Attributes().isotope, str(self.Isotope['name']))
            clonedNode.SetAttribute(
                Attributes().doseACTMVolume, str(actNode.nodeID))
            clonedNode.SetAttribute(
                Attributes().doseAlgorithm, 'Local Energy Deposition')
            clonedNode.SetAttribute(
                Attributes().studyCreation, getCurrentTime())

            DOSENode = Node.new(clonedItemID)
            DOSENode.setArrayData(new_arr)

            self.logger.info(f'Node {newName} created...')
            return True

        self.logger.error(
            f"Wrong imput: Images {actNode.name} and {DENSNode.name} don't have the same shape")
        return False

    def LED(self, DVK_material='water', threshold=0.1):
        ''' method to perform local energy deposition in each time point prior to integration
            TODO: this shall be parallel, but is not working, it is fast anyway
        '''

        self.logger.info("Performing local energy deposition")
        folders = getFolderIDs()
        for folderID in folders:
            self.o_LED(folderID, Constants(
            ).materials[DVK_material]['density'], threshold)

    def buildKernel(self):
        kernel = np.zeros(
            (2 * self.topx + 1, 2 * self.topy + 1, 2 * self.topz + 1))
        for i in np.arange(-self.topx, self.topx + 1):
            for j in np.arange(-self.topy, self.topy + 1):
                for k in np.arange(-self.topz, self.topz + 1):
                    distance = int(round(np.sum(np.square([i, j, k]))))
                    identifier = int(abs(i) + abs(j) + abs(k))
                    kernel[i + self.topx, j + self.topy, k +
                           self.topz] = self.DVK[distance][identifier][0]
        return kernel

    def buildDistanceKernel(self):
        kernel = np.zeros(
            (2 * self.topx + 1, 2 * self.topy + 1, 2 * self.topz + 1), dtype=np.int)
        for i in np.arange(-self.topx, self.topx + 1):
            for j in np.arange(-self.topy, self.topy + 1):
                for k in np.arange(-self.topz, self.topz + 1):
                    kernel[i + self.topx, j + self.topy, k +
                           self.topz] = int(round(np.sum(np.square([i, j, k]))))
        return kernel

    def __convolute(self, folderID, lmode, threshold, multithreaded):
        ''' convolution in the entire 3D matrix for one time point
        '''
        nodes = Node.getFolderChildren(folderID)

        if 'ACTM' not in nodes:
            self.logger.error(Constants().strings['Scaling not Done'])
            return

        actNode = nodes['ACTM']

        if 'DERS' not in nodes:
            minNodes = Node.getFolderChildren(self.minFolderID)

            if 'DERS' not in minNodes:
                self.logger.error(Constants().strings['Scaling not Done'])
                return

            DENSNode = minNodes['DERS']
        else:
            DENSNode = nodes['DERS']

        self.logger.info(f"Reading node {actNode.name}")
        act_arr = actNode.getArrayData()
        shape = np.shape(act_arr)
        size = 1
        for x in shape:
            size *= x
        self.size = size

        self.logger.info(f"Reading node {DENSNode.name}")
        densityArray = DENSNode.getArrayData() * Constants().units['kg_m3']
        activityArray = act_arr * Constants().units['MBq']

        # This threshold is to eliminate statistical noise
        canceled = False
        act_ravel = activityArray.ravel()
        threshold = max(act_ravel) * threshold
        act_arr = np.where(  # Apply threshold for consistency
            activityArray > threshold, activityArray, 0)
        # indexes of valid activities
        indexes = np.array(np.nonzero(act_ravel > threshold)).ravel()
        if np.array_equal(np.shape(activityArray), np.shape(densityArray)):
            labelText = ""
            if not self.testing:
                labelText = f"Processing {getItemName(folderID)}..."
                ProgressDialog = slicer.util.createProgressDialog(
                    parent=None, value=0, maximum=100)
                ProgressDialog.labelText = labelText
                ProgressDialog.show()
                slicer.app.processEvents()
            if 'FFT' in lmode:  # Fourier
                if not self.testing:
                    ProgressDialog.value = 1
                    slicer.app.processEvents()
                kernel = self.buildKernel()
                conv_arr = signal.convolve(
                    in1=act_arr, in2=kernel, mode="same")
                if self.densityCorrection:  # Apply density correction
                    new_arr = np.divide(conv_arr, densityArray, out=np.zeros_like(
                        conv_arr), where=densityArray > 0)
                else:  # just divide by water density
                    new_arr = conv_arr / (1.0*Constants().units['g_cm3'])
                if not self.testing:
                    ProgressDialog.close()
                    slicer.app.processEvents()
            else:  # matrix convolution
                new_arr1 = np.zeros(self.size)
                acc = 0
                length1 = np.prod(np.shape(indexes))
                # Initialize distance kernel
                distanceKernel = self.buildDistanceKernel()
                p0 = [self.topx, self.topy, self.topz]
                if 'homogeneous' in lmode:
                    num = 2
                else:
                    num = np.max(p0)*20

                if multithreaded:
                    if not self.testing:
                        ProgressDialog.close()
                    scriptPath = self.script_path / "Logic" / "CLIScripts" / "Convolution.py"

                    chunks = 100  # to avoid overheading
                    iteration = 0
                    args1 = {  # wrap arguments for multithreading, save common objects to share
                        'p0': p0,
                        'DVK': self.DVK,
                        'distance_kernel': distanceKernel,
                        'dens_array': densityArray,
                        'act_array': act_arr,
                        'num': num,
                        'boundary': 'repeat'
                    }
                    with tempfile.TemporaryDirectory() as dirpath:  # Create a temporary path
                        pickleFileName = Path(dirpath) / "OpenDose4D.pckl"
                        with pickleFileName.open('wb') as f:
                            pickle.dump(args1, f)

                        lindexes = {}
                        with ProcessesLogic(windowTitle=labelText) as plogic:
                            while acc < length1:
                                stepsize = max(
                                    1, min(int(length1/chunks), length1-acc))
                                lindex = {'indexes': np.array(
                                    indexes[acc:acc + stepsize])}
                                lindexes[iteration] = lindex['indexes']
                                convolutionProcess = ConvolutionProcess(
                                    scriptPath, str(pickleFileName), lindex, iteration)
                                plogic.addProcess(convolutionProcess)
                                acc += stepsize
                                iteration += 1

                            slicer.app.processEvents()
                            plogic.run()

                        plogic.waitForFinished()
                        canceled = plogic.canceled
                        if canceled:  # Avoid partial results
                            return False

                        for it, elem in lindexes.items():
                            new_arr1[elem] = plogic.results[it]

                else:
                    a1 = 0.0
                    chunks = 1000  # to avoid unresponsiveness
                    convolution = Convolution(
                        [], p0, self.DVK, distanceKernel, densityArray, act_arr, num, 'repeat')
                    while acc < length1:
                        stepsize = max(
                            1, min(int(length1/chunks), length1-acc))
                        if not self.testing and ProgressDialog.wasCanceled:
                            canceled = True
                            slicer.app.processEvents()
                            break
                        convolution.indexes = np.array(
                            indexes[acc:acc + stepsize])

                        new_arr2 = convolution.convolute()
                        new_arr1[convolution.indexes] = new_arr2

                        acc += stepsize
                        a1 += 100/chunks
                        if a1 >= 1:
                            if not self.testing:
                                ProgressDialog.value += a1
                                labelText1 = f"{labelText} \n{acc} of {length1}"
                                ProgressDialog.labelText = labelText1
                            a1 = 0

                    if not self.testing:
                        ProgressDialog.close()
                        slicer.app.processEvents()

                if canceled:  # Avoid partial results
                    return False

                new_arr = np.reshape(new_arr1, shape, 'C')

            # express in mGy/h
            new_arr = new_arr / Constants().units['mGy_h']

            newName = actNode.name.replace(
                'ACTM', 'ADRM').replace('SPECT', lmode)

            if 'ADRM' in nodes and lmode in nodes['ADRM']:
                slicer.mrmlScene.RemoveNode(nodes['ADRM'][lmode].data)

            clonedNode = cloneNode(actNode.data, newName)
            clonedItemID = getItemID(clonedNode)
            clonedNode.SetAttribute(Attributes().modality, 'RTDOSE')
            clonedNode.SetAttribute(
                Attributes().isotope, str(self.Isotope['name']))
            clonedNode.SetAttribute(
                Attributes().doseACTMVolume, str(actNode.nodeID))
            clonedNode.SetAttribute(Attributes().doseAlgorithm, 'Convolution')
            clonedNode.SetAttribute(
                Attributes().doseAlgorithmDescription, lmode)
            clonedNode.SetAttribute(
                Attributes().studyCreation, getCurrentTime())
            clonedNode.SetAttribute(
                Attributes().doseKernelLimit, str(self.kernellimit))

            DOSENode = Node.new(clonedItemID)
            DOSENode.setArrayData(new_arr)

            self.logger.info(f'Node {newName} created...')
            return True
        else:
            self.logger.error(
                f"Wrong imput: Images {actNode.name} and {DENSNode.name} don't have the same shape")
            return False

    def convolution(self, isotopeText, DVK_size=None, DVK_material='water', threshold=0.1, kernellimit=0.0, mode='density_correction', multithreaded=False):
        """@brief Method to perform convolution in all time points prior to integration

            TODO: this shall be parallel, but is not working, it is fast anyway

        @param threshold this is the value in percentage of the maximum of the activity map that will be used for voxel discrimination.
        @param kernellimit this is the maximum distance allowed for the kernel, 0 means use all kernel.
        """
        import ast
        from .dbutils import readDVK
        referenceId = self.getReferenceFolder()
        refNodes = Node.getFolderChildren(referenceId)
        self.spacing = refNodes['ACSC'].getSpacing()
        self.DVK = {}
        if DVK_size is None:
            self.DVK_size = np.multiply(self.spacing, Constants().units['mm'])
            self.logger.info(
                f"Loading DVK {isotopeText} - {self.DVK_size/Constants().units['mm']} mm")
        elif np.isscalar(DVK_size):
            self.DVK_size = np.array([DVK_size * Constants().units['mm']]*3)
        else:
            self.DVK_size = np.array(DVK_size * Constants().units['mm'])

        self.DVK_density = Constants().materials[DVK_material]['density']

        sizelow = np.floor(np.multiply(self.spacing, 10))/10 * Constants().units['mm']
        sizehigh = np.ceil(np.multiply(self.spacing, 10))/10 * Constants().units['mm']
        DVKlow = {}
        while not DVKlow:
            DVKlow = readDVK(isotopeText=isotopeText, material=DVK_material,
                                 DVK_size=sizelow, compressed=self.compressed)
            if not DVKlow:
                sizelow -= 0.1 * Constants().units['mm']
                if sizelow < 1.0 * Constants().units['mm']:
                    raise IOError("Can't find lower bound data file")
        DVKhigh = {}
        while not DVKhigh:
            DVKhigh = readDVK(isotopeText=isotopeText, material=DVK_material,
                                  DVK_size=sizehigh, compressed=self.compressed)
            if not DVKhigh:
                sizehigh += 0.1 * Constants().units['mm']
                if sizehigh > 6.0 * Constants().units['mm']:
                    raise IOError("Can't find higher bound data file")
        xs = [sizelow[0], sizehigh[0]]

        topdist = 0
        self.topx = 0
        self.topy = 0
        self.topz = 0

        # Rectify limit so we dont have empty slices afterwards
        if kernellimit > 0:
            self.kernellimit = np.max(
                self.DVK_size*np.ceil(np.array([kernellimit]*3)/self.DVK_size))
        else:
            self.kernellimit = 0

        for coord in DVKlow:
            l = ast.literal_eval(coord)
            distance = np.linalg.norm(l*self.DVK_size)/Constants().units['mm']
            # kernel limit is already in mm
            if np.all((l*self.DVK_size/Constants().units['mm']) <= self.kernellimit) or self.kernellimit == 0:
                d1 = int(round(np.sum(np.square(l))))
                if not d1 in self.DVK:
                    self.DVK[d1] = {}
                dvkvalue = np.interp(self.DVK_size[0], xs,
                                     [DVKlow[coord][0], DVKhigh[coord][0]])
                dvkerror = dvkvalue * \
                    np.sqrt((DVKlow[coord][1]/DVKlow[coord][0])**2 +
                            (DVKhigh[coord][1]/DVKhigh[coord][0])**2)
                # avoid overwrite non simmetrycal voxels at exact same euclidean distance
                identifier = int(np.sum(np.abs(l)))
                self.DVK[d1][identifier] = [dvkvalue*Constants().units['mGy_MBqs']*self.DVK_density,
                                            dvkerror*Constants().units['mGy_MBqs']*self.DVK_density]
                if distance > topdist:
                    topdist = distance
                if l[0] > self.topx:
                    self.topx = l[0]
                if l[1] > self.topy:
                    self.topy = l[1]
                if l[2] > self.topz:
                    self.topz = l[2]
        folders = getFolderIDs()
        for fID in folders:
            if not self.__convolute(fID, mode, threshold, multithreaded):
                break

    def Propagate(self, segmentationNode):
        ''' Propagates reference segmentation in all time points
          TODO: make all segmenttion not visible by default
        '''
        nodes = Node.getFolderChildren(self.referenceFolderID)
        if 'DERS' not in nodes:
            self.logger.error(
                'Densities not present, please create them before propagating segmentations')
            return

        folders = getFolderIDs()
        shNode = getSubjectHierarchyNode()

        oldsegmNode = segmentationNode.GetName()
        segnodeID = getItemID(segmentationNode)
        segmentationNode.GetDisplayNode().SetVisibility(0)
        if not self.testing:
            ProgressDialog = slicer.util.createProgressDialog(
                parent=None, value=0, maximum=len(folders))

        for index, folderID in enumerate(folders):
            folderName = getItemName(folderID)
            nodes = Node.getFolderChildren(folderID)
            if not self.testing:
                ProgressDialog.labelText = f"Processing {folderName}..."
                ProgressDialog.value = index
                if ProgressDialog.wasCanceled:
                    break
                slicer.app.processEvents()
            DERSNode = nodes['DERS']
            newName = DERSNode.name.replace('DERS', 'SEGM')
            if newName == oldsegmNode:  # This is already the reference, put it in the right place just in case
                setNodeFolder(segmentationNode, folderID)
                setItemName(segnodeID, newName)
                segmentationNode.SetAttribute(
                    Attributes().modality, 'RTSTRUCT')
                segmentationNode.SetAttribute(
                    Attributes().studyCreation, getCurrentTime())
                segmentationNode.Modified()
                continue
            newsegmNode = slicer.util.getFirstNodeByClassByName(
                'vtkMRMLSegmentationNode', newName)
            while newsegmNode:
                slicer.mrmlScene.RemoveNode(newsegmNode)
                newsegmNode = slicer.util.getFirstNodeByClassByName(
                    'vtkMRMLSegmentationNode', newName)
            newsegmID = slicer.modules.subjecthierarchy.logic(
            ).CloneSubjectHierarchyItem(shNode, segnodeID, newName)
            newsegmNode = shNode.GetItemDataNode(newsegmID)
            newsegmNode.CreateDefaultDisplayNodes()
            newsegmNode.CreateClosedSurfaceRepresentation()
            newsegmNode.GetDisplayNode().SetVisibility(0)
            newsegmNode.SetReferenceImageGeometryParameterFromVolumeNode(
                DERSNode.data)
            setNodeFolder(newsegmNode, folderID)
            newsegmNode.SetAttribute(Attributes().modality, 'RTSTRUCT')
            newsegmNode.SetAttribute(
                Attributes().studyCreation, getCurrentTime())
            newsegmNode.Modified()

        if not self.testing:
            ProgressDialog.close()

    def setupModeAlgo(self, mode, algo):
        # TODO remove this method with a better approach
        if 'dose' in mode.lower():
            if 'FFT' in algo:
                lalgo = 'FFT convolution (homogeneous)'
            elif 'homogeneous' in algo:
                lalgo = 'Convolution (homogeneous)'
            elif 'heterogeneous' in algo:
                lalgo = 'Convolution (heterogeneous)'
            elif 'monte' in algo.lower():
                lalgo = 'MonteCarlo'
            else:
                lalgo = 'LocalEnergyDepositionLED'
        else:
            lalgo = 'Activity'
        return lalgo

    def computeNewValues(self, mode, algorithm=''):
        '''
        update tables once images are normalized
        '''
        lalgo = self.setupModeAlgo(mode, algorithm)
        segmentationNodes = slicer.util.getNodesByClass(
            'vtkMRMLSegmentationNode')
        segmentationNodesNames = [segmentationNode.GetName()
                                  for segmentationNode in segmentationNodes]
        if len(segmentationNodesNames) == 0:
            slicer.util.errorDisplay(
                "No Segmentation found, please create one")
            self.logger.debug("No Segmentation found, please create one")
            return
        segmentationNode = segmentationNodes[0]
        folders = getFolderIDs()
        if not self.testing:
            ProgressDialog = slicer.util.createProgressDialog(
                parent=None, value=0, maximum=len(folders))

        for index, folderID in enumerate(folders):
            folderName = getItemName(folderID)
            nodes = Node.getFolderChildren(folderID)
            if not self.testing:
                ProgressDialog.labelText = f"Processing {folderName}..."
                ProgressDialog.value = index
                if ProgressDialog.wasCanceled:
                    break
                slicer.app.processEvents()

            if 'SEGM' in nodes:
                segmNode = nodes['SEGM'].data
            else:
                segmNode = segmentationNode

            if 'dose' in mode.lower():
                masterNode = nodes['ADRM'][lalgo]
                newName = masterNode.name.replace(':ADRM', ':TABL').replace(
                    'LocalEnergyDepositionLED', 'DoseRateLED').replace('Convolution', 'DoseRateCONV').replace('fast convolution', 'DoseRateFFT')
            else:
                masterNode = nodes['ACTM']
                newName = masterNode.name.replace(
                    ':ACTM', ':TABL').replace('SPECT', 'Activity')
            setSlicerViews(
                nodes['DERS'].data.GetID(), masterNode.data.GetID())

            tmode = extractMode(newName)
            if 'TABL' in nodes and tmode in nodes['TABL']:
                slicer.mrmlScene.RemoveNode(nodes['TABL'][tmode].data)

            resultsTableNode = slicer.mrmlScene.AddNewNodeByClass(
                'vtkMRMLTableNode')
            resultsTableNode.SetName(newName)
            setNodeFolder(resultsTableNode, folderID)

            self.logger.info(
                f"Processing node {masterNode.name} with segmentation {segmNode.GetName()}")
            helperSegmentStatisticsTable(
                segmNode, masterNode.data, resultsTableNode)
            resultsTableNode.SetAttribute(
                Attributes().tableCreationMode, tmode)
            if not hasTable(resultsTableNode):
                raise IOError(f"Failed to create Table {newName}")

            # Get the masses
            DERSNode = nodes['DERS']
            newName = DERSNode.name.replace(
                ':DERS', ':TABL').replace('SPECT', 'Densities')
            tmode = extractMode(newName)
            if 'TABL' in nodes and tmode in nodes['TABL']:
                slicer.mrmlScene.RemoveNode(nodes['TABL'][tmode].data)

            resultsDTableNode = slicer.mrmlScene.AddNewNodeByClass(
                'vtkMRMLTableNode')
            resultsDTableNode.SetName(newName)
            setNodeFolder(resultsDTableNode, folderID)

            helperSegmentStatisticsTable(
                segmNode, DERSNode.data, resultsDTableNode)

            resultsDTableNode.SetAttribute(
                Attributes().tableCreationMode, tmode)

            if not hasTable(resultsDTableNode):
                raise IOError(f"Failed to create Table {newName}")

        if not self.testing:
            ProgressDialog.close()

    def processTimeIntegrationVariables(self, mode, algorithm=''):
        '''
        Creation of the data object to generate plots and integration in time
        '''
        lalgo = self.setupModeAlgo(mode, algorithm)
        if not mode in self.data:
            self.data[mode] = {}
            self.tableheader[mode] = {}
        self.tableheader[mode][lalgo] = []  # reinitialize the headers, always
        folders = getFolderIDs()
        if not self.testing:
            ProgressDialog = slicer.util.createProgressDialog(
                parent=None, value=0, maximum=len(folders))

        l_dose = []
        l_density = []
        delay = []
        for index, folderID in enumerate(folders):
            folderName = getItemName(folderID)
            nodes = Node.getFolderChildren(folderID)
            if not self.testing:
                ProgressDialog.labelText = f"Processing {folderName}..."
                ProgressDialog.value = index
                if ProgressDialog.wasCanceled:
                    break
                slicer.app.processEvents()
            # Get time points in Hours
            timestamp = nodes['ACTM'].getTimeStamp()
            delay.append(float(timestamp))  # already in HR
            # Get segment mass in kg
            DERSNode = nodes['DERS']
            newName = DERSNode.name.replace(
                ':DERS', ':TABL').replace('SPECT', 'Densities')
            tmode = extractMode(newName)
            tabledensitynode = getItemDataNode(
                nodes['TABL'][tmode].nodeID)
            densities_cols = []
            for row in range(tabledensitynode.GetNumberOfRows()):
                densities_cols.append(float(tabledensitynode.GetCellText(
                    row, 2)) * Constants().units['cm3'] * float(tabledensitynode.GetCellText(row, 3)))
            l_density.append(densities_cols)
            # Get the Absorbed Dose Rate (Activity) in Gy/s (MBq)
            if 'dose' in mode.lower():
                masterNode = nodes['ADRM'][lalgo]
                newName = masterNode.name.replace(':ADRM', ':TABL').replace(
                    'LocalEnergyDepositionLED', 'DoseRateLED').replace('Convolution', 'DoseRateCONV').replace('fast convolution', 'DoseRateFFT')
            else:
                masterNode = nodes['ACTM']
                newName = masterNode.name.replace(
                    ':ACTM', ':TABL').replace('SPECT', 'Activity')

            tmode = extractMode(newName)
            resultsTableNode = nodes['TABL'][tmode]
            tableDOSENode = getItemDataNode(
                resultsTableNode.nodeID)

            self.tableheader[mode][lalgo].append(resultsTableNode.name)
            dose_cols = []
            for row in range(tableDOSENode.GetNumberOfRows()):
                if 'dose' in mode.lower():  # mean is what we want
                    dose_cols.append(
                        float(tableDOSENode.GetCellText(row, 3)))
                else:  # We need the total, not the mean
                    dose_cols.append(float(tableDOSENode.GetCellText(
                        row, 1)) * float(tableDOSENode.GetCellText(row, 3)))
            l_dose.append(dose_cols)
        ddose = np.array(l_dose)
        ddensity = np.array(l_density)
        delay = np.array(delay)
        ind = delay.argsort()  # Sort by time
        delay = delay[ind]
        ddose = ddose[ind]
        ddensity = ddensity[ind]
        ddosefit = np.copy(ddose)
        self.data[mode][lalgo] = np.vstack(
            [delay, ddensity.T, ddose.T, ddosefit.T])

        if not self.testing:
            ProgressDialog.close()

    def getTstack(self, data):
        tstack = []
        for elem in data.T:
            tstack.append(np.hstack(elem).tolist())

        sizeX = len(tstack)
        sizeY = len(tstack[0])
        res = np.zeros([sizeX, sizeY])
        for i in range(sizeX):
            for j in range(sizeY):
                res[i, j] = tstack[i][j]

        return res

    def showTableandPlotTimeIntegrationVariables(self, mode, algorithm='', showlegend=False):
        import matplotlib._color_data as mcd
        lalgo = self.setupModeAlgo(mode, algorithm)
        name = f'T:TABL {mode} {lalgo} summary'
        tableNode = slicer.util.getFirstNodeByClassByName(
            'vtkMRMLTableNode', name)
        if tableNode:
            slicer.mrmlScene.RemoveNode(tableNode)

        tableNode = slicer.mrmlScene.AddNewNodeByClass(
            'vtkMRMLTableNode', name)

        resultsTableNodeID = getResultIDs()[0]
        setNodeFolder(tableNode, resultsTableNodeID)

        ltnode = slicer.util.getFirstNodeByClassByName(
            'vtkMRMLTableNode', self.tableheader[mode][lalgo][0])
        lrows = ltnode.GetNumberOfRows()

        header_cells_mass = [f'M_{ltnode.GetCellText(i, 0)}'
                             for i in range(lrows)]
        if 'dose' in mode.lower():
            addedY = ' (mGy/h)'
            header_cells_doserate = [f'DR_{ltnode.GetCellText(i, 0)}'
                                     for i in range(lrows)]
            header_cells_doserate_fit = [f'DR_{ltnode.GetCellText(i, 0)}_fit'
                                         for i in range(lrows)]
        else:
            addedY = ' (MBq)'
            header_cells_doserate = [f'CA_{ltnode.GetCellText(i, 0)}'
                                     for i in range(lrows)]
            header_cells_doserate_fit = [f'CA_{ltnode.GetCellText(i, 0)}_fit'
                                         for i in range(lrows)]

        xname = "time (h)"
        columnNames = [xname] + header_cells_mass + \
            header_cells_doserate + header_cells_doserate_fit

        slicer.util.updateTableFromArray(
            tableNode, self.getTstack(self.data[mode][lalgo]), columnNames)
        tableNode.SetAttribute(
            Attributes().studyCreation, getCurrentTime())
        tableNode.Modified()

        # Remove previous chart
        chartname = name.replace('TABL', 'CHRT')
        plotChartNode = slicer.util.getFirstNodeByClassByName(
            'vtkMRMLPlotChartNode', chartname)
        if plotChartNode:
            slicer.mrmlScene.RemoveNode(plotChartNode)
        # now create the new chart and the plots
        plotChartNode = slicer.mrmlScene.AddNewNodeByClass(
            "vtkMRMLPlotChartNode", chartname)
        plotChartNode.SetTitle(chartname)
        plotChartNode.SetXAxisTitle(xname)
        plotChartNode.SetYAxisTitle(mode + addedY)
        plotChartNode.SetYAxisLogScale(self.logyaxis)
        setNodeFolder(plotChartNode, resultsTableNodeID)
        ColorsHex = list(mcd.CSS4_COLORS.values())[10:] # First 10 colors are too dark
        Colors = list(tuple(int(h[i:i+2], 16) for i in (1, 3, 5)) for h in ColorsHex)

        for i, header in enumerate(header_cells_doserate):
            # Remove previous plots
            plotname = f"P:PLOT {lalgo} {header}"
            plotSeriesNode = slicer.util.getFirstNodeByClassByName(
                "vtkMRMLPlotSeriesNode", plotname)
            while plotSeriesNode:
                slicer.mrmlScene.RemoveNode(plotSeriesNode)
                plotSeriesNode = slicer.util.getFirstNodeByClassByName(
                    "vtkMRMLPlotSeriesNode", plotname)

            # Now create the plots
            plotSeriesNode = slicer.mrmlScene.AddNewNodeByClass(
                "vtkMRMLPlotSeriesNode", plotname)

            plotSeriesNode.SetAndObserveTableNodeID(tableNode.GetID())
            plotSeriesNode.SetXColumnName(xname)
            plotSeriesNode.SetYColumnName(header)
            plotSeriesNode.SetPlotType(
                slicer.vtkMRMLPlotSeriesNode.PlotTypeScatter)
            plotSeriesNode.SetLineStyle(
                slicer.vtkMRMLPlotSeriesNode.LineStyleNone)
            plotSeriesNode.SetMarkerStyle(
                slicer.vtkMRMLPlotSeriesNode.MarkerStyleCircle)
            plotSeriesNode.SetColor(Colors[i])
            setNodeFolder(plotSeriesNode, resultsTableNodeID)
            # Add to chart
            plotChartNode.AddAndObservePlotSeriesNodeID(plotSeriesNode.GetID())

        for i, header in enumerate(header_cells_doserate_fit):
            # Remove previous plots
            plotname = f"P:PLOT {lalgo} {header}"
            plotSeriesNode = slicer.util.getFirstNodeByClassByName(
                "vtkMRMLPlotSeriesNode", plotname)
            while plotSeriesNode:
                slicer.mrmlScene.RemoveNode(plotSeriesNode)
                plotSeriesNode = slicer.util.getFirstNodeByClassByName(
                    "vtkMRMLPlotSeriesNode", plotname)

            # Now create the plots
            plotSeriesNode = slicer.mrmlScene.AddNewNodeByClass(
                "vtkMRMLPlotSeriesNode", plotname)
            setNodeFolder(plotSeriesNode, resultsTableNodeID)

            plotSeriesNode.SetAndObserveTableNodeID(tableNode.GetID())
            plotSeriesNode.SetXColumnName(xname)
            plotSeriesNode.SetYColumnName(header)
            plotSeriesNode.SetPlotType(
                slicer.vtkMRMLPlotSeriesNode.PlotTypeScatter)
            plotSeriesNode.SetLineStyle(
                slicer.vtkMRMLPlotSeriesNode.LineStyleSolid)
            plotSeriesNode.SetMarkerStyle(
                slicer.vtkMRMLPlotSeriesNode.MarkerStyleNone)
            plotSeriesNode.SetColor(Colors[i])
            # Add to chart
            plotChartNode.AddAndObservePlotSeriesNodeID(plotSeriesNode.GetID())

        plotChartNode.SetLegendVisibility(showlegend)
        plotChartNode.Modified()
        # Show chart in layout
        showChartinLayout(plotChartNode)
        # Refresh Results folder view
        RefreshFolderView(resultsTableNodeID)

    def integrate(self, mode, algorithm='', incorporationmode='linear', integrationmode='trapezoid', tailmode='physical', forceExtraPoint=True):
        from numpy import log, exp
        lalgo = self.setupModeAlgo(mode, algorithm)
        name = f'T:TABL {mode} {lalgo} summary'
        tableNode = slicer.util.getFirstNodeByClassByName(
            'vtkMRMLTableNode', name)
        if 'dose' in mode.lower():
            lunit = '(Gy)'
            ltext = 'Absorbed Dose'
        else:
            lunit = '(MBqh)'
            ltext = 'Cummulated Activity'
        x_values = self.data[mode][lalgo][0]
        ltnode = slicer.util.getFirstNodeByClassByName(
            'vtkMRMLTableNode', self.tableheader[mode][lalgo][0])
        lrows = ltnode.GetNumberOfRows()
        header_cells_mass = [f'M_{ltnode.GetCellText(i, 0)}'
                             for i in range(lrows)]
        if 'dose' in mode.lower():
            header_cells_doserate = [f'DR_{ltnode.GetCellText(i, 0)}'
                                     for i in range(lrows)]
            header_cells_doserate_fit = [f'DR_{ltnode.GetCellText(i, 0)}_fit'
                                         for i in range(lrows)]
            dose_values = self.data[mode][lalgo][len(header_cells_mass)+1:]
        else:
            header_cells_doserate = [f'CA_{ltnode.GetCellText(i, 0)}'
                                     for i in range(lrows)]
            header_cells_doserate_fit = [f'CA_{ltnode.GetCellText(i, 0)}_fit'
                                         for i in range(lrows)]
            dose_values = self.data[mode][lalgo][len(header_cells_mass)+1:]

        mass_values = self.data[mode][lalgo][1:len(header_cells_mass)+1]

        TableNodes = slicer.util.getNodesByClass('vtkMRMLTableNode')
        name = f'T:TABL {mode} {lalgo} integrated'
        for node in TableNodes:
            if name == node.GetName():  # Table exists, erase it
                slicer.mrmlScene.RemoveNode(node)

        # prepare clean table
        resultsTableNode = slicer.mrmlScene.AddNewNodeByClass(
            'vtkMRMLTableNode')
        resultsTableNode.RemoveAllColumns()
        resultsTableNodeID = getResultIDs()[0]
        setNodeFolder(resultsTableNode, resultsTableNodeID)

        resultsTableNode.SetName(name)
        table = resultsTableNode.GetTable()

        segmentColumnValue = vtk.vtkStringArray()
        segmentColumnValue.SetName("Segment")
        table.AddColumn(segmentColumnValue)

        segmentColumnValue = vtk.vtkStringArray()
        segmentColumnValue.SetName("Mass (kg)")
        table.AddColumn(segmentColumnValue)

        trapintColumnValue = vtk.vtkStringArray()
        trapintColumnValue.SetName(f'Data points integral {lunit}')
        table.AddColumn(trapintColumnValue)

        TotalColumnValue = vtk.vtkStringArray()
        TotalColumnValue.SetName(f'{ltext} {lunit}')
        table.AddColumn(TotalColumnValue)

        if not 'dose' in mode.lower():
            doseColumnValue = vtk.vtkStringArray()
            doseColumnValue.SetName('Absorbed Dose by LED (Gy)')
            table.AddColumn(doseColumnValue)

        differenceColumnValue = vtk.vtkStringArray()
        differenceColumnValue.SetName('STDDEV (Gy)')
        table.AddColumn(differenceColumnValue)

        FitColumnValue = vtk.vtkStringArray()
        FitColumnValue.SetName('Fit function')
        table.AddColumn(FitColumnValue)

        table.SetNumberOfRows(len(header_cells_doserate))
        xeff = x_values  # this in hours for fitting
        if xeff[0] <= 0:
            zeroincluded = True
        else:
            zeroincluded = False
            xeff = np.insert(xeff, 0, 0)
        if forceExtraPoint:
            xeff = np.append(xeff, x_values[-1]*10)

        resultsTableNode.SetAttribute("PKincorporation", incorporationmode)
        resultsTableNode.SetAttribute("PKalgorithm", integrationmode)
        resultsTableNode.SetAttribute("ADalgorithm", lalgo)

        if 'physical' in tailmode.lower():
            T_h = self.Isotope['T_h']  # in hours
        else:
            T_h = self.Isotope['T_eff']

        self.logger.info(f'T1/2 = {T_h:.3e}h')
        lamda = np.log(2) / T_h
        for i, (mass, dose) in enumerate(zip(mass_values, dose_values)):
            yeff = dose
            self.logger.info(f"Processing {header_cells_doserate[i]}")
            incorp = 0
            if not zeroincluded and x_values[0] > 0:
                if 'linear' in incorporationmode:  # incorporation part
                    incorp = xeff[1]*yeff[0]*0.5
                    yeff = np.insert(yeff, 0, 0)
                elif 'const' in incorporationmode:
                    incorp = xeff[1]*yeff[0]
                    yeff = np.insert(yeff, 0, yeff[0])
                elif 'exp' in incorporationmode:
                    incorp = yeff[0]*(exp(lamda*xeff[1])-1)/lamda
                    yeff = np.insert(yeff, 0, yeff[0]*exp(lamda*xeff[1]))                

            if forceExtraPoint:
                yeff = np.append(yeff, 0) # Force extra point to guarantee convergence
            fitrow = 2*lrows + i + 1
            amass = np.nanmean(mass)

            if zeroincluded:
                fit = FitValues(x=xeff, y=yeff, fit_type=integrationmode.lower())
            else:
                fit = FitValues(x=xeff[1:], y=yeff[1:], fit_type=integrationmode.lower()) 
            data_integral, data_integral_error, total_integral, dy, lamda, fit_function = fit.integrate(
                T_h=T_h)
            newy = []
            c = 0
            if zeroincluded:
                for lx, ly in zip(xeff, yeff):
                    if isNumber(lx) and isNumber(ly):
                        newy.append(dy[c])
                        c += 1
                    else:
                        newy.append(np.nan)
            else:
                for lx, ly in zip(xeff[1:], yeff[1:]):
                    if isNumber(lx) and isNumber(ly):
                        newy.append(dy[c])
                        c += 1
                    else:
                        newy.append(np.nan)

            xeff = np.ma.array(xeff, mask=np.isnan(xeff))
            yeff = np.ma.array(yeff, mask=np.isnan(yeff))
            newy = np.ma.array(newy, mask=np.isnan(newy))

            if forceExtraPoint:
                self.data[mode][lalgo][fitrow] = newy[:-1]
                if zeroincluded:
                    error = np.sqrt(np.std(newy[:-1]-yeff[:-1]) *
                            (xeff[-2]-xeff[0])**2 + data_integral_error**2)
                else:
                    error = np.sqrt(np.std(newy[:-1]-yeff[1:-1]) *
                            (xeff[-2]-xeff[1])**2 + data_integral_error**2)
            else:
                self.data[mode][lalgo][fitrow] = newy
                if zeroincluded:
                    error = np.sqrt(np.std(newy-yeff) *
                            (xeff[-1]-xeff[0])**2 + data_integral_error**2)
                else:
                    error = np.sqrt(np.std(newy-yeff[1:]) *
                            (xeff[-1]-xeff[1])**2 + data_integral_error**2)
            # To display the equations change to info
            self.logger.info(f'Fit function: {fit_function}')

            
            total_integral += incorp
            table.SetValue(i, 0, header_cells_doserate[i])
            table.SetValue(i, 1, f'{amass:.3e}')
            if 'dose' in mode.lower():  # units mGy/h * h -> mGy
                # output is in Gy
                table.SetValue(
                    i, 2, f"{data_integral * Constants().units['mGy']:.3e}")
                table.SetValue(
                    i, 3, f"{total_integral * Constants().units['mGy']:.3e}")
                table.SetValue(i, 4, f"{error * Constants().units['mGy']:.3e}")
                table.SetValue(i, 5, fit_function)
                self.logger.info(
                    f"AD to {header_cells_doserate[i]} = ({total_integral * Constants().units['mGy']:.3e} +/- {error * Constants().units['mGy']:.3e}) Gy")
            else:  # units MBq * h -> MBqh
                table.SetValue(i, 2, f'{data_integral:.3e}')  # output in MBqh
                table.SetValue(i, 3, f'{total_integral:.3e}')
                ldose = total_integral * Constants().units['MBqh'] * \
                    self.Isotope['energy'] / amass  # output is in Gy
                table.SetValue(i, 4, f'{ldose:.3e}')
                table.SetValue(i, 5, f'{error * ldose / total_integral:.3e}')
                table.SetValue(i, 6, fit_function)
                self.logger.info(
                    f"AD to {header_cells_doserate[i]} = ({ldose:.3e} +/- {error * ldose / total_integral:.3e}) Gy")

        xname = "time (h)"
        columnNames = [xname] + header_cells_mass + \
            header_cells_doserate + header_cells_doserate_fit

        slicer.util.updateTableFromArray(
            tableNode, self.getTstack(self.data[mode][lalgo]), columnNames)

        resultsTableNode.Modified()
        # Show the Table
        showTable(resultsTableNode)

        # Refresh Results folder view
        RefreshFolderView(resultsTableNodeID)
