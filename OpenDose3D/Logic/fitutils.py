from abc import abstractmethod
import numpy as np
from numpy import log
from scipy.integrate import quad

try:
    import lmfit
    LMFIT_WORKING = True
except:
    from scipy.optimize.minpack import curve_fit
    LMFIT_WORKING = False
    print('lmfit package not properly installed, using scipy curve_fit...')

from Logic.errors import ConventionError

if LMFIT_WORKING:
    # Methods not listed here are not working in our user-case, for a complete list see lmfit.minimizer.minimize function
    MINIMIZER_METHODS = {
        'leastsq': 'Levenberg-Marquardt (default)', 'least_squares': 'Least-Squares minimization trust-region',
        'differential_evolution': 'differential evolution', 'brute': 'brute force',
        'basinhopping': 'basinhopping',
        **lmfit.minimizer.SCALAR_METHODS
    }
else:
    MINIMIZER_METHODS = {'leastsq': 'Levenberg-Marquardt (default)'}

MINIMIZER_METHODS_INV = dict([(v, k) for k, v in MINIMIZER_METHODS.items()])


class FitFunction:

    def __init__(self, x, y, method='leastsq'):
        self.x = x
        self.y = y
        self.height = y.max() - y.min()
        self.width = x.max() - x.min()
        if method not in MINIMIZER_METHODS:
            raise ConventionError('method not valid')

        self.method = method
        self.ln2 = log(2)

        if LMFIT_WORKING:
            self.parameters = self.initParameters()
            self.fitter = lmfit.Minimizer(
                self.residuals, self.parameters, fcn_args=(self.x,), fcn_kws={'data': self.y})
        else:
            self.parameters = []
            self.initParametersNoLMFit()

        self.res = None
        self.report = ''

    def residuals(self, parameters, x, data=None):
        if LMFIT_WORKING:
            model = self.model(parameters, x, data)
        else:
            p = tuple(parameters)
            model = self.function(x, *p)

        if data is None:
            return model
        return model - data

    @abstractmethod
    def initParameters(self):
        pass

    @abstractmethod
    def initParametersNoLMFit(self):
        pass

    @abstractmethod
    def model(self, parameters, x, data=None):
        # model compatible with leastsq
        pass

    @abstractmethod
    def dmodel(self, parameters, x, data=None):
        # jacobian compatible with leastsq
        pass

    @abstractmethod
    def function(self, x, *args):
        # function definition y = f(x)
        pass

    @abstractmethod
    def jacobian(self, x, *args):
        # jacobian definition
        pass

    def getParametersDict(self):
        return self.parameters.valuesdict()

    def getParametersTuple(self):
        if LMFIT_WORKING:
            return tuple(self.getParametersDict().values())
        else:
            return tuple(self.parameters)

    def minimize(self, useJacobian=False):
        if LMFIT_WORKING:
            if useJacobian:
                if self.method in ['leastsq']:
                    kws = {"Dfun": self.dmodel, "col_deriv": 1}
                elif self.method in [*tuple(lmfit.minimizer.SCALAR_METHODS.keys())]:
                    kws = {"jac": self.jacobian}
                else:
                    kws = {}
            else:
                kws = {}
            self.res = self.fitter.minimize(method=self.method, **kws)
            self.parameters = self.res.params
            self.report = lmfit.fit_report(self.res)
        else:
            self.parameters, _ = curve_fit(
                self.function, self.x, self.y, self.parameters)
            self.report = ''

    def evaluate(self, newx=None):
        if newx is None:
            lnewx = np.array(self.x)
        else:
            lnewx = np.array(newx)

        return np.array(self.residuals(self.parameters, lnewx))

    def integrate(self, loBound, hiBound):
        return quad(lambda x: self.residuals(self.parameters, x), loBound, hiBound)


class MonoExponential(FitFunction):
    def function(self, x, a, b):  # "mono exponential function"
        return a*np.exp(-self.ln2*x/b)

    def jacobian(self, x, a, b):  # "mono exponential function jacobian"
        v = np.exp(-self.ln2*x/b)
        return np.array([v, a*v*(self.ln2*x/b**2)])

    def model(self, parameters, x, data=None):
        a, b = parameters['a'], parameters['b']
        return self.function(x, a, b)

    def dmodel(self, parameters, x, data=None):
        a, b = parameters['a'], parameters['b']
        return self.jacobian(x, a, b)

    def initParameters(self):
        parameters = lmfit.Parameters()
        parameters.add('a', value=self.height, min=0, max=2*self.height)
        parameters.add('b', value=self.width/5, min=0, max=self.width)
        return parameters

    def minimize(self):
        return super().minimize(useJacobian=True)

    def initParametersNoLMFit(self):
        self.parameters = [self.height, self.width/5]

    def __repr__(self):
        p = self.getParametersTuple()
        return f'mexp: y = {p[0]:.3e} * np.exp(-ln2*x / {p[1]:.3e})'


class BiExponential(FitFunction):
    def function(self, x, a, b, c, d):  # "bi exponential function"
        return a*np.exp(-self.ln2*x/b)+c*np.exp(-self.ln2*x/d)

    def jacobian(self, x, a, b, c, d):  # "bi exponential function jacobian"
        v1, v2 = np.exp(-self.ln2*x/b), np.exp(-self.ln2*x/d)
        return np.array([v1, a*v1*(self.ln2*x/b**2), v2, c*v2*(self.ln2*x/d**2)])

    def model(self, parameters, x, data=None):
        a, b, c, d = parameters['a'], parameters['b'], parameters['c'], parameters['d']
        return self.function(x, a, b, c, d)

    def dmodel(self, parameters, x, data=None):
        a, b, c, d = parameters['a'], parameters['b'], parameters['c'], parameters['d']
        return self.jacobian(x, a, b, c, d)

    def initParameters(self):
        parameters = lmfit.Parameters()
        parameters.add('a', value=self.height, min=0, max=2*self.height)
        parameters.add('b', value=self.width/5, min=0, max=self.width)
        parameters.add('c', value=-self.height, min=-
                       2*self.height, max=2*self.height)
        parameters.add('d', value=self.width/5, min=0, max=self.width)
        return parameters

    def minimize(self):
        return super().minimize(useJacobian=True)

    def initParametersNoLMFit(self):
        self.parameters = [self.height, self.width /
                           5, -self.height, self.width/5]

    def __repr__(self):
        p = self.getParametersTuple()
        return f'bexp: y = {p[0]:.3e} * exp(-ln2*x / {p[1]:.3e}) + {p[2]:.3e} * exp(-ln2*x / {p[3]:.3e})'


class TriExponential(FitFunction):
    def function(self, x, a, b, c, d, e, f):  # "tri exponential function"
        return a*np.exp(-self.ln2*x/b)+c*np.exp(-self.ln2*x/d)+e*np.exp(-self.ln2*x/f)

    def jacobian(self, x, a, b, c, d, e, f):  # "tri exponential function jacobian"
        v1, v2, v3 = np.exp(-self.ln2*x/b), np.exp(-self.ln2 *
                                                   x/d), np.exp(-self.ln2*x/f)
        return np.array([v1, a*v1*(self.ln2*x/b**2), v2, c*v2*(self.ln2*x/d**2), v3, e*v3*(self.ln2*x/f**2)])

    def model(self, parameters, x, data=None):
        a, b, c, d, e, f = parameters['a'], parameters['b'], parameters[
            'c'], parameters['d'], parameters['e'], parameters['f']
        return self.function(x, a, b, c, d, e, f)

    def dmodel(self, parameters, x, data=None):
        a, b, c, d, e, f = parameters['a'], parameters['b'], parameters[
            'c'], parameters['d'], parameters['e'], parameters['f']
        return self.jacobian(x, a, b, c, d, e, f)

    def initParameters(self):
        parameters = lmfit.Parameters()
        parameters.add('a', value=self.height, min=0, max=2*self.height)
        parameters.add('b', value=self.width/5, min=0, max=self.width)
        parameters.add('c', value=-self.height, min=-
                       2*self.height, max=2*self.height)
        parameters.add('d', value=self.width/5, min=0, max=self.width)
        parameters.add('e', value=self.height/10, min=0, max=2*self.height)
        parameters.add('f', value=self.width, min=0, max=self.width)
        return parameters

    def minimize(self):
        return super().minimize(useJacobian=True)

    def initParametersNoLMFit(self):
        self.parameters = [self.height, self.width/5, -
                           self.height, self.width/5, self.height/10, self.width]

    def __repr__(self):
        p = self.getParametersTuple()
        return f'texp: y = {p[0]:.3e} * exp(-ln2*x / {p[1]:.3e}) + {p[2]:.3e} * exp(-ln2*x / {p[3]:.3e}) + {p[4]:.3e} * exp(-ln2*x / {p[5]:.3e})'


class XExponential(FitFunction):
    def function(self, x, a, b, c, d):  # "y=a*x^b*exp(-ln2*x^d/c)"
        return a*np.power(x, b)*np.exp(-self.ln2*np.power(x, d)/c)

    def jacobian(self, x, a, b, c, d):  # "x exponential function jacobian"
        v1, v2 = np.power(x, b), np.exp(-self.ln2*np.power(x, d)/c)
        lnx = np.where(x > 0, np.log(x), 0)  # lim(d/db(x^b))(x->0)=0
        return np.array([v1*v2, a*v1*v2*lnx, a*v1*v2*(self.ln2*np.power(x, d)/c**2), a*v1*v2*(-self.ln2*np.power(x, d)*lnx/c)])

    def model(self, parameters, x, data=None):
        a, b, c, d = parameters['a'], parameters['b'], parameters['c'], parameters['d']
        return self.function(x, a, b, c, d)

    def dmodel(self, parameters, x, data=None):
        a, b, c, d = parameters['a'], parameters['b'], parameters['c'], parameters['d']
        return self.jacobian(x, a, b, c, d)

    def initParameters(self):
        parameters = lmfit.Parameters()
        parameters.add('a', value=self.height, min=0, max=2*self.height)
        parameters.add('b', value=0.5, min=0, max=5)
        parameters.add('c', value=self.width/5, min=0, max=self.width)
        parameters.add('d', value=1.0, min=0, max=5)
        return parameters

    def minimize(self):
        return super().minimize(useJacobian=True)

    def initParametersNoLMFit(self):
        self.parameters = [self.height, 0.5, self.width/5, 1.0]

    def __repr__(self):
        p = self.getParametersTuple()
        return f'xexp: y = {p[0]:.3e} * x^{p[1]:.3e} * exp(-ln2 * x^{p[3]:.3e} / {p[2]:.3e})'


class RecoveryFunction(FitFunction):
    def function(self, x, a, b, c):  # "y=a - a / (1 + (x / b) ^ c"
        return a * (1 - (1 / (1 + np.power(x / b, c))))

    def jacobian(self, x, a, b, c):  # "recovery function jacobian"
        v1 = np.power(x / b, c)
        v2 = (1+v1)**2
        return np.array([1 - (1 / (1 + v1)), -(a * c / b) * v1 / v2, a * v1 * np.log(x / b) / v2])

    def model(self, parameters, x, data=None):
        a, b, c = parameters['a'], parameters['b'], parameters['c']
        return self.function(x, a, b, c)

    def dmodel(self, parameters, x, data=None):
        a, b, c = parameters['a'], parameters['b'], parameters['c']
        return self.jacobian(x, a, b, c)

    def initParameters(self):
        parameters = lmfit.Parameters()
        parameters.add('a', value=self.height, min=0, max=2*self.height)
        parameters.add('b', value=0.5, min=0, max=5)
        parameters.add('c', value=1, min=0, max=self.width)
        return parameters

    def minimize(self):
        return super().minimize(useJacobian=True)

    def initParametersNoLMFit(self):
        self.parameters = [self.height, 0.5, self.width/5, 1.0]

    def __repr__(self):
        p = self.getParametersTuple()
        return f'recovery: y = {p[0]:.3e} - {p[0]:.3e} / (1 + (x / {p[1]:.3e})^{p[2]:.3e})'
