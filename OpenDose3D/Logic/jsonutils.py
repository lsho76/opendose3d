import slicer
from Logic.logging import getLogger, INFO
from slicer.ScriptedLoadableModule import ScriptedLoadableModuleTest
import zlib
import gzip
import json, base64

ZIPJSON_KEY = 'base64(zip(o))'
GZIPJSON_KEY = 'base64(gzip(o))'

def json_zip(j, method='zip'):
    if method=='zip':
        j = {
            ZIPJSON_KEY: base64.b64encode(
                zlib.compress(
                    json.dumps(j).encode('utf-8')
                )
            ).decode('ascii')
        }
    elif method=='gzip':
        j = {
            GZIPJSON_KEY: base64.b64encode(
                gzip.compress(
                    json.dumps(j).encode('utf-8')
                )
            ).decode('ascii')
        }
    else:
        raise RuntimeError("Wrong or unsupported compression method, select between zip and gzip")

    return j


def json_unzip(j, insist=True):
    if not isinstance(j, dict):
        if insist:
            raise RuntimeError(f"JSON not in the expected format: \n {ZIPJSON_KEY}: zipstring \n {GZIPJSON_KEY}: gzipstring")
        else:
            return j
    if ZIPJSON_KEY in j:
        try:
            j = zlib.decompress(base64.b64decode(j[ZIPJSON_KEY]))
        except Exception as e:
            raise RuntimeError(f"Could not decode/unzip the contents \n {e}")
    elif GZIPJSON_KEY in j:
        try:
            j = gzip.decompress(base64.b64decode(j[GZIPJSON_KEY]))
        except Exception as e:
            raise RuntimeError(f"Could not decode/unzip the contents \n {e}")
    else:
        if insist:
            raise RuntimeError(f"JSON not in the expected format: \n {ZIPJSON_KEY}: zipstring \n {GZIPJSON_KEY}: gzipstring")
        else:
            return j

    try:
        j = json.loads(j)
    except Exception as e:
        raise RuntimeError(f"Could not interpret the unzipped contents \n {e}")

    return j


class jsonutilsTest(ScriptedLoadableModuleTest):

    def setUp(self):
        self.tearDown()

        # Unzipped
        self.unzipped = {'a': "A", 'b': "B"}

        # Zipped
        self.zipped = {ZIPJSON_KEY: "eJyrVkpUslJQclTSUVBKArGclGoBLeoETw=="}

        # List of items
        self.items = [123, "123", self.unzipped]

        # logger
        self.logger = getLogger('OpenDose3D.jsonutilsTest')
        self.logger.setLevel(INFO)

    def tearDown(self):
        slicer.mrmlScene.Clear(0)
        slicer.app.processEvents()

    def runTest(self):
        self.setUp()
        self.logger.info('\n\n***** jsonutils Module Testing *****\n')
        self.test_json_zip()
        self.test_json_zipunzip()
        self.test_json_zipunzip_chinese()
        self.test_json_unzip()
        self.test_json_unzip_insist_failure()
        self.test_json_unzip_noinsist_justified()
        self.test_json_unzip_noinsist_unjustified()
        self.tearDown()
        self.logger.info('\n******* jsonutils tests passed **********\n')

    def test_json_zip(self):
        self.assertEqual(self.zipped, json_zip(self.unzipped))

    def test_json_unzip(self):
        self.assertEqual(self.unzipped, json_unzip(self.zipped))

    def test_json_zipunzip(self):
        for item in self.items:
            self.assertEqual(item, json_unzip(json_zip(item)))

    def test_json_zipunzip_chinese(self):
        item = {'hello': "你好"}
        self.assertEqual(item, json_unzip(json_zip(item)))

    def test_json_unzip_insist_failure(self):
        for item in self.items:
            with self.assertRaises(RuntimeError):
                json_unzip(item, insist=True)

    def test_json_unzip_noinsist_justified(self):
        for item in self.items:
            self.assertEqual(item, json_unzip(item, insist=False))

    def test_json_unzip_noinsist_unjustified(self):
        self.assertEqual(self.unzipped, json_unzip(self.zipped, insist=False))
