# Special packages
import numpy as np
from numpy import trapz, log

# Internal packages
from Logic.logging import getLogger
from Logic.utils import isNumber
from Logic.errors import ConventionError
from Logic.fitutils import MonoExponential, BiExponential, TriExponential, XExponential, MINIMIZER_METHODS, LMFIT_WORKING

FIT_FUNCTIONS = {
    'mono-exponential': MonoExponential,
    'bi-exponential': BiExponential,
    'tri-exponential': TriExponential,
    'x-exponential': XExponential
}


class FitValues:
    def __init__(self, x, y, fit_type, method='leastsq'):
        self.x, self.y = np.array([(lx, ly) for lx, ly in zip(
            x, y) if isNumber(lx) and isNumber(ly)]).T
        self.fit_type = fit_type
        self.logger = getLogger('OpenDose3D.fitting')
        self.ln2 = log(2)

        self.fitter = None
        if method not in MINIMIZER_METHODS:
            raise ConventionError('method not valid')

        self.method = method

    def fit(self):
        "fits y values according to x with lower Bayesian Information Criterion (BIC) function"
        if 'auto' in self.fit_type:
            minBIC = 1000
            for d, f in FIT_FUNCTIONS.items():
                fitter = f(self.x, self.y, self.method)
                try:
                    fitter.minimize()
                except Exception:
                    continue

                if not LMFIT_WORKING:
                    newY = fitter.evaluate()
                    residuals = newY - self.y
                    chiSquare = (residuals**2).sum()
                    numberOfDataPoints = len(residuals)
                    numberOfVariables = len(fitter.getParametersTuple())
                    _neg2_log_likel = numberOfDataPoints * \
                        np.log(chiSquare / numberOfDataPoints)
                    BIC = _neg2_log_likel + \
                        np.log(numberOfDataPoints) * numberOfVariables
                else:
                    BIC = fitter.res.bic

                if (BIC < minBIC):
                    minBIC = BIC
                    self.fitter = fitter
        else:
            f = FIT_FUNCTIONS[self.fit_type]
            self.logger.debug(
                f'Performing {self.fit_type} fit with {self.method} Method')
            self.fitter = f(self.x, self.y, self.method)
            self.fitter.minimize()

        if self.fitter:
            self.logger.debug(self.fitter.report)

    def __str__(self):
        return str(self.fitter)

    def integrate(self, T_h):
        lamda = self.ln2/T_h
        try:
            if len(self.x)==1:
                dataIntegral = 0
                dataIntegralError = 0
                totalIntegral = self.y[0]/lamda
                newY = self.y
                fitString = 'Single point'
            elif 'trapezoid' in self.fit_type:
                dataIntegral = trapz(y=self.y, x=self.x)
                dataIntegralError = 0
                totalIntegral = dataIntegral + self.y[-2]/lamda
                newY = self.y
                fitString = 'Trapezoid integration'
            else:
                self.fit()
                newY = self.fitter.evaluate()
                fitString = f'{self.fitter}'

                data = self.fitter.integrate(self.x[0], self.x[-1])
                tail = self.fitter.integrate(self.x[-1], np.inf)[0]

                dataIntegral = data[0]
                dataIntegralError = data[1]
                totalIntegral = dataIntegral + tail

        except Exception as e:
            self.logger.error(e)
            self.logger.info(self.x)
            self.logger.info(self.y)
            fitString = "The fitting does not converge"
            dataIntegral = 0
            dataIntegralError = 0
            totalIntegral = 0
            newY = self.y

        return dataIntegral, dataIntegralError, totalIntegral, newY, lamda, fitString
