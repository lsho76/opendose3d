from datetime import datetime, timedelta
from pathlib import Path
import re
import numpy as np
import pydicom
import slicer
import vtk

from Logic.attributes import Attributes
from Logic.errors import IOError, ConventionError, DICOMError
from Logic.logging import getLogger
from Logic.vtkmrmlutils import *
from Logic.dbutils import getIsotopes
from Logic.constants import Constants
from Logic.utils import extractMode
from Logic.testutils import DicomValues


class Node:
    # Node is the base class for all kinds of nodes

    def __init__(self, nodeID):
        self.logger = getLogger("OpenDose3D.nodes")

        self.nodeID = nodeID
        self.name = getItemDataNodeName(self.nodeID)
        self.data = getItemDataNode(self.nodeID)

    @staticmethod
    def new(nodeID, default_isotope='', default_activity=-1):
        node = Node(nodeID)

        if node is None:
            raise ConventionError(
                f"Invalid data format for node with id '{node.nodeID}'")

        if node.isArtefact():
            node = NodeArtefact(nodeID)
        else:
            node = NodeMRB(nodeID)
            isotope = node.getIsotope()
            if not isotope or (default_isotope and default_isotope!=isotope): 
                node.setIsotope(default_isotope)
            activity= node.getInjectedActivity()
            if not activity or activity < 0 or (default_activity>0 and activity!=default_activity):
                node.setInjectedActivity(default_activity)

        if not node.complete():
            if node.isDICOM():
                node = NodeDICOM(nodeID, default_isotope, default_activity)
            else:
                raise ConventionError(
                    f"Invalid data format for node with id '{node.nodeID}'")

        return node

    @staticmethod
    def getFolderChildren(folderID):
        """
        Gets all children of a specific folder and creates a map of their attributes

        Form:
            { "identifier": Node }

        see nodes.py

        """
        itemIDs = getAllFolderChildren(folderID)
        logger = getLogger("OpenDose3D.nodes")
        children = {}
        for itemID in itemIDs:
            node = Node.new(itemID, "", -1)
            if (not ":" in node.name) or (len(node.name.split(":")[1]) < 8):
                logger.error(f"Invalid name: {node.name}")
                continue
            key = node.name.split(":")[1][:4]
            if key == "ADRM" or key == "TABL":
                mode = extractMode(node.name)
                if not key in children:
                    children[key] = {}
                children[key][mode] = node
            else:
                children[key] = node
        return children

    @staticmethod
    def getInitialTime(defaultInjectedActivity=-1, defaultInjectionTime=0):
        logger = getLogger("OpenDose3D.nodes")
        volumeNodeIDs = getScalarVolumeNodes()
        hourunit = Constants().units['hour']
        # search for initial acquisition time
        timestamp = datetime.now()
        hours0 = 0
        injectedActivity = float(defaultInjectedActivity)
        injectionTime = defaultInjectionTime
        for volumeNodeID in volumeNodeIDs:
            node = Node.new(volumeNodeID)
            if (node.getModality() in ['NM', 'PT']):
                localInjectedActivity = float(node.getInjectedActivity())
                if not localInjectedActivity:
                    localInjectedActivity = float(defaultInjectedActivity)
                if localInjectedActivity > 0:
                    injectedActivity = localInjectedActivity

                acquisitionTimeStr = node.getAcquisition()
                if acquisitionTimeStr:
                    acquisitionTime = datetime.strptime(
                        acquisitionTimeStr, Constants().timeFormat)
                    difference = acquisitionTime - timestamp
                    seconds = difference.total_seconds()
                    if seconds < 0:
                        timestamp = acquisitionTime

                        # Search Injection DateTime first
                        try:
                            injectionTimeStr = node.getInjectionTime()
                            if not injectionTimeStr:
                                raise IOError(
                                    "no injection time in header")
                            else:
                                injectionTime = datetime.strptime(
                                    injectionTimeStr, Constants().timeFormat)
                                hour0 = int(injectionTime.strftime('%H%M%S'))
                                if hour0 <= 0:  # Invalid dicom tag set in header
                                    raise IOError(
                                        f"wrong injection time in header {injectionTime}")
                                difference = timestamp - injectionTime
                                hours0 = round(
                                    difference.total_seconds() / hourunit)
                                injectionTime = injectionTime.strftime(Constants().timeFormat)
                        except Exception as e2:  # No Injection Date Time, try to guess from name
                            logger.info(
                                f"Failed to get injection Date Time\n{e2}")
                            try:
                                _, hours0 = node.extractTime()
                                injectionTime = acquisitionTime - timedelta(hours=hours0)
                                injectionTime = injectionTime.strftime(Constants().timeFormat)
                                setItemAttribute(node.nodeID, Attributes().injectionTime, injectionTime)
                            except Exception as e3:
                                logger.info(f"Failed to get injection Time from name, trying user input \n{e3}")
                                try:
                                    injectionTime = datetime.strptime(defaultInjectionTime, Constants().timeFormat)
                                    injectionTime = injectionTime.strftime(Constants().timeFormat)
                                except:
                                    raise ConventionError(f"Impossible to extract injection Time {defaultInjectionTime}-{injectionTime}. \n Please insert a valid injection Time.")
        getLogger("OpenDose3D.nodes").info(
            f"initial time is {timestamp} after {hours0} hours from injection")
        return timestamp, hours0, injectedActivity, injectionTime

    def isArtefact(self):
        artefactNames = ["tabl", "chrt", "plot", "segm"]
        artefact = False
        for name in artefactNames:
            if name in self.name.lower():
                artefact = True

        return artefact

    def isDICOM(self):
        try:
            instanceUIDs = self.data.GetAttribute("DICOM.instanceUIDs").split()
            filename = Path(
                slicer.dicomDatabase.fileForInstance(instanceUIDs[0]))
        except:
            filename = Path("")

        # check if file is in database
        if filename.exists() and filename.is_file():
            return True
        else:
            return False

    def getArrayData(self):
        try:
            return slicer.util.arrayFromVolume(self.data).astype(float)
        except:
            try:
                return slicer.util.array(self.name)
            except:
                print(f"{self.name} has no voxel data")
                raise IOError(f"{self.name} has no voxel data")

    def setArrayData(self, Data):
        slicer.util.updateVolumeFromArray(self.data, Data)
        self.setVisualization()
        self.data.Modified()

    def rescale(self, intercept=0, slope=1):
        if intercept == 0 and slope == 1:
            return
        vox_arr = slicer.util.arrayFromVolume(self.data).astype(float)
        vox_arr = intercept + slope * vox_arr
        slicer.util.updateVolumeFromArray(self.data, vox_arr)

    def getTransformNode(self):
        return self.data.GetParentTransformNode()

    def setTransformNode(self, transformNode):
        self.data.SetAndObserveTransformNodeID(transformNode.GetID())

    def removeTransformNode(self):
        self.data.SetAndObserveTransformNodeID(None)

    def setVisualization(self):
        self.data.CreateDefaultDisplayNodes()
        setAndObserveColorNode(self.nodeID)

    def reorient(self, orientation='LPS'):
        reorient(self.data, orientation=orientation)

    def getOrigin(self):
        return np.array(list(self.data.GetOrigin()))

    def setOrigin(self, neworigin):
        self.data.SetOrigin(tuple(list(neworigin)))

    def getSpacing(self):
        return np.array(self.data.GetSpacing())

    def getDimensions(self):
        return self.data.GetImageData().GetDimensions()

    def getIJKtoRASMatrix(self):
        ijkToRas = vtk.vtkMatrix4x4()
        self.data.GetIJKToRASMatrix(ijkToRas)
        return ijkToRas

    def setIJKtoRASMatrix(self, ijkToRas):
        self.data.SetIJKToRASMatrix(ijkToRas)

    def setSpacing(self, newspacing):
        changeNodeSpacing(self.data, newspacing)

    def setNameParent(self, newName, parentID):
        self.name = newName
        setItemName(self.nodeID, newName)
        setNodeFolder(self.data, parentID)

    def compareAttributes(self, reference):
        """
        Compares the attributes of a reference node to attributes of a given node
        """
        if (self.getPatientID() == reference.getPatientID()) and (self.getStudyInstanceUID() == reference.getStudyInstanceUID()):
            self.logger.info(
                f"Sanity check on node with id '{self.nodeID}' and reference node with id '{reference.nodeID}' passed")
            return True
        else:
            self.logger.error(
                f"Sanity check on node with id '{self.nodeID}' and reference node with id '{reference.nodeID}' failed")
            return False


# NodeArtefact is used for all nodes which do not have dicom data associated
class NodeArtefact(Node):

    def __init__(self, nodeID):
        super().__init__(nodeID)

    def complete(self):
        return True


# NodeMRB is used for previously exported nodes which are expected to have
# dicom attributes stored in their itemDataNodes
class NodeMRB(Node):

    def __init__(self, nodeID):
        super().__init__(nodeID)

    def complete(self):
        """
        Check if all necessary attributes are set, as it should be if it is MRB
        """
        try:
            modality = self.getModality()
            if not modality:
                return False
        except:
            return False

        if modality in ['PT', 'NM', 'CT']:
            attributes = []
            attributes.append(self.getInstanceUIDs())
            attributes.append(self.getStudyInstanceUID())
            attributes.append(self.getSeriesInstanceUID())
            attributes.append(self.getPatientID())
            attributes.append(self.getPatientName())
            attributes.append(self.getStudyCreation())
            attributes.append(self.getAcquisition())
            if not modality == 'CT':
                attributes.append(self.getTimeStamp())
                attributes.append(self.getAcquisitionDuration())

            for attribute in attributes:
                if attribute is None or attribute == 'None' or not attribute:
                    return False

        return True

    def getInstanceUIDs(self):
        instanceUIDs = getItemDataNodeAttributeValue(
            self.nodeID, Attributes().instanceUIDs)
        return instanceUIDs

    def getStudyInstanceUID(self):
        studyInstanceUID = getItemDataNodeAttributeValue(
            self.nodeID, Attributes().studyInstanceUID)
        return studyInstanceUID

    def getSeriesInstanceUID(self):
        seriesInstanceUID = getItemDataNodeAttributeValue(
            self.nodeID, Attributes().seriesInstanceUID)
        return seriesInstanceUID

    def getPatientID(self):
        patientID = getItemDataNodeAttributeValue(
            self.nodeID, Attributes().patientID)
        return patientID

    def getPatientName(self):
        patientName = getItemDataNodeAttributeValue(
            self.nodeID, Attributes().patientName)
        return patientName

    def getStudyCreation(self):
        studyCreation = getItemDataNodeAttributeValue(
            self.nodeID, Attributes().studyCreation)
        return studyCreation

    def getModality(self):
        modality = getItemDataNodeAttributeValue(
            self.nodeID, Attributes().modality)
        if not modality:
            modality = self.extractModalityFromName()
        return modality

    def getAcquisition(self):
        acquisition = getItemDataNodeAttributeValue(
            self.nodeID, Attributes().acquisition)
        return acquisition

    def getInjectionTime(self):
        injectionTime = getItemDataNodeAttributeValue(
            self.nodeID, Attributes().injectionTime)
        return injectionTime

    def getRadiopharmaceutical(self):
        Radiopharmaceutical = getItemDataNodeAttributeValue(
            self.nodeID, Attributes().radiopharmaceutical)
        if Radiopharmaceutical == 'None':
            Radiopharmaceutical = ''
        return Radiopharmaceutical

    def getIsotope(self):
        Isotope = getItemDataNodeAttributeValue(
            self.nodeID, Attributes().isotope)
        if Isotope == 'None':
            Isotope = ''
        return Isotope

    def setRadiopharmaceutical(self, radiopharmaceutical):
        setItemDataNodeAttribute(
            self.nodeID, Attributes().radiopharmaceutical, radiopharmaceutical)

    def setIsotope(self, isotope):
        setItemDataNodeAttribute(
            self.nodeID, Attributes().isotope, isotope)

    def getInjectedActivity(self):
        InjectedActivity = getItemDataNodeAttributeValue(
            self.nodeID, Attributes().injectedActivity)
        try:
            return float(InjectedActivity)
        except:
            return -1

    def setInjectedActivity(self, activity):
        setItemDataNodeAttribute(
            self.nodeID, Attributes().injectedActivity, str(activity))

    def getAcquisitionDuration(self):
        acquisitionDuration = getItemDataNodeAttributeValue(
            self.nodeID, Attributes().acquisitionDuration)
        return acquisitionDuration

    def getCalibration(self):
        calibration = getItemDataNodeAttributeValue(
            self.nodeID, Attributes().calibrationCT)
        return calibration

    def getCalibrationDuration(self):
        calibrationDuration = getItemDataNodeAttributeValue(
            self.nodeID, Attributes().calibrationDuration)
        return calibrationDuration

    def getSensitivity(self):
        sensitivity = getItemDataNodeAttributeValue(
            self.nodeID, Attributes().sensitivity)
        return sensitivity

    def getSensitivityUnits(self):
        sensitivityUnits = getItemDataNodeAttributeValue(
            self.nodeID, Attributes().sensitivityUnits)
        return sensitivityUnits

    def getTimeStamp(self):
        timeStamp = getItemDataNodeAttributeValue(
            self.nodeID, Attributes().timeStamp)
        if not timeStamp: # Create timeStamp from current time and injection time
            injectionTime = self.getInjectionTime()  # This must be present in DICOM header
            if not injectionTime:  # Try to use timeStamp from name
                try:
                    _, timeStamp = self.extractTime()
                    acquisition = datetime.strptime(
                        self.getAcquisition(), Constants().timeFormat)
                    tdelta = timedelta(hours=float(timeStamp))
                    injection = acquisition - tdelta
                    injectionTime = injection.strftime(Constants().timeFormat)
                except:
                    self.logger.error(f"No injection time and no timestamp in name for node {self.name}")
                    return 0
            
            self.setTimeStamp(injectionTime)
            timeStamp = getItemDataNodeAttributeValue(
                self.nodeID, Attributes().timeStamp)
        return timeStamp

    def setTimeStamp(self, injectionTime):
        hours = 0
        acquisitionTimeStr = self.getAcquisition()  # This must be present
        if acquisitionTimeStr:
            setItemDataNodeAttribute(
                self.nodeID, Attributes().injectionTime, injectionTime)
            acquisitionTime = datetime.strptime(
                acquisitionTimeStr, Constants().timeFormat)
            injectionTime = datetime.strptime(
                injectionTime, Constants().timeFormat)
            difference = acquisitionTime - injectionTime
            hours = (difference.total_seconds() / Constants().units['hour'])
            setItemDataNodeAttribute(
                self.nodeID, Attributes().timeStamp, hours)
        else:
            self.logger.warning(f"No acquisition time for node {self.name}")

    def rename(self, hours=-1):
        # intermediate calculation data, created by us, so no need to check
        if "tabl" in self.name.lower() or "chrt" in self.name.lower() or "plot" in self.name.lower() or  \
            "ders" in self.name.lower() or "dens" in self.name.lower() or "trnf" in self.name.lower() or \
            "actm" in self.name.lower() or "ctrs" in self.name.lower() or "segm" in self.name.lower() \
                or ("adrm" in self.name.lower() and "monte" not in self.name.lower()):
            return

        if hours < 0:
            days, hours = self.extractTime()
        else:
            days = int(hours/24)

        modality = self.getModality()
        if modality in ['NM', 'PT']:
            newName = f"J{days}:ACSC SPECT {hours}HR"
        elif modality in ['CT']:
            newName = f"J{days}:CTCT CT {hours}HR"
        # necessary for MonteCarlo manual input
        elif "monte" in self.name.lower():
            newName = f"J{days}:ADRM MonteCarlo {hours}HR"
        else:
            self.logger.info(
                f"Failed to rename node with name '{self.name}', since naming convention not met regarding modality. Example: 'CTCT 48h' or 'J9 ACSC'")
            raise ConventionError(
                f"Failed to rename node with name '{self.name}', since naming convention not met regarding modality. Example: 'CTCT 48h' or 'J9 ACSC'")

        # set attribute
        shNode = getSubjectHierarchyNode()
        shNode.SetItemName(self.nodeID, newName)

        # update instance attribute
        self.name = newName

    def extractTime(self):
        """
        Extracts days and hour by naming conventions of either specifying days or hours
        """
        # NOTE currently just extracts the hours, even if days would be specified as well

        # try to extract by hours convention
        days, hours = self.extractHours()

        # try to extract by days convention
        if hours is None:
            days, hours = self.extractDays()

        # if not conform to naming convention
        if days is None or hours is None:
            self.logger.info(
                f"Failed to rename node with name: {self.name}, since no convention met regarding time")
            raise ConventionError(
                "Please rename files with specifying either days or hours. Example: 'CTCT 48h' or 'J9 ACSC'")
        else:
            return int(days), int(hours)

    def extractHours(self):
        """
        Extracts hours from volume node name
        """
        # NOTE this only workes if this naming convention is abided, otherwise this will raise an error
        # NOTE conventions are: "(\d+)hr{0,1}+" and "hr{0,1}+(\d+)", e.g. "14hr" or "h14"
        p = re.compile(r"(\d+)hr{0,1}")
        times = p.findall(self.name.lower())
        expr = "(\d+)hr{0,1}|hr{0,1}+(\d+)"

        if len(times) == 0:
            p = re.compile(r"hr{0,1}(\d+)")
            times = p.findall(self.name.lower())

            if len(times) == 0:
                self.logger.error(
                    f"No time reference found in name '{self.name.lower()}' using the regex {expr}")
                return None, None

        if len(times) == 1:
            hours = times[0]
            days = int(times[0])/24
            return int(days), int(hours)

        if len(times) > 1:
            self.logger.error(
                f"Multiple time reference found in name '{self.name.lower()}' using the regex {expr}")

    def extractDays(self):
        """
        Extracts days from volume naming
        """
        # NOTE this only workes if this naming convention is abided, otherwise this will raise an error
        # NOTE conventions are: "j([0-9]+)", e.g. "j7"
        p = re.compile(r"j([0-9]+)")
        times = p.findall(self.name.lower())
        expr = 'j([0-9]+)'

        if len(times) == 0:
            self.logger.error(
                f"No time reference found in name '{self.name.lower()}' using the regex {expr}")
            return None, None

        if len(times) == 1:
            if int(times[0]) > 9:
                self.logger.error(
                    "Invalid input, name suggests that the data is over 9 days old")
                return None, None

            days = int(times[0])
            hours = int(days)*24
            return int(days), int(hours)

        if len(times) > 1:
            self.logger.error(
                f"Multiple time reference found in name '{self.name.lower()}' using the regex {expr}")

    def extractModalityFromName(self):
        if "acsc" in self.name.lower():
            modality = "NM"
        elif "pet" in self.name.lower() or "tep" in self.name.lower():
            modality = "PT"
        elif "ctct" in self.name.lower() or ": ct" in self.name.lower():
            modality = "CT"
        elif "ctrs" in self.name.lower():
            modality = "CTRS"
        elif "adrm" in self.name.lower() or "dose" in self.name.lower():
            modality = "RTDOSE"
        elif "trnf" in self.name.lower():
            modality = "REG"
        elif "segm" in self.name.lower():
            modality = "RTSTRUCT"
        elif "actm" in self.name.lower() or "dens" in self.name.lower() or "ders" in self.name.lower():
            modality = "RWV"
        else:
            self.logger.warning(
                f"Could not retrieve modality for node with id '{self.nodeID}' from name '{self.name}'")
            raise ConventionError(
                f"Could not retrieve modality for node with id '{self.nodeID}' from name '{self.name}'")

        self.logger.debug(
            f"Extracted modality '{modality}' from node with id '{self.nodeID}' and name '{self.name}'")
        return modality


# NodeDICOM is used for all nodes which do have dicom data associated
# Look up dicom attributes on http://dicomlookup.com or
# https://northstar-www.dartmouth.edu/doc/nodeIDl/html_6.2/DICOM_Attributes.html
class NodeDICOM(NodeMRB):

    def __init__(self, nodeID, default_isotope, default_activity):
        super().__init__(nodeID)
        self.dataset = self.loadDICOM()
        self.setAttributes(default_isotope, default_activity)
        self.fixNonStandardCorrection()

    def loadDICOM(self):
        """
        Loads DICOM dataset
        """
        instanceUIDs = self.data.GetAttribute("DICOM.instanceUIDs").split()
        # get first dicom header as a reference for all other slices
        filename = slicer.dicomDatabase.fileForInstance(instanceUIDs[0])
        dataset = pydicom.read_file(filename)
        return dataset

    def setAttributes(self, default_isotope, default_activity):
        """
        Sets all itemDataNodeAttributes which are readable from DICOM data
        """
        self.setModality()
        modality = self.getModality()

        # this is always set
        studyInstanceUID = self.getStudyInstanceUID()
        if not studyInstanceUID:
            self.setStudyInstanceUID()
            studyInstanceUID = self.getStudyInstanceUID()

        seriesInstanceUID = self.getSeriesInstanceUID()
        if not seriesInstanceUID:
            self.setSeriesInstanceUID()
            seriesInstanceUID = self.getSeriesInstanceUID()

        patientID = self.getPatientID()
        if not patientID:
            self.setPatientID()
            patientID = self.getPatientID()

        patientName = self.getPatientName()
        if not patientName:
            self.setPatientName()
            patientName = self.getPatientName()

        studyCreation = self.getStudyCreation()
        if not studyCreation:
            self.setStudyCreation()
            studyCreation = self.getStudyCreation()

        acquisition = self.getAcquisition()
        if not acquisition or acquisition is None:
            self.setAcquisition()
            acquisition = self.getAcquisition()

        acquisitionDuration = self.getAcquisitionDuration()
        if not acquisitionDuration:
            self.setAcquisitionDuration()
            acquisitionDuration = self.getAcquisitionDuration()

        if modality in ['PT', 'NM']:
            radiopharmaceutical = self.getIsotope()
            if not radiopharmaceutical or radiopharmaceutical == 'None' or radiopharmaceutical is None:
                try:
                    radiopharmaceutical = self.readRadiopharmaceutical()
                except Exception as e:
                    self.logger.warning(f"{e}")
                    radiopharmaceutical = default_isotope
            self.setIsotope(radiopharmaceutical)

            injectedActivity = self.getInjectedActivity()
            if not injectedActivity:
                try:
                    injectedActivity = self.readInjectionActivity()
                except Exception as e:
                    self.logger.warning(f"{e}")
                    injectedActivity = default_activity
            setItemDataNodeAttribute(
                self.nodeID, Attributes().injectedActivity, injectedActivity)

            try:
                dicomInjectionDateTime = datetime.strptime(
                    self.readInjectionDateTime(), "%Y%m%d%H%M%S")
                dicomInjectionDateTimeStr = dicomInjectionDateTime.strftime(
                    '%H%M%S')
                if int(dicomInjectionDateTimeStr) <= 0:
                    raise
                else:
                    injectionTime = dicomInjectionDateTime.strftime(Constants().timeFormat)
            except:
                try:
                    acqDate = datetime.strptime(
                        acquisition, Constants().timeFormat).strftime('%Y%m%d')
                    dicomInjectionTimeStr = self.readInjectionTime()
                    if int(dicomInjectionTimeStr) <= 0:
                        raise
                    else:
                        injectionTime = datetime.strptime(
                            f"{acqDate}{dicomInjectionTimeStr}", "%Y%m%d%H%M%S").strftime(Constants().timeFormat)
                except:
                    injectionTime = ''

            if injectionTime:
                setItemDataNodeAttribute(
                    self.nodeID, Attributes().injectionTime, injectionTime)

                try:
                    timeStamp = self.getTimeStamp()
                except:
                    timeStamp = ''
                if not timeStamp:
                    timeStamp = (datetime.strptime(acquisition, Constants().timeFormat) - datetime.strptime(
                        injectionTime, Constants().timeFormat)).total_seconds() / Constants().units['hour']
                    setItemDataNodeAttribute(self.nodeID, Attributes().timeStamp, timeStamp)

    def setStudyInstanceUID(self):
        try:
            studyInstanceUID = self.readStudyInstanceUID()
        except:
            studyInstanceUID = ''

        if not studyInstanceUID:
            self.logger.warning(
                f"Attribute '{Attributes().studyInstanceUID}' is empty")
            raise DICOMError(
                f"Attributes '{Attributes().studyInstanceUID}' is empty")

        setItemDataNodeAttribute(
            self.nodeID, Attributes().studyInstanceUID, studyInstanceUID)

    def setSeriesInstanceUID(self):
        try:
            seriesInstanceUID = self.readSeriesInstanceUID()
        except:
            seriesInstanceUID = ''

        if not seriesInstanceUID:
            self.logger.warning(
                f"Attribute '{Attributes().seriesInstanceUID}' is empty")
            raise DICOMError(
                f"Attribute '{Attributes().seriesInstanceUID}' is empty")

        setItemDataNodeAttribute(
            self.nodeID, Attributes().seriesInstanceUID, seriesInstanceUID)

    def setPatientID(self):
        try:
            patientID = self.readPatientID()
        except:
            patientID = ''

        if not patientID:
            self.logger.warning(
                f"Attribute '{Attributes().patientID}' is empty")
            raise DICOMError(
                f"Attribute '{Attributes().patientID}' is empty")

        setItemDataNodeAttribute(
            self.nodeID, Attributes().patientID, patientID)

    def setPatientName(self):
        try:
            patientName = self.readPatientName()
        except:
            patientName = self.getPatientID()

        if not patientName:
            self.logger.warning(
                f"Attribute '{Attributes().patientName}' is empty")
            raise DICOMError(
                f"Attribute '{Attributes().patientName}' is empty")

        setItemDataNodeAttribute(
            self.nodeID, Attributes().patientName, patientName)

    def setStudyCreation(self):
        try:
            studyCreation = self.readStudyCreation()
        except:
            studyCreation = ''

        if not studyCreation:
            self.logger.warning(
                f"Attribute '{Attributes().studyCreation}' is empty")
            raise DICOMError(
                f"Attribute '{Attributes().studyCreation}' is empty")

        setItemDataNodeAttribute(
            self.nodeID, Attributes().studyCreation, studyCreation)

    def setModality(self):
        try:
            modality = self.readModality()
        except:
            modality = ''

        if not modality:
            self.logger.debug(f"Attribute '{Attributes().modality}' is empty")
            try:
                modality = self.extractModalityFromName()
            except:
                raise IOError(
                    f"The name {self.name} does not contain modality information")
        setItemDataNodeAttribute(
            self.nodeID, Attributes().modality, modality)

    def setAcquisition(self):
        try:
            acquisition = self.readAcquisition()
        except:
            acquisition = ''

        if not acquisition:
            self.logger.warning(
                f"Attribute '{Attributes().acquisition}' is empty")
            raise DICOMError(
                f"Attribute '{Attributes().acquisition}' is empty")

        setItemDataNodeAttribute(
            self.nodeID, Attributes().acquisition, acquisition)

    def setAcquisitionDuration(self, duration=''):
        try:
            acquisitionDuration = self.readAcquisitionDuration()
        except:
            acquisitionDuration = duration

        if not acquisitionDuration:
            self.logger.warning(
                f"Attribute '{Attributes().acquisitionDuration}' is empty")

        setItemDataNodeAttribute(
            self.nodeID, Attributes().acquisitionDuration, acquisitionDuration)

    def readStudyInstanceUID(self):
        # NOTE Attribute: (0x0020, 0x000d) - StudyInstanceUID
        try:
            if [0x0020, 0x000d] in self.dataset:
                studyInstanceUID = self.dataset[0x0020, 0x000d].value
                return studyInstanceUID

        except:
            self.logger.warning(
                f"Failed to read DICOM attribute '{Attributes().studyInstanceUID}'")
            raise DICOMError(
                f"Failed to read DICOM attribute '{Attributes().studyInstanceUID}'")

    def readSeriesInstanceUID(self):
        # NOTE Attribute: (0x0020, 0x000e) - SeriesInstanceUID
        try:
            if [0x0020, 0x000e] in self.dataset:
                seriesInstanceUID = self.dataset[0x0020, 0x000e].value
                return seriesInstanceUID

        except:
            self.logger.warning(
                f"Failed to read DICOM attribute '{Attributes().seriesInstanceUID}'")
            raise DICOMError(
                f"Failed to read DICOM attribute '{Attributes().seriesInstanceUID}'")

    def readPatientName(self):
        # NOTE Attribute: (0x0010, 0x0010) - PatientName
        try:
            if [0x0010, 0x0010] in self.dataset:
                patientID = self.dataset[0x0010, 0x0010].value
                return patientID

        except:
            self.logger.warning(
                f"Failed to read DICOM attribute '{Attributes().patientName}'")
            raise DICOMError(
                f"Failed to read DICOM attribute '{Attributes().patientName}'")

    def readPatientID(self):
        # NOTE Attribute: (0x0010, 0x0020) - PatientID
        try:
            if [0x0010, 0x0020] in self.dataset:
                patientID = self.dataset[0x0010, 0x0020].value
                return patientID

        except:
            self.logger.warning(
                f"Failed to read DICOM attribute '{Attributes().patientID}'")
            raise DICOMError(
                f"Failed to read DICOM attribute '{Attributes().patientID}'")

    def readStudyCreation(self):
        # NOTE Attribute: (0x0008, 0x002) - StudyDate
        # TODO this is currently only the date, we would need to also have the time (0008, 0030)
        try:
            if [0x0008, 0x0020] in self.dataset:
                studyCreation = self.dataset[0x0008, 0x0020].value
                return studyCreation

        except:
            self.logger.warning(
                f"Failed to read DICOM attribute '{Attributes().studyCreation}'")
            raise DICOMError(
                f"Failed to read DICOM attribute '{Attributes().studyCreation}'")

    def readModality(self):
        # NOTE Attribute: (0x0008, 0x0060) - Modality
        if [0x0008, 0x0060] in self.dataset and self.dataset[0x0008, 0x0060].value:
            modality = self.dataset[0x0008, 0x0060].value
            return modality
        else:
            self.logger.warning(
                f"Failed to read DICOM attribute '{Attributes().modality}'")
            raise DICOMError(
                f"Failed to read DICOM attribute '{Attributes().modality}'")

    def readAcquisition(self):
        # NOTE Attribute: (0x0008, 0x002A) - AcquisitionDateTime
        # NOTE Attribute: (0x0008, 0x0022) - AcquisitionTime
        # NOTE Attribute: (0x0008, 0x0032) - AcquisitionDate
        try:
            if [0x0008, 0x002A] in self.dataset and self.dataset[0x0008, 0x002A].value:
                dateTime = self.dataset[0x0008, 0x002A].value
                acquisition = datetime.strptime(dateTime[:14], "%Y%m%d%H%M%S")
            else:
                date = self.dataset[0x0008, 0x0022].value
                time = self.dataset[0x0008, 0x0032].value
                acquisition = datetime.strptime(date+time[:6], "%Y%m%d%H%M%S")
            return str(acquisition)

        except:
            self.logger.warning(
                f"Failed to read DICOM attribute '{Attributes().acquisition}'")
            raise DICOMError(
                f"Failed to read DICOM attribute '{Attributes().acquisition}'")

    def readAcquisitionDuration(self):
        # NOTE this functions has to be called once the modality already has been retrieved
        # NOTE Attribute: (no such attribute) - AquisitionDuration
        # NOTE Attribute: (0x0018, 0x1242) - ActualFrameDuration
        # NOTE Attribute: (0x0054, 0x0052) - RotationInformationSequence
        # NOTE Attribute: (0x0054, 0x0053) - NumberOfFramesInRotation
        # TODO Add the Siemens case

        modality = self.getModality()
        if not modality:
            self.logger.warning(
                f"Failed to extract '{Attributes().acquisitionDuration}', missing modality")
            raise DICOMError(
                f"Failed to extract '{Attributes().acquisitionDuration}', missing modality")

        if modality in ["NM", "PT"]:
            if [0x0018, 0x1242] in self.dataset and self.dataset[0x0018, 0x1242].value:
                acquisitionFramesDuration = int(
                    self.dataset[0x0018, 0x1242].value)
            elif [0x0054, 0x0052] in self.dataset and [0x0018, 0x1242] in self.dataset[0x0054, 0x0052][0] and self.dataset[0x0054, 0x0052][0][0x0018, 0x1242].value:
                acquisitionFramesDuration = int(
                    self.dataset[0x0054, 0x0052][0][0x0018, 0x1242].value)
            else:
                self.logger.warning(
                    f"Failed to read DICOM attribute '{Attributes().acquisitionFramesDuration}'")
                raise DICOMError(
                    f"Failed to read DICOM attribute '{Attributes().acquisitionFramesDuration}'")

            if modality == "PT":
                acquisitionFramesTotal = 1
            elif [0x0054, 0x0053] in self.dataset and self.dataset[0x0054, 0x0053].value:
                acquisitionFramesTotal = int(
                    self.dataset[0x0054, 0x0053].value)
            elif [0x0054, 0x0052] in self.dataset and [0x0054, 0x0053] in self.dataset[0x0054, 0x0052][0] and self.dataset[0x0054, 0x0052][0][0x0054, 0x0053].value:
                acquisitionFramesTotal = int(
                    self.dataset[0x0054, 0x0052][0][0x0054, 0x0053].value)
            else:
                self.logger.warning(
                    f"Failed to read DICOM attribute '{Attributes().acquisitionFramesDuration}'")
                raise DICOMError(
                    f"Failed to read DICOM attribute '{Attributes().acquisitionFramesDuration}'")

            acquisitionDuration = float(
                acquisitionFramesTotal * acquisitionFramesDuration / 1000)

        elif modality == "CT":

            if [0x0018, 0x1150] in self.dataset:
                acquisitionDuration = self.dataset[0x0018, 0x1150].value
            elif [0x0043, 0x104e] in self.dataset:
                acquisitionDuration = self.dataset[0x0043, 0x104e].value
            else:
                acquisitionDuration = 0
                self.logger.warning(
                    f"Failed to read DICOM attribute '{Attributes().acquisitionFramesDuration}'")
                raise DICOMError(
                    f"Failed to read DICOM attribute '{Attributes().acquisitionFramesDuration}'")

            acquisitionDuration = int(acquisitionDuration)

        else:
            self.logger.warning(
                f"Failed to extract '{Attributes().acquisitionDuration}', invalid modality '{modality}'")
            raise DICOMError(
                f"Failed to extract '{Attributes().acquisitionDuration}', invalid modality '{modality}'")

        return str(acquisitionDuration)

    def readInjectionTime(self):
        # NOTE Attribute: Injection Time could be located in [0x0018, 0x1072] or [0x0054, 0x0016][0][0x0018, 0x1072]
        if [0x0018, 0x1072] in self.dataset and self.dataset[0x0018, 0x1072].value:
            return self.dataset[0x0018, 0x1072].value[:6]
        elif ([0x0054, 0x0016] in self.dataset) and ([0x0018, 0x1072] in self.dataset[0x0054, 0x0016][0]) and self.dataset[0x0054, 0x0016][0][0x0018, 0x1072].value:
            return self.dataset[0x0054, 0x0016][0][0x0018, 0x1072].value[:6]
        else:
            self.logger.warning(
                f"Failed to retrieve DICOM attribute: {Attributes().injectionTime}")
            raise DICOMError(
                f"Not DICOM attribute set for {Attributes().injectionTime}")

    def readInjectionDateTime(self):
        # NOTE Attribute: Injection DateTime could be located in [0x0018, 0x1078] or [0x0054, 0x0016][0][0x0018, 0x1078]
        if [0x0018, 0x1078] in self.dataset and self.dataset[0x0018, 0x1078].value:
            return self.dataset[0x0018, 0x1078].value[:14]
        elif ([0x0054, 0x0016] in self.dataset) and ([0x0018, 0x1078] in self.dataset[0x0054, 0x0016][0]) and self.dataset[0x0054, 0x0016][0][0x0018, 0x1078].value:
            return self.dataset[0x0054, 0x0016][0][0x0018, 0x1078].value[:14]
        else:
            self.logger.warning(
                f"Failed to retrieve DICOM attribute: {Attributes().injectionTime}")
            raise DICOMError(
                f"Not DICOM attribute set for {Attributes().injectionTime}")

    def readInjectionActivity(self):
        # NOTE Attribute: Injected Activity could be located in [0x0018, 0x1074] or [0x0054, 0x0016][0][0x0018, 0x1074]
        if [0x0018, 0x1074] in self.dataset and self.dataset[0x0018, 0x1074].value:
            return self.dataset[0x0018, 0x1074].value
        elif ([0x0054, 0x0016] in self.dataset) and ([0x0018, 0x1074] in self.dataset[0x0054, 0x0016][0]) and self.dataset[0x0054, 0x0016][0][0x0018, 0x1074].value:
            return self.dataset[0x0054, 0x0016][0][0x0018, 0x1074].value
        else:
            self.logger.warning(
                f"Failed to retrieve DICOM attribute: {Attributes().injectedActivity}")
            raise DICOMError(
                f"Not DICOM attribute set for {Attributes().injectedActivity}")

    def decodeElement(self, elementText):
        for element, list in Constants().elements.items():
            if elementText.lower() in list:
                return element

        return ""

    def decodeIsotope(self, isotopeText):
        "Converts whatever string referring to isotope to standard nn-AAA isotope name"
        Isotopes = getIsotopes()
        for Isotope in Isotopes:  # Search directly first
            if Isotope.lower() in isotopeText.lower():
                return Isotope
        #  We found nothing, lets decode then
        # isotope name and then AAA
        pattern = re.compile(r'([a-zA-Z]+)([\-_\s\^]{1})?(\d{1,3})')
        search = pattern.search(isotopeText)
        if search is not None:
            decrypted = search.groups()
            decrypted = [d for d in decrypted if d and d.strip()]
            correctedText = "-".join(
                [self.decodeElement(decrypted[0]), decrypted[1]])
            for Isotope in Isotopes:
                if correctedText.lower() in Isotope.lower():
                    return Isotope

        # AAA and then isotope name
        pattern = re.compile(
            r'(\d{1,3})([\-_\s\^]{1})?([a-zA-Z]+)')
        search = pattern.search(isotopeText)
        if search is not None:
            decrypted = search.groups()
            decrypted = [d for d in decrypted if d and d.strip()]
            correctedText = "-".join(
                [self.decodeElement(decrypted[1]), decrypted[0]])
            print(decrypted)
            for Isotope in Isotopes:
                if correctedText.lower() in Isotope.lower():
                    return Isotope

        return ""  # nothing was found, return empty string

    def readRadiopharmaceutical(self):
        # NOTE Attribute: Radiopharmaceutical could be located in several places
        Radiopharmaceuticals = []  # create a list of all the common places
        if 0x00180031 in self.dataset:
            Radiopharmaceutical = self.dataset[0x00180031].value
            Radiopharmaceuticals.append(Radiopharmaceutical)
        if 0x00181030 in self.dataset:
            Radiopharmaceutical = self.dataset[0x00181030].value
            Radiopharmaceuticals.append(Radiopharmaceutical)
        if 0x00181031 in self.dataset:
            Radiopharmaceutical = self.dataset[0x00181031].value
            Radiopharmaceuticals.append(Radiopharmaceutical)
        if 0x00540016 in self.dataset and 0x00180031 in self.dataset[0x00540016][0]:
            Radiopharmaceutical = self.dataset[0x00540016][0][0x00180031].value
            Radiopharmaceuticals.append(Radiopharmaceutical)
        if 0x00540016 in self.dataset and 0x00181031 in self.dataset[0x00540016][0]:
            Radiopharmaceutical = self.dataset[0x00540016][0][0x00181031].value
            Radiopharmaceuticals.append(Radiopharmaceutical)
        if 0x00540016 in self.dataset and 0x00540300 in self.dataset[0x00540016][0] and 0x00080104 in self.dataset[0x00540016][0][0x00540300][0]:
            Radiopharmaceutical = self.dataset[0x00540016][0][0x00540300][0][0x00080104].value
            Radiopharmaceuticals.append(Radiopharmaceutical)
        if 0x00540016 in self.dataset and 0x00540300 in self.dataset[0x00540016][0] and 0x00180031 in self.dataset[0x00540016][0][0x00540300][0]:
            Radiopharmaceutical = self.dataset[0x00540016][0][0x00540300][0][0x00180031].value
            Radiopharmaceuticals.append(Radiopharmaceutical)
        # Now try to decypher at least one of the found strings
        for Radiopharmaceutical in Radiopharmaceuticals:
            isotope = self.decodeIsotope(Radiopharmaceutical)
            if isotope:
                return isotope

        self.logger.warning(
            f"Failed to retrieve DICOM attribute: {Attributes().isotope}")
        raise DICOMError(
            f"Not DICOM attribute set for {Attributes().isotope}")

    def readNonStandardStationCorrection(self):
        # NOTE Attribute: (0x33, 0x1038) - Siemens factor
        # NOTE Attribute: [0x40, 0x9096][0][0x40, 0x9224] - Hermes intercept
        # NOTE Attribute: [0x40, 0x9096][0][0x40, 0x9225] - Hermes slope

        # Search Siemens data
        if [0x33, 0x1038] in self.dataset:
            factor = float(self.dataset[0x33, 0x1038].value)
        else:
            factor = 1.0

         # Check if this is a Hermes node
        if 0x00409096 in self.dataset and 0x00409224 in self.dataset[0x00409096][0] and 0x00409225 in self.dataset[0x00409096][0]:
            intercept = float(self.dataset[0x00409096][0][0x00409224].value)
            slope = float(self.dataset[0x00409096][0][0x00409225].value)
            return intercept, slope/factor
        else:  # Not Hermes node
            modality = self.getModality()
            if modality == "CT":  # avoid oversampling outside FOV
                vox_arr = slicer.util.arrayFromVolume(
                    self.data).astype(float)  # the value is not in DICOM so we must calculate it
                smallestValue = np.amin(vox_arr.ravel())

                if smallestValue >= 0:
                    intercept = -1000 - smallestValue
                    slope = 1
                    return intercept, slope/factor
                else:
                    return 0, 1/factor

            else:  # Everything else
                return 0, 1/factor

    def fixNonStandardCorrection(self):
        if not hasItemAttribute(self.nodeID, "fixed"):
            intercept, slope = self.readNonStandardStationCorrection()
            if intercept != 0 or slope != 1:
                vox_arr = slicer.util.arrayFromVolume(self.data).astype(float)
                vox_arr = intercept + slope * vox_arr
                slicer.util.updateVolumeFromArray(self.data, vox_arr)
                setItemAttribute(self.nodeID, "fixed", "1")
