import numpy as np
import vtk
import vtk.util.numpy_support as nps


def getVTKType(typ):
    """Look up the VTK type for a give python data type.
    Corrects for string type mapping issues.
    Return
    ------
        int : the integer type id specified in vtkType.h
    """
    typ = nps.getVTKArrayType(typ)
    # This handles a silly string type bug
    if typ == 3:
        return 13
    return typ


def VTKBitArraytoChar(vtkarr_bint):
    """Cast vtk bit array to a char array."""
    vtkarr = vtk.vtkCharArray()
    vtkarr.DeepCopy(vtkarr_bint)
    return vtkarr


def convertStringArray(arr, name=None):
    """Convert a numpy array of strings to a vtkStringArray or vice versa.
    Note that this is terribly inefficient - inefficient support
    is better than no support :). If you have ideas on how to make this faster,
    please consider opening a pull request.
    """
    if isinstance(arr, np.ndarray):
        vtkarr = vtk.vtkStringArray()
        for val in arr:
            vtkarr.InsertNextValue(val)
        if isinstance(name, str):
            vtkarr.SetName(name)
        return vtkarr
    # Otherwise it is a vtk array and needs to be converted back to numpy
    carr = np.empty(arr.GetNumberOfValues(), dtype='O')
    for i in range(arr.GetNumberOfValues()):
        carr[i] = arr.GetValue(i)
    return carr.astype('str')


def convertArray(arr, name=None, deep=0, array_type=None):
    """Convert a NumPy array to a vtkDataArray or vice versa.
    Parameters
    -----------
    arr : ndarray or vtkDataArry
        A numpy array or vtkDataArry to convert
    name : str
        The name of the data array for VTK
    deep : bool
        if input is numpy array then deep copy values
    Return
    ------
    vtkDataArray, ndarray, or DataFrame:
        the converted array (if input is a NumPy ndaray then returns
        ``vtkDataArray`` or is input is ``vtkDataArray`` then returns NumPy
        ``ndarray``). If pdf==True and the input is ``vtkDataArry``,
        return a pandas DataFrame.
    """
    if arr is None:
        return None
    if isinstance(arr, np.ndarray):
        if arr.dtype is np.dtype('O'):
            arr = arr.astype('str')
        arr = np.ascontiguousarray(arr)
        try:
            # This will handle numerical data
            arr = np.ascontiguousarray(arr)
            vtk_data = nps.numpy_to_vtk(
                num_array=arr, deep=deep, array_type=array_type)
        except ValueError:
            # This handles strings
            typ = getVTKType(arr.dtype)
            if typ == 13:
                vtk_data = convertStringArray(arr)
        if isinstance(name, str):
            vtk_data.SetName(name)
        return vtk_data
    # Otherwise input must be a vtkDataArray
    if not isinstance(arr, (vtk.vtkDataArray, vtk.vtkBitArray, vtk.vtkStringArray)):
        raise TypeError(f'Invalid input array type ({type(arr)}).')
    # Handle booleans
    if isinstance(arr, vtk.vtkBitArray):
        arr = VTKBitArraytoChar(arr)
    # Handle string arrays
    if isinstance(arr, vtk.vtkStringArray):
        return convertStringArray(arr)
    # Convert from vtkDataArry to NumPy
    return nps.vtk_to_numpy(arr)
