import math
import sys
import numpy as np
from scipy.interpolate import interp1d, CubicSpline
from scipy import stats
from pathlib import Path

# Units
gram = 1e-3
cm = 1e-2
mm = 1e-3
g_cm3 = gram / cm**3
elCh = 1.602177e-19
MeV = 1e6*elCh
mGy = 1e-3
MBqs = 1e6
mGy_MBqs = mGy/MBqs

# simulation data, self explained variables to be changed by user

# Copy and paste the same variables from the generator
directory = Path(expanduser('~')) / 'Documents' / \
    'DVK'  # Modify, use the same as the generator
# Modify, use the same as the generator
Isotope = {'Symbol': 'F', 'Z': 9, 'A': 18}

RadioNuclide = f"{Isotope['Symbol']}-{Isotope['A']}"
# If provided use the radionuclide
if len(list(sys.argv)) > 1:
    arguments = list(sys.argv)
    RadioNuclide = arguments[1]

# Reading data
Data = {}
Data['DPK'] = []  # DPK for the shell
Data['distances'] = []  # shell outer radius
Data['uncertainty'] = []  # uncertainty
dpkFile = directory / f'DPK-{RadioNuclide}.txt'
with dpkFile.open() as f:
    x = [l.strip() for l in f]
    for l in x[1:]:
        if (l != ''):
            entries = l.split()
            # This is the outer radius of the shell in mm
            Data['distances'].append(float(entries[1])*mm)
            # This is the Edep in MeV
            Data['DPK'].append(float(entries[2])*MeV)
            Data['uncertainty'].append(float(entries[3]))

DPK = Data['DPK']
distances = Data['distances']
last_d_d = distances[1] - distances[0]   # spacing
shell_V = []            # shell volume
Data['eff_distances'] = []      # effective radius of the shell (Janicky 2004)
distances.insert(0, 0.0)  # origin
TotEdep = 0
for i in range(len(DPK)):
    d = distances[i]
    d1 = distances[i+1]
    shell_V.append(4./3.*math.pi*(d1**3-d**3)*1*g_cm3)
    Data['eff_distances'].append(math.sqrt((1/3)*(d1**3-d**3)/(d1-d)))

# calculate DPK = Edep / shell_mass
for i in range(len(DPK)):
    TotEdep += DPK[i]
    Data['DPK'][i] /= shell_V[i]
print(RadioNuclide, TotEdep/MeV)

D0 = Data['DPK'][0] * (Data['eff_distances'][0] / 1e-15)**2
Data['eff_distances'].insert(0, 1e-15)
Data['DPK'].insert(0, D0)
Data['uncertainty'].insert(0, 0)
Data['DPK'] = np.array(Data['DPK'])
Data['DPK'] = list(np.where(Data['DPK'] == 0, np.min(
    Data['DPK'][np.nonzero(Data['DPK'] > 0)])/10, Data['DPK']))
Data['DPK'] = np.array(Data['DPK'])
print(np.min(Data['DPK'][np.nonzero(Data['DPK'] > 0)]))

Data['xnew2'] = np.array(Data['eff_distances'])**2
f2 = CubicSpline(np.log(Data['eff_distances']), np.log(Data['DPK']))
Data['ynew2'] = np.exp(f2(np.log(Data['eff_distances'])))
print(len(Data['eff_distances']), len(Data['DPK']), len(Data['ynew2']))

dpkFile = directory / f'DPK_{RadioNuclide}_fine.txt'
with dpkFile.open('w') as f_DPK:
    DPK = Data['DPK']
    eff_distances = np.array(Data['eff_distances'])
    ynew = Data['ynew2']
    uncertainty = Data['uncertainty']
    f_DPK.write('distance(mm) \t DPK (Gy/Bq) \t Uncertainty \n')
    for i in range(len(eff_distances)):
        f_DPK.write('{} \t {} \t {}\n'.format(
            eff_distances[i]/mm, ynew[i].item(), uncertainty[i]))
