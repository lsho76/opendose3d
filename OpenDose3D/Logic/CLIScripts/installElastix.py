import importlib
import slicer


def checkTesting(verbose=True):
    try:
        mainWindow = slicer.util.mainWindow(verbose)
        if mainWindow is not None:
            layoutManager = slicer.app.layoutManager()
            if layoutManager is not None:
                return False
        return True
    except:
        return True


def installModule(module="SlicerElastix"):
    emm = slicer.app.extensionsManagerModel()
    if emm.isExtensionInstalled(module):
        print(f"Detected {module}")
        return

    md = emm.retrieveExtensionMetadataByName(module)
    if not md or 'extension_id' not in md:
        raise IOError(f"{module} is not installed properly")

    if emm.downloadAndInstallExtension(md['extension_id']):
        if not checkTesting():
            slicer.app.confirmRestart(
                f"Restart to complete {module} installation")
    else:
        raise IOError(f"{module} was not installed properly")


moduleLoader = importlib.util.find_spec("Elastix")
if not moduleLoader:
    try:
        installModule("SlicerElastix")
        moduleLoader = importlib.util.find_spec("Elastix")
    except IOError as e:
        print(e.message)
