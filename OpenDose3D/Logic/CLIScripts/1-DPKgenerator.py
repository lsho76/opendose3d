## Use: python3 1-DPKgenerator.py F 9 18
#  three arguments are expected:
#     1. The isotope Symbol
#     2. The isotope Z (atomic number)
#     3. The isotope A (atomic mass)
import numpy as np
from os.path import expanduser
from shutil import rmtree
from pathlib import Path

a = np.concatenate((np.array([0]), np.logspace(start=-3, stop=3, num=199)))
lsize = len(a)-1
NPrimaries = 1e8

Isotope = {'Symbol': 'F', 'Z': 9, 'A': 18}  # Modify
directory = Path(expanduser('~')) / 'Documents' / 'DVK'  # Modify

# If provided use the radionuclide
if len(list(sys.argv)) > 1:
    arguments = list(sys.argv)
    Isotope = {
        'Symbol': arguments[1],
        'Z': int(float(arguments[2])),
        'A': int(float(arguments[3]))
    }
RadioNuclide = f"{Isotope['Symbol']}-{Isotope['A']}"

outdir = directory / f'output-{RadioNuclide}'
if outdir.exists():
    rmtree(str(outdir))
outdir.mkdir(exist_ok=True)
gateMacro = directory / f'DPKspheres_{RadioNuclide}.mac'
with gateMacro.open('w') as f:
    f.write('#=====================================================\n')
    f.write('# GEOMETRY\n')
    f.write('#=====================================================\n')
    f.write('/gate/geometry/setMaterialDatabase GateMaterials.db\n')
    f.write('/gate/world/geometry/setXLength 1.1 m\n')
    f.write('/gate/world/geometry/setYLength 1.1 m\n')
    f.write('/gate/world/geometry/setZLength 1.1 m\n')
    f.write('/gate/world/setMaterial Water\n')
    f.write('#=====================================================\n')
    f.write('# PHYSICS\n')
    f.write('#=====================================================\n')
    f.write('/gate/physics/addPhysicsList emlivermore\n')
    f.write('/gate/physics/addProcess                         Decay\n')
    f.write('/gate/physics/addProcess   RadioactiveDecay GenericIon\n')
    f.write('#=====================================================\n')
    f.write('/gate/physics/Gamma/SetCutInRegion      world 0.001 mm\n')
    f.write('/gate/physics/Electron/SetCutInRegion   world 0.001 mm\n')
    f.write('/gate/physics/Positron/SetCutInRegion   world 0.001 mm\n')
    f.write('/gate/physics/displayCuts\n')
    f.write('#=====================================================\n')
    f.write('# SPHERES\n')
    f.write('#=====================================================\n')
    for i in range(lsize):
        f.write(
            f'/gate/world/daughters/name                      sphere{i+1}\n')
        f.write('/gate/world/daughters/insert                    sphere\n')
    f.write('#=====================================================\n')
    f.write('# SHELLS\n')
    f.write('#=====================================================\n')
    for i in range(lsize):
        f.write(f'/gate/sphere{i+1}/geometry/setRmin {round(a[i],6)} mm\n')
        f.write(f'/gate/sphere{i+1}/geometry/setRmax {round(a[i+1],6)} mm\n')
        f.write(f'/gate/sphere{i+1}/setMaterial Water\n')
    f.write('#=====================================================\n')
    f.write('# DETECTORS\n')
    f.write('#=====================================================\n')
    f.write('/gate/actor/addActor               SimulationStatisticActor stat\n')
    f.write(
        f'/gate/actor/stat/save              output-{RadioNuclide}/{RadioNuclide}-stat.txt\n')
    f.write(f'/gate/actor/stat/saveEveryNSeconds  3600\n')
    for i in range(lsize):
        f.write(f'/gate/actor/addActor           DoseActor  dose3D-{i+1}\n')
        f.write(
            f'/gate/actor/dose3D-{i+1}/save       output-{RadioNuclide}/Sphere-{i+1}.txt\n')
        f.write(
            f'/gate/actor/dose3D-{i+1}/attachTo              sphere{i+1}\n')
        f.write(f'/gate/actor/dose3D-{i+1}/stepHitType             random\n')
        f.write(f'/gate/actor/dose3D-{i+1}/setPosition           0 0 0 mm\n')
        f.write(f'/gate/actor/dose3D-{i+1}/setResolution         1 1 1\n')
        f.write(f'/gate/actor/dose3D-{i+1}/enableEdep            true\n')
        f.write(f'/gate/actor/dose3D-{i+1}/enableUncertaintyEdep true\n')
        f.write(f'/gate/actor/dose3D-{i+1}/enableDose            false\n')
        f.write(f'/gate/actor/dose3D-{i+1}/enableNumberOfHits    true\n')
        f.write(f'/gate/actor/dose3D-{i+1}/saveEveryNSeconds    3600\n')
    f.write('#=====================================================\n')
    f.write('# INITIALIZATION\n')
    f.write('#=====================================================\n')
    f.write('/gate/run/initialize\n')
    f.write('/geometry/test/run\n')
    f.write('#=====================================================\n')
    f.write('# SOURCE\n')
    f.write('#=====================================================\n')
    f.write(f'/gate/source/addSource {RadioNuclide}\n')
    f.write(f'/gate/source/{RadioNuclide}/gps/centre 0 0 0 mm\n')
    f.write(f'/gate/source/{RadioNuclide}/gps/particle ion\n')
    f.write(
        f'/gate/source/{RadioNuclide}/gps/ion {Isotope["Z"]} {Isotope["A"]} 0 0\n')
    f.write(f'/gate/source/{RadioNuclide}/setForcedUnstableFlag true\n')
    f.write(f'/gate/source/{RadioNuclide}/useDefaultHalfLife\n')
    f.write(f'/gate/source/{RadioNuclide}/gps/angtype iso\n')
    f.write(f'/gate/source/{RadioNuclide}/gps/energytype Mono\n')
    f.write(f'/gate/source/{RadioNuclide}/gps/monoenergy 0. keV\n')
    f.write('/gate/source/list\n')
    f.write('#=====================================================\n')
    f.write('# START\n')
    f.write('#=====================================================\n')
    f.write('/gate/random/setEngineName MersenneTwister\n')
    f.write('/gate/random/setEngineSeed auto\n')
    f.write(f'/gate/application/setTotalNumberOfPrimaries {NPrimaries}\n')
    f.write('/gate/application/start\n')
