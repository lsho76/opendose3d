from datetime import datetime
from pathlib import Path
import os
import sys
import re
import numpy as np
import slicer

from Logic.errors import IOError

# attribute values for creating new test nodes


def whoami():
    return sys._getframe(1).f_code.co_name


def checkTesting():
    try:
        mainWindow = slicer.util.mainWindow()
        if mainWindow:
            layoutManager = slicer.app.layoutManager()
            if layoutManager:
                return False
        return True
    except Exception as e:
        print(e)
        return True


def getScriptPath():
    try:
        script_path = Path(__file__).resolve().parents[1]
    except:
        from inspect import getsourcefile
        script_path = Path(getsourcefile(lambda: 0)).resolve().parents[1]
    finally:
        return script_path


def getCurrentTime():
    return str(datetime.now().strftime('%Y-%m-%d %H:%M:%S'))


def getSystemCoresInfo():
    if sys.platform.startswith('win'):
        from multiprocessing import cpu_count
        cpu_cores = cpu_count()
        cpu_processors = cpu_cores
        lsystem = 'windows'
    else:
        from cpu_cores import CPUCoresCounter
        fac = CPUCoresCounter.factory()
        cpu_cores = fac.get_physical_cores_count()
        cpu_processors = fac.get_physical_processors_count()
        lsystem = 'posix'

    return cpu_cores, cpu_processors, lsystem


def change_permissions_recursive(path, mode):
    for root, dirs, files in os.walk(path, topdown=False):
        for dir in [os.path.join(root, d) for d in dirs]:
            os.chmod(dir, mode)
        for lfile in [os.path.join(root, f) for f in files]:
            os.chmod(lfile, mode)


def extractMode(name):
    return ' '.join(name.split(':')[1].split()[1:-1])


def get_valid_filename(s):
    """
    Return the given string converted to a string that can be used for a clean
    filename. Remove leading and trailing spaces; convert other spaces to
    underscores; and remove anything that is not an alphanumeric, dash,
    underscore, or dot.
    >>> get_valid_filename("john's portrait in 2004.jpg")
    'johns_portrait_in_2004.jpg'
    """
    s = str(s).strip().replace(' ', '_')
    return re.sub(r'(?u)[^-\w.]', '', s)


def Center_of_Mass(Matrix, spacing):
    ''' Calculates the center of mass of a 3D matrix
    '''
    # Shape
    shape = np.shape(Matrix)
    # Axis
    x = np.arange(0, shape[0])*spacing[0]
    y = np.arange(0, shape[1])*spacing[1]
    z = np.arange(0, shape[2])*spacing[2]
    # Mass along axis
    mx = np.sum(Matrix, axis=(1, 2))
    my = np.sum(Matrix, axis=(0, 2))
    mz = np.sum(Matrix, axis=(0, 1))
    # center of Masses
    comx = np.sum(x*mx)/np.sum(mx)
    comy = np.sum(y*my)/np.sum(my)
    comz = np.sum(z*mz)/np.sum(mz)
    return np.array([comx, comy, comz])


def checkRequirements():
    # Install all requirements
    import importlib
    from slicer.util import pip_install
    pip_install('pip -U')
    spectList = ['scipy', 'pydicom', 'sentry_sdk', 'pillow', 'cpu_cores',
                 'xsdata', 'defusedxml', 'xmltodict', 'lxml','xmlschema', 
                 'simpleeval', 'pymedphys', 'matplotlib', 'scikit-image',
                 'singleton-decorator','importlib-metadata', 'lmfit']

    for spec in spectList:
        found = importlib.util.find_spec(spec) is not None
        if not found:
            pip_install(f"{spec} -U")


def clearLayout(layout):
    if not layout:
        return
    while layout.count():
        child = layout.takeAt(0)
        if child.widget():
            child.widget().deleteLater()


def installModule(module="SlicerElastix"):
    emm = slicer.app.extensionsManagerModel()
    if emm.isExtensionInstalled(module):
        print(f"Detected {module}")
        return

    md = emm.retrieveExtensionMetadataByName(module)
    if not md or 'extension_id' not in md:
        raise IOError(f"{module} is not installed properly")

    if emm.downloadAndInstallExtension(md['extension_id']):
        if not checkTesting():
            slicer.app.confirmRestart(
                f"Restart to complete {module} installation")
    else:
        raise IOError(f"{module} was not installed properly")


def isNumber(x):
    try:
        if float(x) == 0.0:
            return True
        lx = x / float(x)  # 0 would have failed this
        if lx == 1.0:
            return True
        else:
            return False
    except:
        return False


def reject_outliers(data, m=2):  # Remove outliers
    d = np.abs(data - np.median(data))
    mdev = np.median(d)
    s = d/mdev if mdev else 0
    return np.where(s < m, data, np.nan)
