import importlib
import json
from pathlib import Path

import ctk
import qt
import slicer
from slicer.ScriptedLoadableModule import ScriptedLoadableModule, ScriptedLoadableModuleWidget

# Check requirements
from Logic import utils
utils.checkRequirements()

from simpleeval import SimpleEval
from Logic.OpenDose3DLogic import OpenDose3DLogic
from Logic.OpenDose3DTest import OpenDose3DTest
from Logic.vtkmrmlutilsTest import vtkmrmlutilsTest
from Logic.xmlutilsTest import xmlutilsTest
from Logic.nodesTest import nodesTest
from Logic import vtkmrmlutils, logging, sentry, xmlexport, dbutils, jsonutils
from Logic.fit_values import FIT_FUNCTIONS
from Logic.config import readSPECTSensitivity
from Logic.gate import Gate
from Logic.constants import Constants
from Logic.nodes import Node

__submoduleNames__ = ['OpenDose3DLogic', 'OpenDose3DTest', 'fit_values', 'fitutils', 'PhysicalUnits', 'vtkmrmlutils', 'vtkutils', 'constants',
                      'vtkmrmlutilsTest', 'testbuilder', 'testutils', 'xmlutils', 'xmlutilsTest', 'nodes', 'nodesTest', 'errors', 'Process',
                      'attributes', 'xmlconstants', 'config', 'sentry', 'logging', 'utils', 'xmlexport', 'dbutils', 'gate', 'jsonutils']
__package__ = 'OpenDose3D'
mod = importlib.import_module('Logic', __name__)
importlib.reload(mod)
__all__ = ['OpenDose3D', 'OpenDose3DWidget',
           'OpenDose3DLogic', 'OpenDose3DTest']


#######################
#                     #
# OpenDose3D         #
#                     #
#######################


class OpenDose3D(ScriptedLoadableModule):
    """Uses ScriptedLoadableModule base class, available at:
    https://github.com/Slicer/Slicer/blob/master/Base/Python/slicer/ScriptedLoadableModule.py
    """

    def __init__(self, parent):
        super().__init__(parent)
        self.script_path = utils.getScriptPath()
        # TODO make this more human readable by adding spaces
        self.parent.title = "OpenDose Dosimetry 3D"
        self.parent.categories = ["Radiotherapy"]
        self.parent.dependencies = []
        # replace with "Firstname Lastname (Organization)"
        self.parent.contributors = [
            "Alex Vergara Gil (INSERM, France)", "Janick Rueegger (KSA, Switzerland)", "Ludovic Ferrer (NANTES, France)"]
        self.parent.helpText = '''
            This module implements the full 3D Dosimetry for molecular radiotherapy on multiple time points.\n
            Naming convention: \n
              Volumes must contain the time in hours like 4HR\n
              Volumes must contain the identifier "CTCT" for CT and "ACSC" for SPECT\n
              By pressing the Clean Scene button all files that does not follow this convention will be removed from scene!!\n
            WARNING: This module is currently in beta stage and it CONTAINS BUGS. DO NOT use it for clinical purposes!!!
        '''
        self.parent.helpText += '<p>For more information see the <a href="https://gitlab.com/opendose/opendose3d/-/blob/develop/User_Manual.md">online documentation</a>.</p>'
        self.parent.acknowledgementText = '''
            This software was originally developed by Alex Vergara Gil (INSERM, France) and Janick Rueegger (KSA, Switzerland),
            in collaboration with Ludovic Ferrer (NANTES, France) and Thiago NM Lima (KSA, Switzerland).
        '''  # replace with organization, grant and thanks.

        sentry.init()

    def runTest(self, msec=100, **kwargs):
        """
        :param msec: delay to associate with :func:`ScriptedLoadableModuleTest.delayDisplay()`.
        """
        # test vtkmrmlutilsTest
        testCase = vtkmrmlutilsTest()
        testCase.messageDelay = msec
        testCase.runTest(**kwargs)

        # test nodesTest
        testCase = nodesTest()
        testCase.messageDelay = msec
        testCase.runTest(**kwargs)

        # test xmlutilsTest
        testCase = xmlutilsTest()
        testCase.messageDelay = msec
        testCase.runTest(**kwargs)

        # test jsonutils
        testCase = jsonutils.jsonutilsTest()
        testCase.messageDelay = msec
        testCase.runTest(**kwargs)

        # test OpenDose3DTest
        # name of the test case class is expected to be <ModuleName>Test
        module = importlib.import_module(self.__module__)
        className = self.moduleName + 'Test'
        try:
            TestCaseClass = getattr(module, className)
        except AttributeError:
            # Treat missing test case class as a failure; provide useful error message
            raise AssertionError(
                f'Test case class not found: {self.__module__}.{className} ')

        testCase = TestCaseClass()
        testCase.messageDelay = msec
        testCase.runTest(**kwargs)

        print('\n******* All tests passed **********\n')


#######################
#                     #
# OpenDose3DWidget   #
#                     #
#######################


class OpenDose3DWidget(ScriptedLoadableModuleWidget):
    """Uses ScriptedLoadableModuleWidget base class, available at:
    https://github.com/Slicer/Slicer/blob/master/Base/Python/slicer/ScriptedLoadableModule.py
    """

    def setupOptions(self):
        self.options = {}
        self.optionsFile = self.script_path / "Resources" / "JSON" / "options.json"
        if not self.optionsFile.exists():
            self.options = {
                'Isotope': 2,
                'InjectionActivity': '6848',
                'InjectionTime': 0,
                'usecolors': 1,
                'usePath': str(self.usePath)
            }
            with self.optionsFile.open('w') as f:
                f.write(json.dumps(self.options))
        else:
            with self.optionsFile.open('r') as f:
                self.options = json.load(f)
                if not 'Isotope' in self.options:
                    self.options['Isotope'] = 2
                if not 'InjectionActivity' in self.options:
                    self.options['InjectionActivity'] = 6848
                if not 'InjectionTime' in self.options:
                    self.options['InjectionTime'] = 0
                if not 'usecolors' in self.options:
                    self.options['usecolors'] = 1
                if not 'usePath' in self.options:
                    self.options['usePath'] = str(self.usePath)
        self.usePath = Path(self.options['usePath'])
        self.calibration = {}
        self.calibrationFile = self.script_path / \
            "Resources" / "JSON" / "calibration.json"
        if self.calibrationFile.exists():
            with self.calibrationFile.open('r') as f:
                self.calibration = json.load(f)
        else:
            self.calibration = {
                "CTCalibration": {
                    "a0": 0.001, "b0": 1, "a1": 0.0005116346986394071, "b1": 1},
                "SPECTSensitivity": {"Value": 122.6, "Units": "counts/MBqs", "Time": 1800},
                "SPECTRecovery": {}
            }

    def saveOptions(self):
        if self.optionsFile.exists():
            self.optionsFile.unlink()
        self.options = {'Isotope': self.isotopsList.currentIndex,
                        'InjectionActivity': self.InjectionActivity.text,
                        'InjectionTime': self.InjectionTime.text,
                        'usecolors': 1 if self.useColorButton.isChecked() else 0,
                        'usePath': str(self.usePath)}
        with self.optionsFile.open('w') as f:
            f.write(json.dumps(self.options))

    def setup(self):
        ''' Creation of the Module Widgets
            This procedure set up all widgets and define all conections
            TODO: Add CT calibration curve
        '''
        self.script_path = utils.getScriptPath()
        self.usePath = self.script_path

        # self.developerMode = False # Comment to show developer mode

        self.setupOptions()
        self.logger = logging.getLogger('OpenDose3D')
        self.logger.setLevel(logging.WARNING)
        self.DoseCalculated = False
        self.ADRMPlotsMade = False
        self.ACTMPlotsMade = False
        self.SegmentationPropagated = False
        self.Evaluator = SimpleEval()
        self.db = self.script_path / "Resources" / "OpenDose3D.db3"
        self.enableColors = bool(self.options['usecolors'])
        self.setupWidgets()

    def setupWidgets(self):
        # Instantiate and connect widgets ...
        utils.clearLayout(self.layout)
        super().setup()

        #
        # Start of Parameters Area
        #
        ''' This section is for calibration data (CT, SPECT) and for general data input like isotope.
        '''

        ParametersCollapsibleButton = ctk.ctkCollapsibleButton()
        ParametersCollapsibleButton.text = "Parameters"
        self.layout.addWidget(ParametersCollapsibleButton)
        ParameterLayout = qt.QHBoxLayout(ParametersCollapsibleButton)
        ParametersWidget = qt.QWidget()
        ParameterLayout.addWidget(ParametersWidget)
        if self.enableColors:
            ParametersCollapsibleButton.setStyleSheet(
                Constants().createCSS('Parameters'))
        ParametersFormLayout = qt.QFormLayout(ParametersWidget)

        calibrationHContainer = qt.QHBoxLayout()

        label = qt.QLabel("Select clinical center")
        calibrationHContainer.addWidget(label)

        self.sensitivity = readSPECTSensitivity()
        centers = list(self.sensitivity.keys())
        self.ClinicalCenter = qt.QComboBox()
        self.ClinicalCenter.setToolTip(
            "Create your center in Calibration module")
        self.ClinicalCenter.addItems(centers)
        calibrationHContainer.addWidget(self.ClinicalCenter)

        label = qt.QLabel("Select calibration date")
        calibrationHContainer.addWidget(label)

        self.calibrationDate = qt.QComboBox()
        self.calibrationDate.setToolTip(
            "Create calibration data in Calibration module")
        calibrationHContainer.addWidget(self.calibrationDate)

        ParametersFormLayout.addRow(calibrationHContainer)

        # isotopes
        self.isotopsList = qt.QComboBox()
        self.defaultIsotopesList = Constants().isotopes.keys()
        self.isotopsList.addItems(sorted(self.defaultIsotopesList))
        self.isotopsList.setCurrentIndex(int(self.options['Isotope']))
        ParametersFormLayout.addRow("Isotope", self.isotopsList)

        # SPECT sensitivity (MBq/counts)
        sensitivityHContainer = qt.QHBoxLayout()
        self.sensitivityUnits = qt.QComboBox()
        self.sensitivityUnits.addItems(['Bq/counts', 'MBq/counts', 'counts/Bq', 'counts/MBq',
                                        'Bqs/counts', 'MBqs/counts', 'counts/Bqs', 'counts/MBqs'])
        index = self.sensitivityUnits.findText(
            self.calibration['SPECTSensitivity']['Units'], qt.Qt.MatchFixedString)
        if index > 0:
            self.sensitivityUnits.setCurrentIndex(index)

        self.spectSensitivity = qt.QLineEdit()
        self.spectSensitivity.setText(
            str(self.calibration['SPECTSensitivity']['Value']))
        self.spectSensitivity.setToolTip("Camera factor, select proper unit.")

        sensitivityHContainer.addWidget(self.spectSensitivity)
        sensitivityHContainer.addWidget(self.sensitivityUnits)
        ParametersFormLayout.addRow("Camera factor", sensitivityHContainer)

        self.calibrationTimeContainer = qt.QFrame()
        layout = qt.QHBoxLayout()
        layout.addStretch()
        label = qt.QLabel("Calibration Time (seconds)")
        layout.addWidget(label)
        self.spectCalibrationTime = qt.QLineEdit()
        self.spectCalibrationTime.setText(
            str(self.calibration['SPECTSensitivity']['Time']))
        layout.addWidget(self.spectCalibrationTime)
        self.calibrationTimeContainer.setLayout(layout)
        ParametersFormLayout.addRow(self.calibrationTimeContainer)
        self.setCalibrationTimeVisibility()

        # Injection Activity
        self.InjectionActivity = qt.QLineEdit()
        self.InjectionActivity.setText(str(self.options['InjectionActivity']))
        ParametersFormLayout.addRow(
            "Injected Activity (MBq)", self.InjectionActivity)

        # Injection Time
        self.InjectionTime = qt.QLineEdit()
        self.InjectionTime.setText(str(self.options['InjectionTime']))
        ParametersFormLayout.addRow(
            "Injection Time", self.InjectionTime)

        ParametersWidget.setLayout(ParametersFormLayout)

        self.isotopsList.currentTextChanged.connect(self.onIsotopeChange)
        self.ClinicalCenter.currentTextChanged.connect(self.onIsotopeChange)
        self.calibrationDate.currentTextChanged.connect(
            self.onCalibrationDateChanged)
        self.sensitivityUnits.currentIndexChanged.connect(
            self.setCalibrationTimeVisibility)

        #
        # End of Parameters Area
        #

        #
        # Start of Preprocessing Area
        #
        ''' This section is for preprocessing
              1. Rename files will create the required variables, will rename all files to a proper convention and will group everything in time points folders
              2. Resample CTs will make use of Resample Volume (BRAINS) to resample the CTs to SPECT resolution required by most methods
              3. Rescale will convert the CT volumes to density volumes and will apply sensitivity to SPECT so it will also create activity volumes
        '''

        PreprocessingCollapsibleButton = ctk.ctkCollapsibleButton()
        PreprocessingCollapsibleButton.text = "Preprocessing"
        self.layout.addWidget(PreprocessingCollapsibleButton)
        PreprocessingLayout = qt.QHBoxLayout(PreprocessingCollapsibleButton)
        PreprocessingWidget = qt.QWidget()
        PreprocessingLayout.addWidget(PreprocessingWidget)
        if self.enableColors:
            PreprocessingCollapsibleButton.setStyleSheet(
                Constants().createCSS('Processing'))
        PreprocessingFormLayout = qt.QFormLayout(PreprocessingWidget)

        PreprocessingHContainer = qt.QHBoxLayout()

        # rename files
        self.renameButton = qt.QPushButton("Rename files")
        self.renameButton.toolTip = "Rename original DICOM files and sort them by time point."
        PreprocessingHContainer.addWidget(self.renameButton)

        # resample the CTs
        self.resampleButton = qt.QPushButton("Resample CTs")
        self.resampleButton.toolTip = "Resample the CTs to SPECT resolution using Lanczos interpolation."
        PreprocessingHContainer.addWidget(self.resampleButton)

        # scale the SPECT values and calculate density matrixes
        self.rescaleButton = qt.QPushButton("Rescale")
        self.rescaleButton.toolTip = "Rescale the SPECT values and calculate density matrixes."
        PreprocessingHContainer.addWidget(self.rescaleButton)

        PreprocessingFormLayout.addRow(PreprocessingHContainer)
        PreprocessingWidget.setLayout(PreprocessingFormLayout)

        # connections
        self.renameButton.clicked.connect(self.onRenameButton)
        self.resampleButton.clicked.connect(self.onResampleButton)
        self.rescaleButton.clicked.connect(self.onRescaleButton)

        #
        # End of Preprocessing Area
        #

        #
        # Start of Registration Area
        #
        ''' This section is for registration of multiple time points.
              1. The registration is made CT to CT, you need to provide the reference CT to start with. Once is executed, a transformation will be applied to all volumes in each time point.
              2. User are expected to check the output of this module, is it is incorrect then go to transform module and manually adjust the transformations to get a closer registration in each time point. The finally run again the registration provided here.
              3. Users are not supposed to provide his own transformations as they will be unreadable by the functionality, follow strictly the steps: execute -> manual check -> execute
        '''

        registrationCollapsibleButton = ctk.ctkCollapsibleButton()
        registrationCollapsibleButton.text = "Registration"
        self.layout.addWidget(registrationCollapsibleButton)
        registrationLayout = qt.QHBoxLayout(registrationCollapsibleButton)
        registrationWidget = qt.QWidget()
        registrationLayout.addWidget(registrationWidget)
        if self.enableColors:
            registrationCollapsibleButton.setStyleSheet(
                Constants().createCSS('Slicer'))
        registrationFormLayout = qt.QFormLayout(registrationWidget)

        # input volume selector
        self.initialPoint = slicer.qMRMLNodeComboBox()
        self.initialPoint.nodeTypes = ["vtkMRMLScalarVolumeNode"]
        self.initialPoint.addAttribute(
            "vtkMRMLScalarVolumeNode", "DICOM.Modality", "CT")
        self.initialPoint.selectNodeUponCreation = True
        self.initialPoint.addEnabled = False
        self.initialPoint.removeEnabled = False
        self.initialPoint.noneEnabled = False
        self.initialPoint.showHidden = False
        self.initialPoint.showChildNodeTypes = False
        self.initialPoint.setMRMLScene(slicer.mrmlScene)
        self.initialPoint.setToolTip("Select the reference Volume.")
        registrationFormLayout.addRow("Reference Volume: ", self.initialPoint)

        # Execute Button
        transformationHContainer = qt.QHBoxLayout()
        self.registrationButton = qt.QPushButton("Execute")
        self.registrationButton.toolTip = "Execute the CT-CT registration."
        transformationHContainer.addWidget(self.registrationButton)
        self.elastixContainer = qt.QHBoxLayout()
        self.installElastixButton = qt.QPushButton("Install Elastix")
        self.installElastixButton.toolTip = "Installs Slicer Elastix if it is not installed."
        transformationHContainer.addWidget(self.installElastixButton)
        emm = slicer.app.extensionsManagerModel()
        if emm.isExtensionInstalled("SlicerElastix"):
            self.installElastixButton.hide()
        self.switchtoTransformsButton = qt.QPushButton("Check Transforms")
        self.switchtoTransformsButton.toolTip = "Switches to Transforms Module for checking the transformations."
        transformationHContainer.addWidget(self.switchtoTransformsButton)
        registrationFormLayout.addRow(transformationHContainer)

        registrationWidget.setLayout(registrationFormLayout)

        # connections
        self.registrationButton.clicked.connect(self.onRegistrationButton)
        self.installElastixButton.clicked.connect(
            lambda: self.onInstallModuleButton("SlicerElastix"))
        self.initialPoint.currentNodeChanged.connect(self.onInitialPointChange)
        self.switchtoTransformsButton.clicked.connect(
            lambda: self.switchModule('Transforms'))

        #
        # End of Registration Area
        #

        #
        # Start of Absorbed Dose Rate Calculation Area
        #
        ''' This section is for absorbed dose rate calculation. User has to select the proper method depending on study type. If more isotopes are needed then make a request.
        '''
        doseCollapsibleButton = ctk.ctkCollapsibleButton()
        doseCollapsibleButton.text = "Absorbed Dose Rate calculation"
        self.layout.addWidget(doseCollapsibleButton)
        doseLayout = qt.QHBoxLayout(doseCollapsibleButton)
        doseWidget = qt.QWidget()
        doseLayout.addWidget(doseWidget)
        if self.enableColors:
            doseCollapsibleButton.setStyleSheet(
                Constants().createCSS('Calculation'))
        doseFormLayout = qt.QFormLayout(doseWidget)

        # Dose Rate calculation algorithm
        self.doseRateAlgorithm = qt.QComboBox()
        Algorithms = ['Local Energy Deposition (LED)',
                      'FFT convolution (homogeneous)',
                      #   'Convolution (homogeneous)', # Removed by Manuel Bardies
                      # Removed by Manuel Bardies
                      'Convolution (heterogeneous)',
                      'Monte Carlo']
        self.doseRateAlgorithm.addItems(Algorithms)
        doseFormLayout.addRow(
            "Absorbed dose rate algorithm", self.doseRateAlgorithm)

        self.kernelDistanceLimitContainer = qt.QFrame()
        layout1 = qt.QHBoxLayout()
        layout1.addStretch()
        label = qt.QLabel("kernel limit (mm)")
        layout1.addWidget(label)
        self.kernelDistanceLimit = qt.QDoubleSpinBox()
        self.kernelDistanceLimit.setDecimals(1)
        self.kernelDistanceLimit.setMinimum(0)
        self.kernelDistanceLimit.setMaximum(1000)
        self.kernelDistanceLimit.value = 0
        self.kernelDistanceLimit.setToolTip(
            "Maximum distance in mm to consider for the convolution kernel.\n A value of 0 mm means to use the entire kernel")
        layout1.addWidget(self.kernelDistanceLimit)
        self.kernelDistanceLimitContainer.setLayout(layout1)
        doseFormLayout.addRow(self.kernelDistanceLimitContainer)
        self.kernelDistanceLimitContainer.hide()

        self.densityCorrectionContainer = qt.QFrame()
        layout2 = qt.QHBoxLayout()
        self.applyDensityCorrection = qt.QCheckBox("Apply density correction")
        self.applyDensityCorrection.setChecked(True)
        self.kernelMaterialContainer = qt.QFrame()
        layout3 = qt.QHBoxLayout()
        label = qt.QLabel("select kernel material")
        layout3.addWidget(label)
        materials = [materialName['name']
                     for materialName in Constants().materials.values()]
        self.kernelMaterial = qt.QComboBox()
        self.kernelMaterial.addItems(materials)
        layout3.addWidget(self.kernelMaterial)
        self.kernelMaterialContainer.setLayout(layout3)
        self.kernelMaterialContainer.hide()
        layout2.addWidget(self.applyDensityCorrection)
        layout2.addWidget(self.kernelMaterialContainer)
        self.densityCorrectionContainer.setLayout(layout2)
        doseFormLayout.addRow(self.densityCorrectionContainer)
        # self.densityCorrectionContainer.hide()

        # threshold value
        self.activityThresholdContainer = qt.QFrame()
        layout1 = qt.QHBoxLayout()
        label = qt.QLabel("Activity threshold (%)")
        layout1.addWidget(label)
        self.activityThresholdSliderWidget = ctk.ctkSliderWidget()
        self.activityThresholdSliderWidget.singleStep = 0.1
        self.activityThresholdSliderWidget.minimum = 0
        self.activityThresholdSliderWidget.maximum = 10
        self.activityThresholdSliderWidget.value = 0
        self.activityThresholdSliderWidget.suffix = '%'
        self.activityThresholdSliderWidget.decimals = 1
        self.activityThresholdSliderWidget.setToolTip(
            "Set threshold value (%) for computing the Dose image. Voxels that have activity lower than this value will be set to zero.")
        layout1.addWidget(self.activityThresholdSliderWidget)
        self.activityThresholdContainer.setLayout(layout1)
        doseFormLayout.addRow(self.activityThresholdContainer)
        self.activityThresholdContainer.hide()

        # Calculate Dose Rate Button
        self.doseRateButton = qt.QPushButton("Calculate Dose Rate Images")
        self.doseRateButton.toolTip = "Execute the absorbed dose rate algorithm specified."
        doseFormLayout.addRow(self.doseRateButton)

        # Monte Carlo section
        self.MonteCarloContainer = qt.QFrame()
        MonteCarloHContainer = qt.QHBoxLayout()
        self.GenerateGateButton = qt.QPushButton("Generate Gate")
        self.GenerateGateButton.toolTip = "Generates Gate macros to run Monte Carlo."
        MonteCarloHContainer.addWidget(self.GenerateGateButton)
        self.ImportGateButton = qt.QPushButton("Import Gate")
        self.ImportGateButton.toolTip = "Import Gate results."
        MonteCarloHContainer.addWidget(self.ImportGateButton)
        self.MonteCarloContainer.setLayout(MonteCarloHContainer)
        doseFormLayout.addRow(self.MonteCarloContainer)
        self.MonteCarloContainer.hide()
        doseWidget.setLayout(doseFormLayout)

        # connections
        self.doseRateAlgorithm.currentIndexChanged.connect(
            self.onDoseRateAlgorithm)
        self.applyDensityCorrection.stateChanged.connect(
            self.onDensityCorrectionCheck)
        self.doseRateButton.clicked.connect(self.onDoseRateButton)
        self.GenerateGateButton.clicked.connect(self.onGenerateGate)
        self.ImportGateButton.clicked.connect(self.onImportMonteCarlo)

        #
        # End of Absorbed Dose Rate Calculation Area
        #

        #
        # Start of Segmentation Area
        #
        ''' This section is to handle with segmentations. User is supposed to provide a segmentation either by the use of Segment Editor module, or by importing his own segmentation. If RTStructs are imported for this reason they must be converted to slicer segments (they must contain a binary map)
              1. The segmentation is propagated to each time point
              2. The user is expected to modify each time point segmentation after propagation to match the movements of desired structures
        '''
        segmentationCollapsibleButton = ctk.ctkCollapsibleButton()
        segmentationCollapsibleButton.text = "Segmentation"
        self.layout.addWidget(segmentationCollapsibleButton)
        segmentationLayout = qt.QHBoxLayout(segmentationCollapsibleButton)
        segmentationWidget = qt.QWidget()
        segmentationLayout.addWidget(segmentationWidget)
        if self.enableColors:
            segmentationCollapsibleButton.setStyleSheet(
                Constants().createCSS('Slicer'))
        segmentationFormLayout = qt.QFormLayout(segmentationWidget)

        # Segmentation Propagation
        self.segmentation = slicer.qMRMLNodeComboBox()
        self.segmentation.nodeTypes = ["vtkMRMLSegmentationNode"]
        #self.segmentation.addAttribute("vtkMRMLScalarVolumeNode", "DICOM.Modality", "RTSTRUCT")
        self.segmentation.selectNodeUponCreation = True
        self.segmentation.addEnabled = False
        self.segmentation.removeEnabled = False
        self.segmentation.noneEnabled = False
        self.segmentation.showHidden = False
        self.segmentation.showChildNodeTypes = False
        self.segmentation.setMRMLScene(slicer.mrmlScene)
        self.segmentation.setToolTip("Select the reference segmentation.")
        segmentationFormLayout.addRow(
            "Reference Segmentation: ", self.segmentation)

        segmentationHContainer = qt.QHBoxLayout()
        self.propagateButton = qt.QPushButton("Propagate Segmentation")
        self.propagateButton.toolTip = "Propagates the segmentation to all time points."
        segmentationHContainer.addWidget(self.propagateButton)
        self.installExtraEffectsButton = qt.QPushButton(
            "Install extra effects")
        self.installExtraEffectsButton.toolTip = "Installs Segmentation Extra Effects if it is not installed."
        segmentationHContainer.addWidget(self.installExtraEffectsButton)
        if emm.isExtensionInstalled("SegmentEditorExtraEffects"):
            self.installExtraEffectsButton.hide()
        self.switchtoSegmentEditorButton = qt.QPushButton("Segment Editor")
        self.switchtoSegmentEditorButton.toolTip = "Switches to Segment Editor for creating the segmentation."
        segmentationHContainer.addWidget(self.switchtoSegmentEditorButton)
        segmentationFormLayout.addRow(segmentationHContainer)

        segmentationWidget.setLayout(segmentationFormLayout)

        # connections
        self.propagateButton.clicked.connect(self.onPropagateButton)
        self.installExtraEffectsButton.clicked.connect(
            lambda: self.onInstallModuleButton("SegmentEditorExtraEffects"))
        self.segmentation.currentNodeChanged.connect(self.checkAvailability)
        self.switchtoSegmentEditorButton.clicked.connect(
            lambda: self.switchModule('SegmentEditor'))

        #
        # End of Segmentation Area
        #

        #
        # Start of Creation of tables and Plots Area
        #
        ''' This section is for creation of tables and plots
              1. It will create activity(dose rate) tables for each segment in each time point, it also creates the segment densities tables (required for mass calculation)
              2. It will create the time plots of activity(dose rate)
              3. Plots are supposed to behave nicely, if this is not happening please check segmentation!!
        '''
        TablesandPlotsCollapsibleButton = ctk.ctkCollapsibleButton()
        TablesandPlotsCollapsibleButton.text = "Segment Tables and Plots"
        self.layout.addWidget(TablesandPlotsCollapsibleButton)
        TablesandPlotsLayout = qt.QHBoxLayout(TablesandPlotsCollapsibleButton)
        TablesandPlotsWidget = qt.QWidget()
        TablesandPlotsLayout.addWidget(TablesandPlotsWidget)
        if self.enableColors:
            TablesandPlotsCollapsibleButton.setStyleSheet(
                Constants().createCSS('Processing'))
        TablesandPlotsFormLayout = qt.QFormLayout(TablesandPlotsWidget)

        tablesHContainer = qt.QHBoxLayout()

        self.ACTMTablesButton = qt.QPushButton("Create ACTM Tables")
        self.ACTMTablesButton.toolTip = "Creates the activity tables for all structures in each time point."
        tablesHContainer.addWidget(self.ACTMTablesButton)

        self.DOSETablesButton = qt.QPushButton("Create ADR Tables")
        self.DOSETablesButton.toolTip = "Creates absorbed dose rate tables for all structures in each time point."
        tablesHContainer.addWidget(self.DOSETablesButton)

        TablesandPlotsFormLayout.addRow(tablesHContainer)

        TablesandPlotsWidget.setLayout(TablesandPlotsFormLayout)

        # connections
        self.ACTMTablesButton.clicked.connect(self.onACTMTablesButton)
        self.DOSETablesButton.clicked.connect(self.onDOSETablesButton)

        #
        # End of Creation of tables and Plots Area
        #

        #
        # Start Time Integration Area
        #
        TimeIntegrationCollapsibleButton = ctk.ctkCollapsibleButton()
        TimeIntegrationCollapsibleButton.text = "Time integration"
        self.layout.addWidget(TimeIntegrationCollapsibleButton)
        TimeIntegrationLayout = qt.QHBoxLayout(
            TimeIntegrationCollapsibleButton)
        TimeIntegrationWidget = qt.QWidget()
        TimeIntegrationLayout.addWidget(TimeIntegrationWidget)
        if self.enableColors:
            TimeIntegrationCollapsibleButton.setStyleSheet(
                Constants().createCSS('Calculation'))
        TimeIntegrationFormLayout = qt.QFormLayout(TimeIntegrationWidget)

        timeintHContainer = qt.QHBoxLayout()

        self.incorporationMode = qt.QComboBox()
        self.incorporationMode.addItems(['linear', 'constant', 'exponential'])
        self.incorporationMode.toolTip = "Select incorporation part aproximation"
        TimeIntegrationFormLayout.addRow(
            'Incorporation Mode', self.incorporationMode)

        self.integrationMode = qt.QComboBox()
        self.integrationMode.addItems(
            ['trapezoid', *tuple(FIT_FUNCTIONS.keys()), 'auto-fit'])
        self.integrationMode.toolTip = "Select integration algorithm"
        TimeIntegrationFormLayout.addRow(
            'Integration Algorithm', self.integrationMode)

        self.tailModeContainer = qt.QFrame()
        layout = qt.QHBoxLayout()
        layout.addStretch()
        labeltail = qt.QLabel("Tail Mode")
        layout.addWidget(labeltail)
        self.tailMode = qt.QComboBox()
        self.tailMode.addItems(["Physical Decay", "Effective Decay"])
        layout.addWidget(self.tailMode)
        self.tailModeContainer.setLayout(layout)
        TimeIntegrationFormLayout.addRow(self.tailModeContainer)
        self.onIntegrationModeAlgoritm()

        self.IntegrateActivityButton = qt.QPushButton("Integrate Activity")
        self.IntegrateActivityButton.toolTip = "Integrates activity in time for all structures. Returns cumulative activity per segment"
        timeintHContainer.addWidget(self.IntegrateActivityButton)

        self.IntegrateDoseButton = qt.QPushButton(
            "Integrate Absorbed Dose Rate")
        self.IntegrateDoseButton.toolTip = "Integrates absorbed dose rates in time for all structures. Returns absorbed dose per segment"
        timeintHContainer.addWidget(self.IntegrateDoseButton)

        TimeIntegrationFormLayout.addRow(timeintHContainer)
        TimeIntegrationWidget.setLayout(TimeIntegrationFormLayout)

        # connections

        self.IntegrateActivityButton.clicked.connect(
            self.onIntegrateActivityButton)
        self.integrationMode.currentIndexChanged.connect(
            self.onIntegrationModeAlgoritm)
        self.IntegrateDoseButton.clicked.connect(self.onIntegrateDoseButton)

        #
        # End Time Integration Area
        #

        #
        # Start Utilities Area
        #
        ''' This section contains two functionalities:
              1. Clean Scene: check naming convention and erase everything that does not follow it, also erases all additional data for reprocessing
        '''

        UtilitiesCollapsibleButton = ctk.ctkCollapsibleButton()
        UtilitiesCollapsibleButton.text = "Utilities"
        UtilitiesCollapsibleButton.collapsed = True
        self.layout.addWidget(UtilitiesCollapsibleButton)
        UtilitiesFormLayout = qt.QFormLayout(UtilitiesCollapsibleButton)
        UtilitiesHContainer = qt.QHBoxLayout()

        exportHContainer = qt.QHBoxLayout()

        self.ACTMExportButton = qt.QPushButton("Export XML: ACTM")
        self.ACTMExportButton.toolTip = "Export XML for Cumulated Activity Mode."
        exportHContainer.addWidget(self.ACTMExportButton)

        self.DOSEExportButton = qt.QPushButton("Export XML: ADRM")
        self.DOSEExportButton.toolTip = "Export XML for Absorbed Dose Rate Mode."
        exportHContainer.addWidget(self.DOSEExportButton)

        UtilitiesFormLayout.addRow(exportHContainer)

        # DB Compression
        if dbutils.isCompressed():  # Database is compressed
            self.DBCompressionButton = qt.QPushButton("Decompress Database")
            self.DBCompressionButton.toolTip = "Decompress the database."
        else:
            self.DBCompressionButton = qt.QPushButton("Compress Database")
            self.DBCompressionButton.toolTip = "Compress the database."

        UtilitiesHContainer.addWidget(self.DBCompressionButton)

        # Clean up
        self.CleanUpButton = qt.QPushButton("Clean Scene")
        self.CleanUpButton.toolTip = "Cleans Scene from all additional or intermediate results."
        UtilitiesHContainer.addWidget(self.CleanUpButton)

        UtilitiesFormLayout.addRow(UtilitiesHContainer)

        # connections
        self.DBCompressionButton.clicked.connect(self.onCompressDB)
        self.CleanUpButton.clicked.connect(self.onCleanUpButton)
        self.ACTMExportButton.clicked.connect(self.onACTMExportButton)
        self.DOSEExportButton.clicked.connect(self.onDOSEExportButton)

        #
        # End Utilities Area
        #

        #
        # Start Options Area
        #

        OptionsCollapsibleButton = ctk.ctkCollapsibleButton()
        OptionsCollapsibleButton.text = "Options"
        OptionsCollapsibleButton.collapsed = False
        self.layout.addWidget(OptionsCollapsibleButton)
        OptionsFormLayout = qt.QFormLayout(OptionsCollapsibleButton)
        OptionsVContainer = qt.QVBoxLayout()

        OptionsHContainer = qt.QHBoxLayout()
        self.LegendButton = qt.QCheckBox("Show Legend in graphs")
        self.ShowLegend = False
        self.LegendButton.setChecked(self.ShowLegend)
        OptionsHContainer.addWidget(self.LegendButton)
        self.LnyaxisButton = qt.QCheckBox("ln y axis")
        self.Lnyaxis = False
        self.LnyaxisButton.setChecked(self.Lnyaxis)
        OptionsHContainer.addWidget(self.LnyaxisButton)
        OptionsVContainer.addLayout(OptionsHContainer)

        OptionsHContainer = qt.QHBoxLayout()
        self.MTButton = qt.QCheckBox("Use multithreads")
        self.multithreading = True
        self.MTButton.setChecked(self.multithreading)
        OptionsHContainer.addWidget(self.MTButton)
        self.useColorButton = qt.QCheckBox("Use colors")
        self.useColorButton.setChecked(self.enableColors)
        OptionsHContainer.addWidget(self.useColorButton)
        OptionsVContainer.addLayout(OptionsHContainer)

        OptionsHContainer = qt.QHBoxLayout()
        self.forceExtraTailPointButton = qt.QCheckBox("Force 0 extra point at 10*T(last data point) (omitted in trapezoid)")
        self.forceExtraTailPointButton.setChecked(True)
        OptionsHContainer.addWidget(self.forceExtraTailPointButton)
        OptionsVContainer.addLayout(OptionsHContainer)

        OptionsFormLayout.addRow(OptionsVContainer)

        self.loggingLevels = {
            'Critical': logging.CRITICAL,
            'Error': logging.ERROR,
            'Warning': logging.WARNING,
            'Info': logging.INFO,
            'Debug': logging.DEBUG,
            'All': logging.NOTSET
        }
        self.loggingButton = qt.QComboBox()
        self.loggingButton.addItems(list(self.loggingLevels.keys()))
        self.loggingButton.setCurrentIndex(2)
        OptionsFormLayout.addRow('Set logging level', self.loggingButton)

        self.LegendButton.stateChanged.connect(self.onLegendButton)
        self.LnyaxisButton.stateChanged.connect(self.onLnyaxisButton)
        self.MTButton.stateChanged.connect(self.onMTButton)
        self.useColorButton.stateChanged.connect(self.useColors)
        self.loggingButton.currentIndexChanged.connect(self.onloggingButton)

        #
        # End Options Area
        #

        # Add vertical spacer
        self.layout.addStretch(1)

        # Initial setup of the isotope
        isotopeText = self.isotopsList.currentText
        self.logic = OpenDose3DLogic(parent=self, isotopeText=isotopeText)
        self.onIsotopeChange()

        # Refresh Apply button state
        self.checkAvailability()

    def onReload(self):
        """
        Override reload scripted module widget representation.
        """
        self.logger.warning("Reloading OpenDose3D")
        importlib.reload(mod)
        for submoduleName in __submoduleNames__:
            mod1 = importlib.import_module(
                '.'.join(['Logic', submoduleName]), __name__)
            importlib.reload(mod1)

        if isinstance(self, ScriptedLoadableModuleWidget):
            ScriptedLoadableModuleWidget.onReload(self)

    def onDensityCorrectionCheck(self):
        if self.applyDensityCorrection.isChecked():
            self.kernelMaterialContainer.hide()
        else:
            self.kernelMaterialContainer.show()

    def onMTButton(self):
        self.multithreading = self.MTButton.isChecked()

    def onLegendButton(self):
        self.ShowLegend = self.LegendButton.isChecked()
        graphnodes = slicer.util.getNodesByClass('vtkMRMLPlotChartNode')

        for graphnode in graphnodes:
            graphnode.SetLegendVisibility(self.ShowLegend)

    def onLnyaxisButton(self):
        if self.logic:
            self.logic.logyaxis = self.LnyaxisButton.isChecked()
        graphnodes = slicer.util.getNodesByClass('vtkMRMLPlotChartNode')

        for graphnode in graphnodes:
            graphnode.SetYAxisLogScale(self.LnyaxisButton.isChecked())

    def onInstallModuleButton(self, module="SlicerElastix"):
        utils.installModule(module)

    def onloggingButton(self):
        levelText = self.loggingButton.currentText
        levelValue = self.loggingLevels[levelText]
        self.logger.setLevel(levelValue)

    def switchModule(self, moduleName):
        # Switch to another module
        pluginHandlerSingleton = slicer.qSlicerSubjectHierarchyPluginHandler.instance()
        pluginHandlerSingleton.pluginByName(
            'Default').switchToModule(moduleName)

    # Definition of connections events

    def onCleanUpButton(self):
        self.logic.clean()
        self.DoseCalculated = False
        self.ADRMPlotsMade = False
        self.ACTMPlotsMade = False
        self.checkAvailability()

    def onRenameButton(self):
        self.logic.standardise(
            self.InjectionActivity.text, self.InjectionTime.text)
        if self.InjectionActivity.text != self.logic.injectedActivity:
            self.InjectionActivity.setText(str(self.logic.injectedActivity))
            self.InjectionActivity.setToolTip(
                f"Injected activity {self.logic.injectedActivity} extracted from DICOMheader")
        if self.InjectionTime.text != self.logic.injectionTime:
            self.InjectionTime.setText(str(self.logic.injectionTime))
            self.InjectionTime.setToolTip(
                f"Injection Time {self.logic.injectionTime} extracted from DICOMheader")
        self.saveOptions()
        self.checkAvailability()

    def onResampleButton(self):
        self.logic.resampleCT()
        self.checkAvailability()

    def onRescaleButton(self):
        self.saveOptions()

        try:
            sensitivity = self.Evaluator.eval(self.spectSensitivity.text)
        except Exception as e:
            self.logger.error(f'Sensitivity not valid, please check\n{e}')

        try:
            calibrationTime = self.Evaluator.eval(
                self.spectCalibrationTime.text)
        except Exception as e:
            self.logger.error(f'Calibration Time not valid, please check\n{e}')

        self.calibration['SPECTSensitivity'] = {
            'Value': float(sensitivity),
            'Units': self.sensitivityUnits.currentText,
            'Time': int(calibrationTime)
        }

        self.logic.scaleValues(
            calibration=self.calibration,
            injectedActivity=self.InjectionActivity.text,
            injectionTime=self.InjectionTime.text
        )
        self.checkAvailability()

    def onRegistrationButton(self):
        referenceNode = self.initialPoint.currentNode()
        self.logic.createTransforms(referenceNode)
        self.checkAvailability()

    def onDoseRateAlgorithm(self):
        algorithm = self.doseRateAlgorithm.currentText
        if "led" in algorithm.lower() or "homogeneous" in algorithm.lower():
            self.densityCorrectionContainer.show()
        else:
            self.densityCorrectionContainer.hide()

        if "heterogeneous" in algorithm.lower():
            self.kernelDistanceLimitContainer.show()
            self.activityThresholdContainer.show()
        else:
            self.kernelDistanceLimitContainer.hide()
            self.activityThresholdContainer.hide()

        if "Monte Carlo" in algorithm:
            self.MonteCarloContainer.show()
            self.doseRateButton.hide()
        else:
            self.MonteCarloContainer.hide()
            self.doseRateButton.show()

    def onIntegrationModeAlgoritm(self):
        algorithm = self.integrationMode.currentText
        if "trapezoid" in algorithm:
            self.tailModeContainer.show()
        else:
            self.tailModeContainer.hide()

    def setCalibrationTimeVisibility(self):
        if 'Bqs' in self.sensitivityUnits.currentText:
            self.calibrationTimeContainer.hide()
        else:
            self.calibrationTimeContainer.show()

    
    def onCalibrationDateChanged(self):
        center = self.ClinicalCenter.currentText
        isotopeText = self.isotopsList.currentText
        calibrationDate = self.calibrationDate.currentText
        if center in self.sensitivity and isotopeText in self.sensitivity[center] and calibrationDate in self.sensitivity[center][isotopeText]:
            lsensitivity = self.sensitivity[center][isotopeText][calibrationDate]
            self.spectSensitivity.setText(lsensitivity["Sensitivity"])
            index = self.sensitivityUnits.findText(
                lsensitivity['Units'], qt.Qt.MatchFixedString)
            if index > 0:
                self.sensitivityUnits.setCurrentIndex(index)
            self.spectCalibrationTime.setText(lsensitivity['Time'])

    def onIsotopeChange(self):
        self.logger.info(
            f'Loading data for isotope {self.isotopsList.currentText}')
        center = self.ClinicalCenter.currentText
        isotopeText = self.isotopsList.currentText
        self.logic.Isotope = dbutils.setupIsotope(isotopeText)
        self.calibrationDate.clear()
        if center in self.sensitivity and isotopeText in self.sensitivity[center]:
            lsensitivity = self.sensitivity[center][isotopeText]
            dates = list(lsensitivity.keys())
            self.calibrationDate.addItems(dates)
        self.saveOptions()

    def onInitialPointChange(self):
        lnode = self.initialPoint.currentNode()
        if not lnode:  # The node must has been selected
            self.registrationButton.enabled = False
            return
        if lnode.GetImageData() is None:  # The selected node must contain an image
            self.registrationButton.enabled = False
            return
        lName = lnode.GetName()
        # This node must be a CT
        if 'CTCT' in lName:
            self.registrationButton.enabled = True
            self.logger.info(f"Selected {lName} as reference volume")
        else:
            self.registrationButton.enabled = False
            self.logger.error(f"Selected volume {lName} is not a CT node")

    def onCompressDB(self):
        if dbutils.isCompressed():  # Database is compressed
            dbutils.decompressDataBase()
        else:
            dbutils.compressDataBase()
        self.checkAvailability()

    def onDoseRateButton(self):
        # initialize
        mode = self.doseRateAlgorithm.currentText
        self.logic.densityCorrection = self.applyDensityCorrection.isChecked()
        if self.applyDensityCorrection.isChecked():
            kernelMaterial = 'water'
        else:
            kernelMaterial = [material for material, values in Constants(
            ).materials.items() if values['name'] == self.kernelMaterial.currentText][0]
        # select method
        activityThreshold = self.activityThresholdSliderWidget.value / \
            100 if 'heterogeneous' in mode.lower() else 0
        if 'LED' in mode:
            self.logic.LED(DVK_material=kernelMaterial,
                           threshold=activityThreshold)
        elif 'convolution' in mode.lower():
            self.logic.compressed = dbutils.isCompressed()
            kernellimit = 0 if 'homogeneous' in mode.lower() else self.kernelDistanceLimit.value
            self.logic.convolution(
                isotopeText=self.isotopsList.currentText,
                DVK_material=kernelMaterial,
                threshold=activityThreshold,
                kernellimit=kernellimit,
                mode=mode,
                multithreaded=self.multithreading
            )
        elif 'monte' in mode.lower():
            self.logic.importMonteCarlo()
        self.DoseCalculated = True
        self.checkAvailability()

    def onGenerateGate(self):
        directory = qt.QFileDialog.getExistingDirectory(
            self.parent,
            Constants().strings["Choose Directory"],
            self.usePath,
            qt.QFileDialog.ShowDirsOnly | qt.QFileDialog.DontResolveSymlinks
        )
        if directory:
            self.usePath = Path(directory)
            self.Gate = Gate(
                directory=self.usePath,
                calibration=self.calibration,
                isotope=self.isotopsList.currentText
            )
            self.Gate.generate()
        self.saveOptions()

    def onImportMonteCarlo(self):
        directory = qt.QFileDialog.getExistingDirectory(
            self.parent,
            Constants().strings["Choose Directory"],
            self.usePath,
            qt.QFileDialog.ShowDirsOnly | qt.QFileDialog.DontResolveSymlinks
        )
        if directory:
            self.usePath = Path(directory)
            self.logic.importMonteCarlo(directory=self.usePath)
        self.checkAvailability()

    def onPropagateButton(self):
        self.logic.Propagate(self.segmentation.currentNode())
        self.SegmentationPropagated = True
        self.checkAvailability()

    def onACTMTablesButton(self):
        self.logic.computeNewValues(mode='Activity')
        self.logic.processTimeIntegrationVariables(mode='Activity')
        self.logic.showTableandPlotTimeIntegrationVariables(
            mode='Activity', showlegend=self.ShowLegend)
        self.ACTMPlotsMade = True
        self.checkAvailability()

    def onDOSETablesButton(self):
        self.logic.computeNewValues(
            mode='DoseRate',
            algorithm=self.doseRateAlgorithm.currentText
        )
        self.logic.processTimeIntegrationVariables(
            mode='DoseRate',
            algorithm=self.doseRateAlgorithm.currentText
        )
        self.logic.showTableandPlotTimeIntegrationVariables(
            mode='DoseRate',
            algorithm=self.doseRateAlgorithm.currentText,
            showlegend=self.ShowLegend
        )
        self.ADRMPlotsMade = True
        self.checkAvailability()

    def onIntegrateActivityButton(self):
        integrationMode = self.integrationMode.currentText
        forceExtraPoint = self.forceExtraTailPointButton.isChecked() and not 'trapezoid' in integrationMode.lower()
        self.logic.integrate(
            mode='Activity',
            algorithm='',
            incorporationmode=self.incorporationMode.currentText,
            integrationmode=integrationMode,
            tailmode=self.tailMode.currentText,
            forceExtraPoint=forceExtraPoint
        )

    def onIntegrateDoseButton(self):
        integrationMode = self.integrationMode.currentText
        forceExtraPoint = self.forceExtraTailPointButton.isChecked() and not 'trapezoid' in integrationMode.lower()
        self.logic.integrate(
            mode='DoseRate',
            algorithm=self.doseRateAlgorithm.currentText,
            incorporationmode=self.incorporationMode.currentText,
            integrationmode=integrationMode,
            tailmode=self.tailMode.currentText,
            forceExtraPoint=forceExtraPoint
        )

    def onACTMExportButton(self):
        # TODO: Define input for Clinical Study Title and ID
        referenceFolder = self.logic.getReferenceFolder()

        directory = qt.QFileDialog.getExistingDirectory(
            self.parent,
            Constants().strings["Choose Directory"],
            self.usePath,
            qt.QFileDialog.ShowDirsOnly | qt.QFileDialog.DontResolveSymlinks
        )
        if directory:
            self.usePath = Path(directory)
            XMLString = xmlexport.XML_MEDIRAD(
                referenceFolder,
                self.usePath,
                'Dosimetry calculation - I-131 ablation of thyroid',
                '755523-t33'
            )
            XMLString.fillACTM_XMLfile()
            self.saveOptions()

    def onDOSEExportButton(self):
        # TODO: Define input for Clinical Study Title and ID
        referenceFolder = self.logic.getReferenceFolder()

        directory = qt.QFileDialog.getExistingDirectory(
            self.parent,
            Constants().strings["Choose Directory"],
            self.usePath,
            qt.QFileDialog.ShowDirsOnly | qt.QFileDialog.DontResolveSymlinks
        )
        if directory:
            self.usePath = Path(directory)
            XMLString = xmlexport.XML_MEDIRAD(
                referenceFolder,
                self.usePath,
                'Dosimetry calculation - I-131 ablation of thyroid',
                '755523-t33'
            )
            XMLString.fillADRM_XMLfile(
                mode='DoseRate',
                algorithm=self.doseRateAlgorithm.currentText
            )
            self.saveOptions()

    def useColors(self):
        self.enableColors = not(self.enableColors)
        self.saveOptions()
        self.setupWidgets()

    def checkAvailability(self):
        self.resampleButton.enabled = False
        self.rescaleButton.enabled = False
        self.initialPoint.enabled = False
        self.onInitialPointChange()
        self.doseRateButton.enabled = False
        self.GenerateGateButton.enabled = False
        self.ImportGateButton.enabled = False
        self.propagateButton.enabled = False
        self.ACTMTablesButton.enabled = False
        self.DOSETablesButton.enabled = False
        self.IntegrateActivityButton.enabled = False
        self.IntegrateDoseButton.enabled = False
        if dbutils.isCompressed():  # Database is compressed
            self.DBCompressionButton.setText("Compress Database")
            self.DBCompressionButton.toolTip = "Compress the database."
        else:
            self.DBCompressionButton.setText("Decompress Database")
            self.DBCompressionButton.toolTip = "Decompress the database."
        folders = vtkmrmlutils.getFolderIDs()
        if folders:
            nodes = Node.getFolderChildren(folders[0])
            if nodes:
                self.resampleButton.enabled = "CTCT" in nodes
                self.rescaleButton.enabled = 'CTRS' in nodes
                self.initialPoint.enabled = "CTCT" in nodes
                self.onInitialPointChange()
                self.doseRateButton.enabled = "ACTM" in nodes
                self.GenerateGateButton.enabled = "ACTM" in nodes
                self.ImportGateButton.enabled = "ACSC" in nodes
                hasSegmentation = vtkmrmlutils.hasSegmentation(
                    self.segmentation.currentNode())
                self.propagateButton.enabled = hasSegmentation
                self.ACTMTablesButton.enabled = "ACTM" in nodes and hasSegmentation
                self.DOSETablesButton.enabled = "ADRM" in nodes and hasSegmentation and (
                    self.DoseCalculated or "MonteCarlo" in nodes['ADRM'])
                self.IntegrateActivityButton.enabled = self.ACTMPlotsMade and self.ACTMTablesButton.enabled
                self.IntegrateDoseButton.enabled = self.ADRMPlotsMade and self.DOSETablesButton.enabled
